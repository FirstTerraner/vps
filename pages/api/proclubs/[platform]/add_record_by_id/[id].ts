import { NextApiRequest, NextApiResponse } from 'next'
import { getClubInfo, getClubStats, isPlatformType } from 'proclubs-api/dist'
import { cacheClient } from '../../../../../src/cache'
import { setHighestRec, sortRecord } from '../../../../../src/helpers'
import { checkPlatform } from '../../../../../src/middleware/clubs'
import { IRecClub } from '../../../../../src/models/interfaces'
import { cTo, isArr, isStr, toJsonStr } from '../../../../../src/models/typeGuards'

const handler = async (
	req: NextApiRequest,
	// 0 for bad response,
	// 1 for success,
	// 2 for duplicated leaderboard entry,
	// 3 for record too low
	res: NextApiResponse<0 | 1 | 2 | 3>
) => {

	res.setHeader(
		'Cache-Control',
		'public, s-maxage=240, stale-while-revalidate=59'
	)

	try {

		console.log('start adding record club by id...')

		const { platform, id } = req.query

		if (!isPlatformType(platform)) {
			res.status(500).send(0)
			return
		}

		// check if club exists, is undefeated & has at least 1 match
		const clubResp = await Promise.all([
			getClubInfo(platform, +id),
			getClubStats(platform, +id)
		])

		console.log('requested record club response: ', clubResp)

		if (clubResp[1]?.totalGames === 0 || (clubResp[1] && +clubResp[1].losses > 0)) {
			res.status(200).send(3)
			return
		}

		// correct input
		if (clubResp[0] && clubResp[1] && clubResp[1].totalGames > 0 && clubResp[1].losses === '0') {

			const client = cacheClient()

			if (!client) {
				res.status(500).send(0)
				return
			}

			const recEntriesStr = await client.get(`${platform.toString()}-record`)
			console.log('already registered record entries string for ', platform, ' : ', recEntriesStr)

			if (!recEntriesStr) {
				await client.quit()
				res.status(500).send(0)
				return
			}

			const recEntries = cTo<IRecClub[] | null>(recEntriesStr)

			if (!isArr(recEntries)) {
				await client.quit()
				res.status(500).send(0)
				return
			}

			// check for duplicates
			if (recEntries.some(rec => +rec.clubId === clubResp[0]?.clubId)) {
				await client.quit()
				res.status(200).send(2)
				return
			}

			// check if record is high enough
			if ((recEntries.length === 25 && recEntries.some(rec => clubResp[1] && +rec.wins < +clubResp[1].wins)) || recEntries.length < 25) {
				// push new rec club
				recEntries.push({
					...clubResp[1],
					clubInfo: clubResp[0],
					name: clubResp[0].name,
					addedAt: new Date().toLocaleDateString(),
					squadList: [],
					hasLost: false,
					plat: platform
				})

				// sort array from highest record to lowest
				sortRecord(recEntries)

				// after sorting, remove the last club if we now have 26 entries
				if (recEntries.length > 25) {
					recEntries.pop()
				}

				console.log('new record array after sort and (pop): ', recEntries)

				const recEntriesString = toJsonStr(recEntries)

				// set new record array in db
				if (isStr(recEntriesString) && recEntriesString.length > 0) {
					console.log('new record string to add to db: ', recEntriesString)
					await client.set(`${platform.toString()}-record`, recEntriesString)
				}
				// set new highest
				await setHighestRec(recEntries, client)

				await client.quit()
				res.status(200).send(1)
				return
			}

			// record too low
			res.status(200).send(3)
			return
		}

	} catch (e) {
		console.error(e)
	}
	res.status(500).send(0)
}

export default checkPlatform(handler) // checkPlatform(checkName())
