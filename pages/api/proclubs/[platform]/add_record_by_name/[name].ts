import { NextApiRequest, NextApiResponse } from 'next'
import { getClubSearch, isPlatformType } from 'proclubs-api/dist'
import { IClubSearch } from 'proclubs-api/dist/model/club'
import { cacheClient } from '../../../../../src/cache'
import { setHighestRec, sortRecord } from '../../../../../src/helpers'
import { checkPlatform } from '../../../../../src/middleware/clubs'
import { IRecClub } from '../../../../../src/models/interfaces'
import { cTo, isArr, isStr, toJsonStr } from '../../../../../src/models/typeGuards'

const handler = async (
	req: NextApiRequest,
	// 0 for bad response,
	// 1 for success,
	// 2 for duplicated leaderboard entry,
	// 3 for record too low
	// club array to let user choose club
	res: NextApiResponse<0 | 1 | 2 | 3 | IClubSearch[]>
) => {

	res.setHeader(
		'Cache-Control',
		'public, s-maxage=240, stale-while-revalidate=59'
	)

	try {

		const { platform, name } = req.query

		if (!isPlatformType(platform)) {
			res.status(500).send(0)
			return
		}

		// check if club exists, is undefeated & has at least 1 match
		const club = await getClubSearch(platform, name.toString())
		console.log('record club search by name: ', club)
		if (!club || club.length === 0) {
			res.status(500).send(0)
			return
		}
		// found more than 1 match
		if (club.length > 1) {
			// TODO send clubs and let user choose one of them
			// ^^ frontend needs API endpoint .../add_record/[id]
			res.status(200).send(club)
			return
		}
		// not a record club
		if (club[0]?.totalGames === 0 || +club[0]?.losses > 0) {
			res.status(500).send(3)
			return
		}
		// correct input
		if (club.length === 1 && club[0]?.totalGames > 0 && club[0]?.losses === '0') {

			const client = cacheClient()

			if (!client) {
				res.status(500).send(0)
				return
			}

			const recEntriesStr = await client.get(`${platform.toString()}-record`)

			console.log('already registered record Entries String: ', recEntriesStr)

			if (!recEntriesStr) {
				await client.quit()
				res.status(500).send(0)
				return
			}

			const recEntries = cTo<IRecClub[] | null>(recEntriesStr)
			console.log('registered record object for ', platform, ' : ', recEntries)

			if (!isArr(recEntries)) {
				await client.quit()
				res.status(500).send(0)
				return
			}

			// check for duplicates
			if (recEntries.some(rec => rec.clubId === club[0].clubId)) {
				await client.quit()
				res.status(200).send(2)
				return
			}

			// check if record is high enough OR if array has not max length of 25 yet
			if ((recEntries.length === 25 && recEntries.some(rec => +rec.wins < +club[0].wins)) || recEntries.length < 25) {
				// push new rec club
				recEntries.push({
					...club[0],
					addedAt: new Date().toLocaleDateString(),
					squadList: [],
					hasLost: false,
					plat: platform
				})

				// sort array from highest record to lowest
				sortRecord(recEntries)

				// after sorting, remove the last club if we now have 26 entries
				if (recEntries.length > 25) {
					recEntries.pop()
				}

				console.log('new record entries after sort and (pop): ', recEntries)

				// set new record array in db
				const recEntriesString = toJsonStr(recEntries)
				// set new record array in db
				if (isStr(recEntriesString) && recEntriesString.length > 0) {
					console.log('new record array string before adding to db: ', recEntriesString)
					await client.set(`${platform.toString()}-record`, recEntriesString)
				}
				// set new highest
				await setHighestRec(recEntries, client)
				await client.quit()
				res.status(200).send(1)
				return
			}

			// record too low
			res.status(200).send(3)
			return
		}

	} catch (e) {
		console.error(e)
	}
	res.status(500).send(0)
}

export default checkPlatform(handler) // checkPlatform(checkName())
