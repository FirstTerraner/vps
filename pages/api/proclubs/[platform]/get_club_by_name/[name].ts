// /api/proclubs/get-club-by-name/blood brotherz
import { NextApiRequest, NextApiResponse } from 'next'
import { getClubSearch, isPlatformType } from 'proclubs-api/dist'
import { IClubSearch } from 'proclubs-api/dist/model/club'
import { cacheClient } from '../../../../../src/cache'
import { checkName, checkPlatform } from '../../../../../src/middleware/clubs'
import { cTo, isArr, isStr, toJsonStr } from '../../../../../src/models/typeGuards'

const handler = async (
	req: NextApiRequest,
	res: NextApiResponse<IClubSearch[]>
) => {
	// console.log('req.query: ', req.query)
	try {
		const { platform, name } = req.query
		// console.log('api req name params: ', name)
		// console.log('req.url: ', req.url)
		if (!isPlatformType(platform) || !isStr(name)) {
			res.status(500).send([])
			return
		}
		const client = cacheClient()
		if (client) {
			const cached = await client.get(req.url || '')
			if (cached) {
				await client.quit()
				const cachedJson = cTo<IClubSearch[] | null>(cached)
				if (!cachedJson) {
					return res.status(500).send([])
				}
				return res.status(200).send(cachedJson)
			}
		}
		const clubs = await getClubSearch(platform, name)
		console.log('search clubs: ', clubs)
		if (isArr(clubs) && clubs?.length) {
			const clubsStr = toJsonStr(clubs)
			if (isStr(clubsStr) && clubsStr.length > 0) {
				await client.setex(req.url || '', 60 * 5, clubsStr)
			}
			await client.quit()
			return res.send(clubs)
		}
		await client.quit()
	} catch (e) {
		console.error(e)
	}
	res.send([])
}

export default checkPlatform(checkName(handler))
