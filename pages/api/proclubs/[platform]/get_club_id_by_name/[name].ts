import { NextApiRequest, NextApiResponse } from 'next'
import { getClubIdByName, isPlatformType } from 'proclubs-api/dist'
import { checkName, checkPlatform } from '../../../../../src/middleware/clubs'
import { isNum, isStr } from '../../../../../src/models/typeGuards'

const handler = async (req: NextApiRequest, res: NextApiResponse<number>) => {
	console.log('req.query: ', req.query)
	try {
		const { platform, name } = req.query
		// console.log('api req name params: ', name)
		if (!isPlatformType(platform) || !isStr(name)) {
			res.status(500).send(-1)
			return
		}
		const clubs = await getClubIdByName(platform, name)
		if (isNum(clubs)) {
			return res.send(clubs)
		}
	} catch (e) {
		console.error(e)
	}
	res.send(-1)
}
export default checkPlatform(checkName(handler))
