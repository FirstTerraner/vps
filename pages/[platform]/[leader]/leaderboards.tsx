// libs
import { GetServerSideProps } from 'next'
import { getOverallLeaderboard, getSeasonLeaderboard, isPlatformType } from 'proclubs-api/dist'
import { IClubRankLeaderboard } from 'proclubs-api/dist/model/leaderboard'
import { cacheClient, getRecClubs, saveVisitor } from '../../../src/cache'
// custom components
import RankingPage from '../../../src/components/pages/rankingPage'
import { getRecordUpdateInfo, sortRecord } from '../../../src/helpers'
import { IHighestClub, IRankingClubsProps, IRecClub, isIRecClubArr } from '../../../src/models/interfaces'
import { cTo, isStr, toJsonStr } from '../../../src/models/typeGuards'

// lederboard props
export const getServerSideProps: GetServerSideProps = async context => {

	const client = cacheClient()

	await saveVisitor(context, client)

	const platform = context.params?.platform
	const leader = context.params?.leader

	if (!platform || !isPlatformType(platform) || !leader || !isStr(leader)) {
		await client.quit()
		return { props: { rankingClubs: null } }
	}

	// record leaders
	const recClubs = await getRecClubs(client, platform)
	// record updated time
	const recInfo = await getRecordUpdateInfo(client)
	// console.log('recUpInfo: ', recInfo)

	const highestStr = await client.get('highest-record')
	if (!isStr(highestStr)) {
		await client.quit()
		return { props: { rankingClubs: null } }
	}
	const highestRecords = cTo<IRecClub | null>(highestStr)

	console.log('highestRec: ', highestRecords)

	// default fifa leaderboards response cache
	const entry = await client.get(context.resolvedUrl)
	if (isStr(entry) && entry.length > 0) {
		console.log('Cache hit, returning cache for route: ' + context.resolvedUrl)
		await client.quit()
		const jsonEntry = cTo<IRecClub[] | IClubRankLeaderboard[] | null>(entry)
		return { props: { rankingClubs: jsonEntry } }
	}
	// no cache.
	// after fetching data, write response in cache and close connection
	console.log('No cache... Fetching data now.')

	console.log('recClubs: ', recClubs)

	// fetch
	const rankingClubs = leader === 'record' ?
		recClubs
		: leader === 'clubs' ?
			await getOverallLeaderboard(platform)
			: await getSeasonLeaderboard(platform)


	console.log('ranking clubs: ', rankingClubs)
	if (!rankingClubs) {
		await client.quit()
		return { props: { rankingClubs: null } }
	}
	// sort array from highest record to lowest
	if (isIRecClubArr(rankingClubs)) {
		sortRecord(rankingClubs)
	}

	if (leader === 'season' || leader === 'clubs') {
		console.log('set response in cache for fifa leaderboard')
		const rankingStr = toJsonStr(rankingClubs)
		if (isStr(rankingStr) && rankingStr.length > 0) {
			await client.setex(context.resolvedUrl, 60 * 5, rankingStr)
		}
	}

	await client.quit()
	return { props: { rankingClubs, highestRecords, recInfo } }
}

// page component
const Leaderboards = ({ rankingClubs, highestRecords, recInfo }: IRankingClubsProps) => (
	<RankingPage rankingClubs={rankingClubs} highestRecords={highestRecords} recInfo={recInfo} />
)

export default Leaderboards