import { GetServerSideProps } from 'next'
import { isPlatformType } from 'proclubs-api/dist'
import { cacheClient, getRecClubById, saveVisitor } from '../../../../../src/cache'
import RecClubPage from '../../../../../src/components/pages/recClubPage'
import { IRecClubOverviewState } from '../../../../../src/models/interfaces'
import { isArr, isStr } from '../../../../../src/models/typeGuards'

// main component
const RecClubOverview = ({ club }: { club: IRecClubOverviewState }) => <RecClubPage club={club} />

export default RecClubOverview

// club page props
export const getServerSideProps: GetServerSideProps = async context => {

	const client = cacheClient()

	await saveVisitor(context, client)

	const platform = context.params?.platform
	const clubId = context.params?.id
	if (!isPlatformType(platform) || !isStr(clubId)) {
		await client.quit()
		return { props: { infos: null, stats: null } }
	}

	const recClub = await getRecClubById(client, platform, clubId)

	if (!isArr(recClub)) {
		await client.quit()
		return { props: { infos: null, stats: null } }
	}

	await client.quit()

	return {
		props: {
			club: {
				infos: recClub[0].clubInfo,
				stats: recClub[0]
			}
		}
	}
}