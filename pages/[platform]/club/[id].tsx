// libs
import { GetServerSideProps } from 'next'
import { isPlatformType } from 'proclubs-api/dist'
import { cacheClient, saveVisitor } from '../../../src/cache'
import ClubPage from '../../../src/components/pages/clubPage'
// helpers
import { IClubOverviewState } from '../../../src/models/interfaces'
import { isStr } from '../../../src/models/typeGuards'
import { getClubInfoProps } from '../../../src/proclubsApis'

// main component
const ClubOverview = ({ club }: { club: IClubOverviewState }) => <ClubPage club={club} />

export default ClubOverview

// club page props
export const getServerSideProps: GetServerSideProps = async context => {

	const client = cacheClient()

	await saveVisitor(context, client)

	const platform = context.params?.platform
	const clubId = context.params?.id
	if (!isPlatformType(platform) || !isStr(clubId)) {
		await client.quit()
		return { props: { club: null } }
	}

	// cache promises
	const setExPromArr: Promise<'OK'>[] = []

	// get club infos and push response in cache promise array
	const clubInfo = await getClubInfoProps(client, platform, clubId, setExPromArr)

	console.log('clubInfo: ', clubInfo)

	// set cache
	await Promise.all(setExPromArr)
	await client.quit()

	return {
		props: {
			club: {
				infos: clubInfo[0] || null,
				stats: clubInfo[1] || null
			}
		}
	}
}