import { GetServerSideProps } from 'next'
import { isPlatformType } from 'proclubs-api/dist'
import { cacheClient, saveVisitor } from '../../../../src/cache'
import ClubPage from '../../../../src/components/pages/clubPage'
import { sortMemberCareerOrStatsByPos, sortMemberCareerOrStatsByPos2 } from '../../../../src/helpers'
import { IMatchesProps } from '../../../../src/models/interfaces'
import { isStr } from '../../../../src/models/typeGuards'
import { getClubInfoProps, getClubMatchesProps, getClubMembersProps } from '../../../../src/proclubsApis'

const Squad = ({ club, matches, cupMatches, squad }: IMatchesProps) =>
	<ClubPage club={club} matches={matches} cupMatches={cupMatches} squad={squad} />

export default Squad

// club matches props
export const getServerSideProps: GetServerSideProps = async context => {

	const client = cacheClient()

	await saveVisitor(context, client)

	const platform = context.params?.platform
	const clubId = context.params?.id
	if (!isPlatformType(platform) || !isStr(clubId)) {
		await client.quit()
		return { props: { club: null, matches: null, cupMatches: null, squad: null } }
	}
	// cache promises
	const setExPromArr: Promise<'OK'>[] = []

	// get club infos and push response in cache promise array
	const clubInfo = await getClubInfoProps(client, platform, clubId, setExPromArr)

	// get club matches and push response in cache promise array
	const clubMatches = await getClubMatchesProps(client, platform, clubId, setExPromArr)

	// get club members and push response in cache promise array
	const clubMembers = await getClubMembersProps(client, platform, clubId, setExPromArr)

	console.log('club members: ', clubMembers)

	// set cache
	await Promise.all(setExPromArr)

	await client.quit()

	const sortedMemberStats = sortMemberCareerOrStatsByPos(clubMembers[1] || [])

	return {
		props: {
			club: {
				infos: clubInfo[0],
				stats: clubInfo[1]
			},
			matches: clubMatches[0],
			cupMatches: clubMatches[1],
			squad: {
				memberCareer: sortedMemberStats,
				memberStats: sortedMemberStats && clubMembers[0] ? sortMemberCareerOrStatsByPos2(sortedMemberStats, clubMembers[0]) : []
			}
		}
	}
}