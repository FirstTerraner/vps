import { GetServerSideProps } from 'next'
import { isPlatformType } from 'proclubs-api/dist'
import { cacheClient, saveVisitor } from '../../../../../../src/cache'
import PlayerOverview from '../../../../../../src/components/pages/playerPage'
import { getSingleMemberObj, sortMemberCareerOrStatsByPos, sortMemberCareerOrStatsByPos2 } from '../../../../../../src/helpers'
import { IPlayerPageProps } from '../../../../../../src/models/interfaces'
import { isStr } from '../../../../../../src/models/typeGuards'
import { getClubInfoProps, getClubMatchesProps, getClubMembersProps } from '../../../../../../src/proclubsApis'

// club page props
export const getServerSideProps: GetServerSideProps = async context => {

	const client = cacheClient()

	await saveVisitor(context, client)

	const platform = context.params?.platform
	const clubId = context.params?.id
	const memberName = context.params?.name
	if (!isPlatformType(platform) || !isStr(clubId) || !isStr(memberName)) {
		await client.quit()
		return { props: { club: null, matches: null, cupMatches: null, squad: null } }
	}

	// cache promises
	const setExPromArr: Promise<'OK'>[] = []

	// get club infos and push response in cache promise array
	const clubInfo = await getClubInfoProps(client, platform, clubId, setExPromArr)

	// get club matches and push response in cache promise array
	const clubMatches = await getClubMatchesProps(client, platform, clubId, setExPromArr)

	// get club members and push response in cache promise array
	const clubMembers = await getClubMembersProps(client, platform, clubId, setExPromArr)

	// set cache
	await Promise.all(setExPromArr)

	// get the single player from member array in backend
	const member = getSingleMemberObj(memberName, clubMembers[0], clubMembers[1])

	// TODO check if this is needed
	clubMembers[1]?.reverse()
	clubMembers[0]?.reverse()

	const sortedMemberStats = sortMemberCareerOrStatsByPos(clubMembers[1] || [])

	await client.quit()

	return {
		props: {
			club: {
				infos: clubInfo[0] || null,
				stats: clubInfo[1] || null
			},
			matches: clubMatches[0] || null,
			cupMatches: clubMatches[1] || null,
			clubMember: member || null,
			squad: {
				memberCareer: sortedMemberStats,
				memberStats: sortedMemberStats ? sortMemberCareerOrStatsByPos2(sortedMemberStats, clubMembers[0] || []) : clubMembers[0]
			}
		}
	}
}

const PlayerDetails = ({ club, squad, clubMember, matches, cupMatches }: IPlayerPageProps) =>
	<PlayerOverview club={club} squad={squad} clubMember={clubMember} matches={matches} cupMatches={cupMatches} />

export default PlayerDetails