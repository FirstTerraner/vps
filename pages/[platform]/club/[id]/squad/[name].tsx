import { GetServerSideProps } from 'next'
import { isPlatformType } from 'proclubs-api/dist'
import { cacheClient, saveVisitor } from '../../../../../src/cache'
import PlayerOverview from '../../../../../src/components/pages/playerPage'
import { getSingleMemberObj } from '../../../../../src/helpers'
import { IClubMember, IClubOverviewState, ISquadOverviewState } from '../../../../../src/models/interfaces'
import { isStr } from '../../../../../src/models/typeGuards'
import { getClubInfoProps, getClubMembersProps } from '../../../../../src/proclubsApis'

interface IPlayerProps { squad: ISquadOverviewState, club: IClubOverviewState, clubMember: IClubMember | null }

const PlayerPage = ({ club, clubMember, squad }: IPlayerProps) => (
	<PlayerOverview club={club} clubMember={clubMember} squad={squad} />
)

export default PlayerPage

// player page props
export const getServerSideProps: GetServerSideProps = async context => {

	const client = cacheClient()

	await saveVisitor(context, client)

	const platform = context.params?.platform
	const clubId = context.params?.id
	const memberName = context.params?.name
	if (!isPlatformType(platform) || !isStr(clubId) || !isStr(memberName)) {
		await client.quit()
		return { props: { club: null, clubMember: null, squad: null } }
	}
	// cache promises
	const setExPromArr: Promise<'OK'>[] = []

	// get club infos and push response in cache promise array
	const clubInfo = await getClubInfoProps(client, platform, clubId, setExPromArr)

	// get club members and push response in cache promise array
	const clubMembers = await getClubMembersProps(client, platform, clubId, setExPromArr)

	// set cache
	await Promise.all(setExPromArr)

	// get the single player from member array in backend
	const member = getSingleMemberObj(memberName, clubMembers[0], clubMembers[1])

	// TODO check if this is needed
	clubMembers[1]?.reverse()
	clubMembers[0]?.reverse()

	await client.quit()

	return {
		props: {
			club: {
				infos: clubInfo[0],
				stats: clubInfo[1]
			},
			clubMember: member,
			squad: {
				memberCareer: clubMembers[1],
				memberStats: clubMembers[0]
			}
		}
	}
}