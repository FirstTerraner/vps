import { GetServerSideProps } from 'next'
import { isPlatformType } from 'proclubs-api/dist'
import { cacheClient, saveVisitor } from '../../../../src/cache'
import ClubPage from '../../../../src/components/pages/clubPage'
import { IMatchesProps } from '../../../../src/models/interfaces'
import { isStr } from '../../../../src/models/typeGuards'
import { getClubInfoProps, getClubMatchesProps } from '../../../../src/proclubsApis'

const DivMatches = ({ club, matches, cupMatches }: IMatchesProps) =>
	<ClubPage club={club} matches={matches} cupMatches={cupMatches} />

export default DivMatches

// club matches props
export const getServerSideProps: GetServerSideProps = async context => {

	const client = cacheClient()

	await saveVisitor(context, client)

	const platform = context.params?.platform
	const clubId = context.params?.id
	if (!isPlatformType(platform) || !isStr(clubId)) {
		await client.quit()
		return { props: { club: null } }
	}

	// cache promises
	const setExPromArr: Promise<'OK'>[] = []

	// get club infos and push response in cache promise array
	const clubInfos = await getClubInfoProps(client, platform, clubId, setExPromArr)

	// get club matches and push response in cache promise array
	const clubMatches = await getClubMatchesProps(client, platform, clubId, setExPromArr)

	// set cache
	await Promise.all(setExPromArr)
	await client.quit()

	return {
		props: {
			club: {
				infos: clubInfos[0] || null,
				stats: clubInfos[1] || null
			},
			matches: clubMatches[0] || null,
			cupMatches: clubMatches[1] || null
		}
	}
}