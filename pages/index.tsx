
import Homepage from '../src/components/pages/homepage'
import { GetServerSideProps } from 'next'
import { cacheClient, saveVisitor } from '../src/cache'

// TODO no need to return props, check for alternative server side execution before page response
export const getServerSideProps: GetServerSideProps = async context => {

	const client = cacheClient()

	await saveVisitor(context, client)

	await client.quit()

	// saveVisitor in getInitialProps throws an error "no access to redis in frontend"
	// returning prop is required in getServerSideProps.
	// ended up returning pseudo prop for setting visitor count, even if this page does not need props
	return { props: { x: null } }
}

const App = () => <Homepage />

export default App