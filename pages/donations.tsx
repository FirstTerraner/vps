import PageBuilding from '../src/components/pages/pageBuilding'

/* export const getServerSideProps: GetServerSideProps = async context => {

	const client = cacheClient()

	await saveVisitor(context, client)

	await client.quit()

	// saveVisitor in getInitialProps throws an error "no access to redis in frontend"
	// returning prop is required in getServerSideProps.
	// ended up returning pseudo prop for setting visitor count, even if this page does not need props
	return { props: { x: null } }
} */

const DonationPage = () => <PageBuilding headerTxt='Donations' />

export default DonationPage