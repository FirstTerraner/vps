import { GetStaticProps } from 'next'
import AboutPage from '../src/components/pages/aboutPage'
import { getAllProjectMembers } from '../src/gitlabApis'
import { IProjectMember } from '../src/models/interfaces'

export const getStaticProps: GetStaticProps = async () => {
	// get project members
	const members = await getAllProjectMembers()

	// console.log('members: ', members)

	if (!members) { return { props: { members: null } } }

	return { props: { members } }
}

/* export const getServerSideProps: GetServerSideProps = async context => {

	// const client = cacheClient()

	// await saveVisitor(context, client)

	// await client.quit()

	// get project members
	const members = await getAllProjectMembers()

	// console.log('members: ', members)

	if (!members) { return { props: { members: null } } }

	return { props: { members } }
} */

const About = ({ members }: { members: IProjectMember[] | null }) => <AboutPage members={members} />

export default About