import type { AppProps } from 'next/app'
import { ClubProvider } from '../src/context/clubContext'
import { ClubSearchProvider } from '../src/context/clubSearchContext'
import { BackdropProvider } from '../src/context/backdropContext'
// TODO CSS refactoring
// styles
import '../src/css/index.css'
import '../src/css/fade.css'
import '../src/css/main.css'
import '../src/css/header.css'
import '../src/css/homepage.css'
import '../src/css/clubOverview.css'
import '../src/css/leaderboards.css'
// import '../src/css/nav.css'
import '../src/css/promotion.css'
import '../src/css/searchClub.css'
import '../src/css/favClubs.css'
import '../src/css/pageBuilding.css'
import '../src/css/mediaQueries.css'
import { TransProvider } from '../src/context/transContext'


/* Axios: https://github.com/axios/axios
 * React: https://reactjs.org/docs/getting-started.html
 * React-use: https://github.com/streamich/react-use
 * React-Player: https://github.com/cookpete/react-player
 * Material-UI: https://github.com/mui-org/material-ui
 * Notifications: https://github.com/iamhosseindhv/notistack
 */

const MyApp = ({ Component, pageProps }: AppProps) => (
	<TransProvider>
		<ClubSearchProvider>
			<ClubProvider>
				<BackdropProvider>
					<Component {...pageProps} />
				</BackdropProvider>
			</ClubProvider>
		</ClubSearchProvider>
	</TransProvider>

)

export default MyApp