/**
 * @type {import('next').NextConfig}
 */

const nextConfig = {
	i18n: {
		// TODO translation
		//
		// 'it-IT' italian
		// 'hu-HU' hungarian
		// 'ar' arabic
		// 'es-ES' spanish
		// 'pt-PT' portuguese
		// 'ru-RU' russian
		// 'tr-TR' turkish
		locales: ['en-US', 'fr-FR', 'de-DE', 'nl-NL'],
		defaultLocale: 'en-US'
	},
	reactStrictMode: true,
	basePath: '',
	async headers() {
		return headersArr
	},
	poweredByHeader: false,
	images: {
		domains: [
			'localhost',
			'fifa21.content.easports.com',
			'fifa17.content.easports.com',
			'fifa22.content.easports.com',
			'img.shields.io'
		]
	}
}

module.exports = nextConfig

const headersArr = [
	{
		source: '/(.*)', // Apply these headers to all routes in your application.
		headers: [
			{
				key: 'Cache-Control',
				value: 's-maxage=240, stale-while-revalidate=59'
			}
		]
	}
]