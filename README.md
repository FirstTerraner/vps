<div align="center">
<p>
	<img src="https://gitlab.com/FirstTerraner/vps/-/raw/main/public/imgs/metaImg.png">
</p>
</div>
<!-- <p>
    <img width='200' src="https://www.virtualpro.space/imgs/appName.png">
</p> -->


<div align="center">
<h1>A browser based open source platform for the FIFA pro clubs community</h1>

[Homepage](https://www.virtualpro.space) | [Pre-release](https://vps-git-dev-firstterraner.vercel.app/) |
[Release notes](https://www.virtualpro.space/release) |
[About us](https://virtualpro.space/about) | [Contribute](https://virtualpro.space/contribute) |
[Donations](https://virtualpro.space/donations)

<!-- [![pipeline status][PipelineStatusBadge]][ProjectUrl]
[![coverage report][CoverageBadge]][ProjectUrl] -->

</div>

<div align="center">

<!-- [![Open Source Love](https://badges.frapsoft.com/os/v2/open-source.png?v=103)](https://github.com/ellerbrock/open-source-badges/) -->
<!-- [![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0) -->
<!-- [![TypeScript](https://badges.frapsoft.com/typescript/code/typescript.svg?v=101)](https://github.com/ellerbrock/typescript-badges/) -->
[![Discord][DiscordBadge]][DiscordUrl]
[![Twitter][TwitterBadge]][TwitterUrl]

</div>

[ProjectUrl]: https://gitlab.com/FirstTerraner/vps
<!-- [ProjectPage]: https://eclubstournaments.gitlab.io/frontend/vps -->

[PipelineStatusBadge]: https://gitlab.com/FirstTerraner/vps/badges/master/pipeline.svg
[CoverageBadge]: https://gitlab.com/FirstTerraner/vps/badges/master/coverage.svg

[DiscordBadge]: https://img.shields.io/discord/835310846480482314?label=Discord&logo=discord&logoColor=white
[TwitterBadge]: https://img.shields.io/twitter/follow/VirtualProSpace.svg?style=flatl&label=Follow&logo=twitter&logoColor=white&color=1da1f2

[DiscordUrl]: https://discord.gg/Y94sAMkCra
[TwitterUrl]: https://twitter.com/VirtualProSpace

# Contribute

<p>Pull requests are always welcome, please make sure to check out the [contribution](https://virtualpro.space/contribute) page.</p>

# Note

<p>Without the contribution of the open source community, this project would not exist.</p>

It is based on a repository created by [@ntaboada](https://github.com/ntaboada) a few years ago,
who got in touch with the developers of the website ["proclubshead.com"](https://proclubshead.com). They helped
back then by sharing the API endpoints they are using.
<br /><br />
Pro Clubs Head was a big inspiration in the creation process of VPS.
<br /><br />

[@billigsterUser](https://gitlab.com/billigsterUser) has forked, typed and added tests the repo created by @ntaboada for further use as a library in Virtual Pro Space. Have a look at the NPM registry: [proclubs-api](https://www.npmjs.com/package/proclubs-api) and at the [repository](https://gitlab.com/FirstTerraner/proclubs-api)

<p>So far, there are not many resources or even any documentation out there for any FIFA pro clubs API.</p>

<p>We are related to the kindness or lazyness of EA Sports and their API endpoints. We hope to be able to
catch up with them as soon as they change something in the data structure.
</p>

<p>With that beeing said, we really appreciate your help and if there is something we are missing, please let us know.</p>

<h3>Thanks a lot!<h3>
