/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { WrappedNodeRedisClient } from 'handy-redis'
import { TPlatformType } from 'proclubs-api/dist'
import { IClubMemberCareer, IClubMemberStats, IClubStats } from 'proclubs-api/dist/model/club'
import { IPkg, IRecClub, IVProAttr, TMatchesState } from './models/interfaces'
import { ISpecTrans, IPlayerPos, TLocale } from './models/translations'
import { isNum } from './models/typeGuards'
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
export const pkg: IPkg = require('../package.json')

export const appName = 'Virtual Pro Space'
export const appUrl = 'https://virtualpro.space'
export const metaImgSrc = `${appUrl}/imgs/metaImg.png`
export const metaAppLogo = `${appUrl}/imgs/appLogo.png`

export const deciToHex = (deci: number) => {
	if (deci === 0) { return '000' }
	// console.log(deci.toString(16))
	return deci.toString(16)
}

export const clubCrestUrl = (crestId?: number) => {
	// if (!club) { return '' }
	// const id = club.customKit.isCustomTeam === '0' ? club.teamId : +club.customKit.crestAssetId
	if (!crestId) { return '' }
	return `https://fifa21.content.easports.com/fifa/fltOnlineAssets/05772199-716f-417d-9fe0-988fa9899c4d/2021/fifaweb/crests/256x256/l${crestId}.png`
}

export const nationUrl = (nationId?: string) => {
	if (!nationId) { return '' }
	return `https://fifa17.content.easports.com/fifa/fltOnlineAssets/CC8267B6-0817-4842-BB6A-A20F88B05418/2017/fut/items/images/flags/html5/35x22/${nationId}.png`
}

export const getMemberPosCount = (members: { favoritePosition: string }[] | null) => {
	const result = members?.map(x => x?.favoritePosition)?.reduce?.((a, v) => {
		a[v] = !a[v] ? 1 : a[v] + 1
		return a
	}, {} as { [key: string]: number })
	return {
		front: result?.['forward'] || 0,
		mid: result?.['midfielder'] || 0,
		def: result?.['defender'] || 0,
		gk: result?.['goalkeeper'] || 0
	}
}

export const ratingColor = (rating: number) => {
	if (rating <= 4) { return 'bgRed whiteTxt' }
	if (rating <= 6 && rating > 4) { return 'bgOrange whiteTxt' }
	if (rating <= 7 && rating > 6) { return 'bgYellow whiteTxt' }
	if (rating <= 8 && rating > 7) { return 'bgLightGreen whiteTxt' }
	if (rating <= 9 && rating > 8) { return 'bgGreen whiteTxt' }
	if (rating <= 10 && rating > 9) { return 'bgDarkGreen whiteTxt' }
	return ''
}

export function isKeyOf<T>(thing: T, key: string | number | symbol): key is keyof T {
	return Object.keys(thing).includes(key.toString())
}
const SCORE_COLOR_WTL = { '2': 'bgGreen', '1': 'bgYellow', '0': 'bgRed' } as const
export const scoreColorWTL = (val: string) => {
	// console.log('scoreColorWTL val: ', val)
	// console.log('isKeyOf(SCORE_COLOR_WTL, val): ', isKeyOf(SCORE_COLOR_WTL, val))
	if (isKeyOf(SCORE_COLOR_WTL, val)) {
		return SCORE_COLOR_WTL[val]
	}
	return 'bgGrey'
}

export const endResultColor = (resultA: number, resultB: number) => {
	if (resultA === resultB) { return 'bgYellow' }
	if (resultA < resultB) { return 'bgRed' }
	if (resultA > resultB) { return 'bgGreen' }
	return 'bgGrey'
}

export const validVal = (val: number) => isNum(val) && val > 0 ? val.toFixed(1) : 0
// const SCORE_COLOR = { ...SCORE_COLOR_WTL, '-1': 'bgGrey'} as const
// export const scoreColor = (val: string) => isKeyOf(SCORE_COLOR_WTL, val) ? SCORE_COLOR_WTL[val] : 'bgGrey'
const WTL_CHARS = { '2': 'W', '1': 'D', '0': 'L' } as const
export const wdlChar = (val: string) => isKeyOf(WTL_CHARS, val) ? WTL_CHARS[val] : ''

/**
 *
 * @deprecated
 *
 */
export const lastScores = (clubRank: IClubStats | null, i: number) => {
	if (!clubRank) { return '' }
	// because we reverse the original response
	switch (i) {
		case 9: return clubRank.lastMatch0
		case 8: return clubRank.lastMatch1
		case 7: return clubRank.lastMatch2
		case 6: return clubRank.lastMatch3
		case 5: return clubRank.lastMatch4
		case 4: return clubRank.lastMatch5
		case 3: return clubRank.lastMatch6
		case 2: return clubRank.lastMatch7
		case 1: return clubRank.lastMatch8
		case 0: return clubRank.lastMatch9
		default: return ''
	}
}

export const getDivOverview = (division?: number) => {
	if (!division) { return null }
	switch (division) {
		case 10: return [{ value: 9, label: 'P:9' }, { value: 12, label: 'T:12' }]
		case 9: return [{ value: 6, label: 'H:6' }, { value: 10, label: 'P:10' }, { value: 13, label: 'T:13' }]
		case 8: return [{ value: 8, label: 'H:8' }, { value: 12, label: 'P:12' }, { value: 15, label: 'T:15' }]
		case 7: return [{ value: 8, label: 'H:8' }, { value: 14, label: 'P:14' }, { value: 17, label: 'T:17' }]
		case 6: return [{ value: 10, label: 'H:10' }, { value: 16, label: 'P:16' }, { value: 19, label: 'T:19' }]
		case 5: return [{ value: 10, label: 'H:10' }, { value: 16, label: 'P:16' }, { value: 19, label: 'T:19' }]
		case 4: return [{ value: 10, label: 'H:10' }, { value: 16, label: 'P:16' }, { value: 19, label: 'T:19' }]
		case 3: return [{ value: 12, label: 'H:12' }, { value: 18, label: 'P:18' }, { value: 21, label: 'T:21' }]
		case 2: return [{ value: 12, label: 'H:12' }, { value: 18, label: 'P:18' }, { value: 21, label: 'T:21' }]
		case 1: return [{ value: 14, label: 'H:14' }, { value: 21, label: 'T:21' }]
		default: return null
	}
}

const defaultProAttr = {
	acceleration: '',
	aggression: '',
	agility: '',
	attackPosition: '',
	balance: '',
	ballControl: '',
	crossing: '',
	curve: '',
	dribbling: '',
	finishing: '',
	freekickAccuracy: '',
	gkDiving: '',
	gkHandling: '',
	gkKicking: '',
	gkReflexes: '',
	headingAccuracy: '',
	interceptions: '',
	jumping: '',
	longPass: '',
	longShots: '',
	marking: '',
	penalties: '',
	reactions: '',
	shortPass: '',
	shotPower: '',
	slideTackle: '',
	sprintSpeed: '',
	stamina: '',
	standTackle: '',
	strength: '',
	vision: '',
	volleys: ''
}

// ea api attribute string example: '099|098|074|085|0...'
export const attrStrToObj = (attr: string) => {
	console.log('attr str: ', attr)
	if (attr.length < 1 || attr === 'NH') { return null }
	const attrArr = attr.split('|')
	// remove last empty string
	attrArr.pop()
	// remove first zero from strings
	const cleanAttrArr = attrArr.map(att => att.substring(1))
	// console.log('cleanAttrArr: ', cleanAttrArr)
	// following v-pro attributes in proper order to match EA Attribute string
	defaultProAttr.acceleration = cleanAttrArr[0]
	defaultProAttr.sprintSpeed = cleanAttrArr[1]
	defaultProAttr.agility = cleanAttrArr[2]
	defaultProAttr.balance = cleanAttrArr[3]
	defaultProAttr.jumping = cleanAttrArr[4]
	defaultProAttr.stamina = cleanAttrArr[5]
	defaultProAttr.strength = cleanAttrArr[6]
	defaultProAttr.reactions = cleanAttrArr[7]
	defaultProAttr.aggression = cleanAttrArr[8]
	// cleanAttrArr[9] not found
	defaultProAttr.interceptions = cleanAttrArr[10]
	defaultProAttr.attackPosition = cleanAttrArr[11]
	defaultProAttr.vision = cleanAttrArr[12]
	defaultProAttr.ballControl = cleanAttrArr[13]
	defaultProAttr.crossing = cleanAttrArr[14]
	defaultProAttr.dribbling = cleanAttrArr[15]
	defaultProAttr.finishing = cleanAttrArr[16]
	defaultProAttr.freekickAccuracy = cleanAttrArr[17]
	defaultProAttr.headingAccuracy = cleanAttrArr[18]
	defaultProAttr.longPass = cleanAttrArr[19]
	defaultProAttr.shortPass = cleanAttrArr[20]
	defaultProAttr.marking = cleanAttrArr[21]
	defaultProAttr.shotPower = cleanAttrArr[22]
	defaultProAttr.longShots = cleanAttrArr[23]
	defaultProAttr.standTackle = cleanAttrArr[24]
	defaultProAttr.slideTackle = cleanAttrArr[25]
	defaultProAttr.volleys = cleanAttrArr[26]
	defaultProAttr.curve = cleanAttrArr[27]
	defaultProAttr.penalties = cleanAttrArr[28]
	defaultProAttr.gkDiving = cleanAttrArr[29]
	defaultProAttr.gkHandling = cleanAttrArr[30]
	defaultProAttr.gkKicking = cleanAttrArr[31]
	defaultProAttr.gkReflexes = cleanAttrArr[32]
	return defaultProAttr
}

// Skill points are rewarded by playing league and cup matches
// they increase the pro attributes
export const getSkillPoints = (matchesPlayed: number) => {
	// console.log('matches played: ', matchesPlayed)
	if (!matchesPlayed || matchesPlayed < 1) { return 15 }
	if (matchesPlayed >= 1 && matchesPlayed <= 3) { return 18 }
	if (matchesPlayed > 3 && matchesPlayed <= 5) { return 21 }
	if (matchesPlayed > 5 && matchesPlayed <= 7) { return 26 }
	if (matchesPlayed > 7 && matchesPlayed <= 10) { return 29 }
	if (matchesPlayed > 10 && matchesPlayed <= 12) { return 34 }
	if (matchesPlayed > 12 && matchesPlayed <= 15) { return 37 }
	if (matchesPlayed > 15 && matchesPlayed <= 20) { return 40 }
	if (matchesPlayed > 20 && matchesPlayed <= 23) { return 43 }
	if (matchesPlayed > 23 && matchesPlayed <= 25) { return 46 }
	if (matchesPlayed > 25 && matchesPlayed <= 30) { return 51 }
	if (matchesPlayed > 30 && matchesPlayed <= 33) { return 54 }
	if (matchesPlayed > 33 && matchesPlayed <= 37) { return 57 }
	if (matchesPlayed > 37 && matchesPlayed <= 40) { return 60 }
	if (matchesPlayed > 40 && matchesPlayed <= 45) { return 63 }
	if (matchesPlayed > 45 && matchesPlayed <= 50) { return 66 }
	if (matchesPlayed > 50 && matchesPlayed <= 53) { return 71 }
	if (matchesPlayed > 53 && matchesPlayed <= 60) { return 74 }
	if (matchesPlayed > 60 && matchesPlayed <= 65) { return 78 }
	if (matchesPlayed > 65 && matchesPlayed <= 70) { return 81 }
	if (matchesPlayed > 70 && matchesPlayed <= 75) { return 86 }
	if (matchesPlayed > 75 && matchesPlayed <= 80) { return 89 }
	if (matchesPlayed > 80 && matchesPlayed <= 85) { return 94 }
	if (matchesPlayed > 85 && matchesPlayed <= 90) { return 97 }
	if (matchesPlayed > 90 && matchesPlayed <= 95) { return 102 }
	if (matchesPlayed > 95 && matchesPlayed < 100) { return 105 }
	if (matchesPlayed >= 100) { return 110 }
	return 0
}

export const getAttrColor = (attr: number) => {
	if (attr < 35) { return 'bgRed' }
	if (attr >= 35 && attr < 60) { return 'bgOrange' }
	if (attr >= 60 && attr < 75) { return 'bgYellow' }
	if (attr >= 75 && attr < 90) { return 'bgLightGreen' }
	if (attr >= 90) { return 'bgGreen' }
	return 'bgGrey'
}

export const wasPlayerPresentLastMatches = (
	matches: TMatchesState,
	playerName: string,
	clubId: string
) => {
	if (!matches) { return false }
	for (const match of matches) {
		for (const player of Object.values(match.players[clubId])) {
			if (player.playername === playerName) {
				return true
			}
		}
	}
	return false
}

export const getPlayerAttr = (matches: TMatchesState, playerName: string, clubId: string) => {
	if (!matches || !playerName || !clubId) { return '' }
	for (const match of matches) {
		if (match) {
			for (const player of Object.values(match.players[clubId])) {
				if (player.playername === playerName) {
					return player.vproattr
				}
			}
		}
	}
	return ''
}

export const getAllPlayersFromLastMatches = (matches: TMatchesState, clubId: number) => {
	const playerArr: IClubMemberCareer[] = []
	if (!matches || !clubId) { return playerArr }
	for (const match of matches) {
		if (match) {
			for (const player of Object.values(match.players[`${clubId}`])) {
				// console.log('player pos: ', player.pos)
				if (!playerArr.some(entry => entry.name === player.playername)) {
					playerArr.push({
						assists: player.assists,
						favoritePosition: player.pos,
						gamesPlayed: '',
						goals: '',
						manOfTheMatch: '',
						name: player.playername,
						proPos: player.pos,
						ratingAve: ''
					})
				}
			}
		}
	}
	return playerArr
}

// specialities src: https://proclubshead.com/21/club/ps4-216353/player/Arsinho-25DK/form-league/#player-attributes
// Poacher * - indicates players with Finishing 85+, Heading accuracy 85+, Attacking work rate low or medium
// Sniper - indicates players with Long shots 87+, Shot power 87+
// Clinical finisher - indicates players with Long shots 80+, Finishing 86+
// Aerial threat - indicates players with Heading accuracy 75+, Jumping 85+, Strength 85+, Height 1.88m+
// Free kick specialist - indicates players with Free kick accuracy 86+, Curve 85+, Shot power 85+
// Dribbler - indicates players with Dribbling 86+, Balance 75+
// Playmaker - indicates players with Short pass 86+, Long pass +73, Vision 86+
// Crosser - indicates players with Crossing 86+, Curve 80+
// Engine * - indicates players with Stamina 86+, Attacking work rate high, Defensive work rate high
// Tackler - indicates players with Stand tackle 86+, Slide tackle 85+
// Speedster - indicates players with Sprint speed 90+, Acceleration 90+
// Strength * - indicates players with Strength 86+, Weight 83kg+
// Tactician - indicates players with Interceptions 86+, Reactions 80+
// Acrobat - indicates players with Agility 86+, Reactions 80+

// Complete Forward ** - indicates players with Poacher specialty and
// at least 2 of: Clinical finisher, Speedster, Aerial threat, Dribbler, Strength
// or with Clinical finisher and at least 2 of: Speedster, Aerial threat, Dribbler, Strength

// Complete Midfielder ** - indicates players with Playmaker specialty and
// at least 2 of: Engine, Crosser, Dribbler, Distance shooter, Free kick specialist, Clinical finisher, Tackler

// Complete Defender ** - indicates players with Tackler specialty and at least 1 of: Acrobat, Strength, Aerial threat

// * is not shown because not all attributes are available
// ** does not take every specialty into account because some of them are not available
export const getPlayerSpecials = (specTrans: ISpecTrans, attrObj: IVProAttr | null, proHeight?: number) => {
	console.log('attributes obj: ', attrObj)
	if (!attrObj || !proHeight) { return [] }
	const playerSpecialsArr: string[] = []
	if (+attrObj.longShots >= 87 && +attrObj.shotPower >= 87) {
		playerSpecialsArr.push(specTrans.sniper)
	}
	if (+attrObj.longShots >= 80 && +attrObj.finishing >= 86) {
		playerSpecialsArr.push(specTrans.finisher)
	}
	if (+attrObj.headingAccuracy >= 75 && +attrObj.jumping >= 85 && +attrObj.strength >= 85 && proHeight >= 188) {
		playerSpecialsArr.push(specTrans.aerialThreat)
	}
	if (+attrObj.freekickAccuracy >= 86 && +attrObj.curve >= 85 && +attrObj.shotPower >= 85) {
		playerSpecialsArr.push(specTrans.freeKick)
	}
	if (+attrObj.dribbling >= 86 && +attrObj.balance >= 75) {
		playerSpecialsArr.push(specTrans.dribbler)
	}
	if (+attrObj.shortPass >= 86 && +attrObj.longPass >= 73 && +attrObj.vision >= 86) {
		playerSpecialsArr.push(specTrans.playmaker)
	}
	if (+attrObj.crossing >= 86 && +attrObj.curve >= 80) {
		playerSpecialsArr.push(specTrans.crosser)
	}
	if (+attrObj.standTackle >= 86 && +attrObj.slideTackle >= 85) {
		playerSpecialsArr.push(specTrans.tackler)
	}
	if (+attrObj.sprintSpeed >= 90 && +attrObj.acceleration >= 85) {
		playerSpecialsArr.push(specTrans.speedster)
	}
	if (+attrObj.interceptions >= 86 && +attrObj.reactions >= 80) {
		playerSpecialsArr.push(specTrans.tactician)
	}
	if (+attrObj.agility >= 86 && +attrObj.reactions >= 80) {
		playerSpecialsArr.push(specTrans.acrobat)
	}
	// indicates players with finisher speciality and at least 2 of: Speedster, Aerial threat, Dribbler, Strength
	if (playerSpecialsArr.includes(specTrans.finisher)) {
		const required = [specTrans.speedster, specTrans.aerialThreat, specTrans.dribbler, specTrans.finisher]
		let matchCount = 0
		for (const el of required) {
			if (playerSpecialsArr.includes(el) && matchCount < 2) {
				required.splice(required.indexOf(el), 1)
				matchCount++
			}
			if (matchCount === 2) {
				playerSpecialsArr.push(specTrans.compForward)
				break
			}
		}
		matchCount = 0
	}
	// indicates players with Playmaker specialty and at least 2 of:
	// Engine, Crosser, Dribbler, Distance shooter, Free kick specialist, Clinical finisher, Tackler
	if (playerSpecialsArr.includes(specTrans.playmaker)) {
		const required = [specTrans.crosser, specTrans.dribbler, specTrans.sniper, specTrans.freeKick, specTrans.finisher, specTrans.tackler]
		let matchCount = 0
		for (const el of required) {
			if (playerSpecialsArr.includes(el) && matchCount < 2) {
				required.splice(required.indexOf(el), 1)
				matchCount++
			}
			if (matchCount === 2) {
				playerSpecialsArr.push(specTrans.compMid)
				break
			}
		}
		matchCount = 0
	}
	// indicates players with Tackler specialty and at least 1 of: Acrobat, Strength, Aerial threat
	if (playerSpecialsArr.includes(specTrans.tackler) && playerSpecialsArr.some(special => [specTrans.acrobat, specTrans.aerialThreat].indexOf(special) > -1)) {
		playerSpecialsArr.push(specTrans.compDef)
	}
	return playerSpecialsArr
}
// const cleanAttrs = attrStrToObj(match.players[clubId][player].vproattr)
// const proSpecials = getPlayerSpecials(cleanAttrs, 160)
// console.log('VPro Specials: ', proSpecials)

export const getRecPoints = (wins: number, ties: number) => wins * 3 + ties

export const sortRecord = (recArray: IRecClub[]) => {
	recArray.sort((a, b) => {
		const pointsA = +a.wins * 3 + +a.ties
		const pointsB = +b.wins * 3 + +b.ties
		const goalDiffA = +a.alltimeGoals - +a.alltimeGoalsAgainst
		const goalDiffB = +b.alltimeGoals - +b.alltimeGoalsAgainst
		// -1 more points
		if (pointsA > pointsB) { return -1 }
		// -1 same points, more goals
		if (pointsA === pointsB && goalDiffA > goalDiffB) { return -1 }
		// 0 same points, same goals, less goalsAgainst
		if (pointsA === pointsB && goalDiffA === goalDiffB) { return 0 }
		// else
		return 1
	})
}

/**
 * @param date1 represents today f.e: (new Date())
 * @param date2 represents the past date f.e: (new Date(2021, 0, 1)) *month idx starts at 0
 * @returns day difference number between date1 & date2
 */
const daysAgo = (date1: Date, date2: Date) => {
	const oneDayInMs = 86400000
	return Math.ceil(Math.abs(date1.getTime() - date2.getTime()) / oneDayInMs)
}

const daysAgoStr = (days: number) => {
	console.log('days ago: ', days)
	days = days - 1
	if (days === 0) { return 'today.' }
	if (days === 1) { return 'a day ago.' }
	if (days > 1 && days <= 7) { return `${days} days ago.` }
	if (days > 7 && days <= 14) { return 'over a week ago.' }
	if (days > 14 && days <= 31) { return 'over two weeks ago.' }
	if (days > 31 && days <= 183) { return 'over a month ago.' }
	if (days > 183 && days <= 366) { return 'over six month ago.' }
	if (days > 366 && days <= 732) { return 'over a year ago.' }
	if (days > 732) { return 'over two years ago.' }
	return ''
}

/**
 * @param date1 represents today f.e: (new Date())
 * @param date2 represents the past date f.e: (new Date(2021, 0, 1)) *month idx starts at 0
 * @returns day difference string between date1 & date2
 */
export const getDaysAgoString = (date1: Date, date2: Date) => daysAgoStr(daysAgo(date1, date2))

// TODO finish position enum
/**
 * @param trans translation object in the router language 
 * @param pos the position notation from EA API
 * @returns translated player position
 */
export const getPos = (trans: IPlayerPos, pos?: string) => {
	if (!pos) { return '' }
	switch (pos) {
		case '0': return trans.gk
		case '2': return trans.rwb
		case '3': return trans.rb
		case '5': return trans.cb
		case '7': return trans.lb
		case '8': return trans.lwb
		case '10': return trans.cdm
		case '14': return trans.cm
		case '15': return trans.rm
		case '16': return trans.lm
		case '18': return trans.cam
		case '20': return trans.rf // right front
		case '21': return trans.cf // central front
		case '22': return trans.lf // left front
		case '23': return trans.rw // right wing
		case '24': return trans.lw // left wing
		case '25': return trans.st // sturm
		// LW occurs 2 times
		case '27': return trans.lw // left wing
		case '': return 'N/A'
		default: return 'X'
	}
}

export const getPosColor = (pos?: string) => {
	if (!pos) { return 'bgWhite' }
	switch (pos) {
		case 'midfielder': return 'bgGreen'
		case 'goalkeeper': return 'bgOrange'
		case 'defender': return 'bgYellow'
		case 'forward': return 'bgBlue'
		case 'Any': return 'bgWhite'
		default: return 'bgWhite'
	}
}

export const getRegionFromId = (id: number | undefined) => {
	if (!id) { return 'Region not available' }
	switch (id.toString()) {
		case '5719381': return 'Western Europe'
		case '5457237': return 'Southern Europe'
		case '5456205': return 'South America'
		case '4344147': return 'British Isles'
		case '4539733': return 'Eastern Europe'
		case '5723475': return 'West Coast US'
		case '4543827': return 'East Coast US'
		case '5129557': return 'Northern Europe'
		default: return id.toString()
	}
}

interface Props { name: string, proPos: string }

export const sortMemberCareerOrStatsByPos = <T extends Props[]>(members: T | null) =>
	!members?.length ? null : members.sort((a, b) =>
		+a.proPos === +b.proPos ? a.name.localeCompare(b.name) : +a.proPos - +b.proPos)


export const sortMemberCareerOrStatsByPos2 = <T extends Props[], T2 extends Props[]>(a: T | null, b: T2 | null) => {
	if (!a?.length || !b?.length || a.length !== b.length) { return null }
	const aNames = Array.from(new Set<string>(a.map(x => x.name)))
	const bNames = Array.from(new Set<string>(b.map(x => x.name)))
	if (aNames.length !== bNames.length || !aNames.every(x => bNames.includes(x)) || !bNames.every(x => aNames.includes(x))) { return null }
	const bCopy = [...b]
	for (let i = 0; i < a.length; i++) {
		const tmp = b.findIndex(x => x.name === a[i].name)
		if (tmp > -1) { bCopy[i] = { ...b[tmp] } }
	}
	return bCopy
}

export const upperCasePlatform = (plat?: TPlatformType | null) => {
	if (!plat) {
		return null
	}
	if (plat === 'xboxone') { return 'XB1' }
	if (plat === 'xbox-series-xs') { return 'XBSXS' }
	return plat.toUpperCase()
}

export const cleanPlatform = (plat?: TPlatformType | null) => {
	if (!plat) { return '' }
	switch (plat) {
		case 'ps4': return 'Playstation 4'
		case 'ps5': return 'Playstation 5'
		case 'xboxone': return 'Xbox One'
		case 'xbox-series-xs': return 'Xbox Series X/S'
		case 'pc': return 'PC'
		default: return plat
	}
}

export const getSingleMemberObj = (name: string, mCareer?: IClubMemberCareer[] | null, mStats?: IClubMemberStats[] | null) => {
	const memberCareer = mStats?.filter(player => player.name === name)[0]
	const memberStats = mCareer?.filter(player => player.name === name)[0]
	return {
		career: memberCareer && memberCareer.name === name ? memberCareer : null,
		stats: memberStats && memberStats.name === name ? memberStats : null
	}
}

export const getLangFromPathLocale = (locale?: TLocale) => {
	if (locale === 'de-DE') { return '🇩🇪 Deutsch' }
	if (locale === 'fr-FR') { return '🇫🇷 Français' }
	if (locale === 'nl-NL') { return '🇳🇱 Nederlands' }
	return '🇬🇧 English'
}
export const getUnixTimestamp = () => Math.floor(Date.now() / 1000)

export const setHighestRec = async (
	allRecClubs: IRecClub[],
	client: WrappedNodeRedisClient
) => {

	// sort array from highest record to lowest
	sortRecord(allRecClubs)

	/* allRecClubs.sort((a, b) => {
		const pointsA = +a.wins * 3 + +a.ties
		const pointsB = +b.wins * 3 + +b.ties
		const goalDiffA = +a.alltimeGoals - +a.alltimeGoalsAgainst
		const goalDiffB = +b.alltimeGoals - +b.alltimeGoalsAgainst
		// -1 more points
		if (pointsA > pointsB) { return -1 }
		// -1 same points, more goals
		if (pointsA === pointsB && goalDiffA > goalDiffB) { return -1 }
		// 0 same points, same goals
		if (pointsA === pointsB && goalDiffA === goalDiffB) { return 0 }
		// else
		return 1
	}) */

	const highest: IRecClub = allRecClubs[0]

	console.log('highest: ', highest.name)
	console.log('record: ', highest.wins)
	// const max = allRecClubs.reduce((prev, current) => (+prev.wins < +current.wins && +prev.points < +current.points) ? prev : current)
	// console.log('max rec: ', max)
	await client.set('highest-record', JSON.stringify(highest))
	await client.set('record-updated', getUnixTimestamp().toString())
	console.log('done')

}

export const mlsToMS = (ms?: number) => {
	if (!ms) { return null }
	// 1- Convert to seconds:
	let seconds = ms / 1000
	seconds = seconds % 3600 // seconds remaining after extracting hours
	// 3- Extract minutes:
	const minutes = seconds / 60  // 60 seconds in 1 minute
	// 4- Keep only seconds not extracted to minutes:
	seconds = seconds % 60
	return `${parseInt(minutes.toString())}:${parseInt(seconds.toString())}`
}

export const getRecordUpdateInfo = async (client: WrappedNodeRedisClient) => {
	const milSectoWait = 1000 * 60 * 15 // 15 mins in milisecs
	const recUpdated = await client.get('record-updated')
	// console.log('recUpdated: ', recUpdated)
	if (recUpdated && +recUpdated > 0) {
		const lastUpdateTime = +recUpdated * 1000
		// console.log((new Date(lastUpdateTime)).toLocaleString())
		const milSecSinceLastUpdate = Date.now() - lastUpdateTime
		const milSecToWaitLeft = milSectoWait - milSecSinceLastUpdate
		// console.log('time', { lastUpdateStart: lastUpdateTime }, { lastUpdateStartDate: new Date(lastUpdateTime) })
		return {
			lastUpdateTime,
			milSecToWaitLeft
		}
	}
	return null
}