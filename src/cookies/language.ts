import { TLocale } from '../models/translations'

export const setLanguageCookie = (lang: TLocale | null, days: number) => {
	let expires = ''
	if (days) {
		const date = new Date()
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
		expires = '; expires=' + date.toUTCString()
	}
	document.cookie = `NEXT_LOCALE=${lang || 'en-US'}${expires}; SameSite=Lax; Secure; path=/`
}

export const getLanguageCookie = () => {
	const nameEQ = 'NEXT_LOCALE='
	const ca = document.cookie.split(';')
	// console.log('ca: ', ca)
	for (let i = 0; i < ca.length; i++) {
		let c = ca[i]
		while (c.charAt(0) === ' ') {
			c = c.substring(1, c.length)
		}
		if (c.indexOf(nameEQ) === 0) {
			return c.substring(nameEQ.length, c.length)
		}
	}
	return null
}