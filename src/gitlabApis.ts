import axios from 'axios'
import { IProjectMember } from './models/interfaces'

// this file closes issue #21
const projectID = '29292866'

const baseReq = axios.create({ baseURL: 'https://gitlab.com/api/v4', timeout: 5000, headers: { 'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN || '' } })

const fetchRoute = async <T>(route: string) => {
	try {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		// const cached: T = cache.get(route)
		// console.log('frontend cached: ', cached)
		// if (cached) {
		// 	return cached
		// }
		// TODO add cancelTokenSource -> get(url, {})
		const resp = await baseReq.get<T>(route)
		// console.log('response in cacheFetch: ', resp)
		// bad status code or null resp
		if ((resp?.status && (resp?.status < 200 || resp?.status > 299)) || !resp?.data) { return null }
		// add to cache
		// cache.put(route, resp.data, 1000 * 60 * 3) // 3mins
		return resp.data
	} catch (e) { console.error('url: ', route, 'error: ', e) }
	return null
}
// for more gitlab endpoints visit: https://docs.gitlab.com/ee/api/api_resources.html

// https://docs.gitlab.com/ee/api/projects.html#get-single-project
export const getGitlabProject = async () => {
	const resp = await fetchRoute<IProjectMember[]>(`/projects/${projectID}`)
	if (!resp) { return null }
	return resp
}

// https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project
export const getAllProjectMembers = async () => {
	const resp = await fetchRoute<IProjectMember[]>(`/projects/${projectID}/members`)
	if (!resp) { return null }
	return resp
}
