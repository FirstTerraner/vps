import { createNodeRedisClient, WrappedNodeRedisClient } from 'handy-redis'
import { GetServerSidePropsContext } from 'next'
import { TPlatformType } from 'proclubs-api/dist'
import { ParsedUrlQuery } from 'querystring'
import { IRecClub, IVisitor } from './models/interfaces'
import { cTo, isStr, toJsonStr } from './models/typeGuards'

export const cacheClient = () => createNodeRedisClient(process.env.REDIS_URL || '')

export const saveVisitor = async (context: GetServerSidePropsContext<ParsedUrlQuery>, client: WrappedNodeRedisClient) => {

	const oneDaysInS = 60 * 60 * 24

	const userAgent = context.req ? context.req.headers['user-agent'] : navigator.userAgent

	const ip = context.req?.headers['x-forwarded-for']?.toString()

	if (!ip || !userAgent) { return }

	const date = new Date()

	const dateStr = date.toLocaleDateString()

	console.log('dateStr: ', dateStr)

	// await client.flushall()

	// await client.set('AT', '211')

	// const all = await client.scan(0)

	// console.log('all_: ', all)

	const visitorsEntry = await client.get(dateStr)

	if (!visitorsEntry) {
		console.log('no visitors today, set new entry with date: ', dateStr)
		const str = toJsonStr([{ ip, a: userAgent, d: date }])
		if (isStr(str) && str.length > 0) {
			await client.setex(dateStr, oneDaysInS, str)
		}
		// delete visitors from previous day

		return
	}

	const visitors = cTo<IVisitor[] | null>(visitorsEntry)

	if (!visitors) { return }

	for (const visitor of visitors) {
		if (visitor.ip === ip && visitor.a === userAgent) { return }
	}

	visitors.push({ ip, a: userAgent, d: date })

	const visitorStr = toJsonStr(visitors)

	await Promise.all([
		client.setex(dateStr, oneDaysInS, visitorStr || '[]'),
		client.incr('AT')
	])

}

export const getRecClubs = async (client: WrappedNodeRedisClient, platform: TPlatformType) => {
	// record leaders
	const recClubsStr = await client.get(`${platform.toString()}-record`)
	const recClubs = cTo<IRecClub[] | null>(recClubsStr)

	if (!recClubs) { return null }

	// console.log(`${platform.toString()} record clubs entries: `, recClubs)

	// recClubs.sort((a, b) => {
	// 	if (+a.wins > +b.wins) { return -1 }
	// 	if (+a.wins === +b.wins && +a.ties === +b.ties) {
	// 		return 0
	// 	}
	// 	return 1
	// })

	return recClubs
}

export const getRecClubById = async (client: WrappedNodeRedisClient, platform: TPlatformType, id: string) => {
	const recClubs = await getRecClubs(client, platform)
	return recClubs?.filter(rec => rec.clubId === id)
}