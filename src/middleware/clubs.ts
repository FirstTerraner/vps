/* eslint-disable @typescript-eslint/restrict-template-expressions */
import { NextApiRequest, NextApiResponse } from 'next'
import {
	isPlatformType
} from 'proclubs-api/dist'
import { isGametype } from 'proclubs-api/dist/model/club'
import { isKeyof, isNum, isStr, toJsonStr } from '../models/typeGuards'

export type TReqHandler<T = any> = (
	req: NextApiRequest,
	res: NextApiResponse<T>
) => Promise<void> | void

// bad/no response function
export const badOrNoResp = (
	key: string,
	query: { [key: string]: string | string[] },
	isBad = false
) => `${isBad ? 'bad ' : 'no '}${key} - ${toJsonStr(query)}`

export const checkPlatform =
	(handler: TReqHandler) => (req: NextApiRequest, res: NextApiResponse) => {
		const key = 'platform'
		console.log('[checkPlatform] req.query: ', req.query)
		if (!req.query || !isKeyof(req.query, key)) {
			return res.status(500).send(badOrNoResp(key, req.query))
		}
		const platform = req.query[key]
		if (!isStr(platform) || !isPlatformType(platform)) {
			return res.status(500).send(badOrNoResp(key, req.query, true))
		}
		return handler(req, res)
	}

export const checkName =
	(handler: TReqHandler) => (req: NextApiRequest, res: NextApiResponse) => {
		const key = 'name'
		console.log('[checkName] req.query: ', req.query)
		if (!req.query || !isKeyof(req.query, key)) {
			return res.status(500).send(badOrNoResp(key, req.query))
		}
		if (!isStr(req.query[key])) {
			return res.status(500).send(badOrNoResp(key, req.query, true))
		}
		return handler(req, res)
	}

export const checkGametype =
	(handler: TReqHandler) => (req: NextApiRequest, res: NextApiResponse) => {
		const key = 'gameType'
		if (!req.query || !isKeyof(req.query, key)) {
			return res.status(500).send(badOrNoResp(key, req.query))
		}
		if (!isStr(req.query[key]) || !isGametype(req.query[key])) {
			return res.status(500).send(badOrNoResp(key, req.query, true))
		}
		return handler(req, res)
	}

export const checkClubId =
	(handler: TReqHandler) => (req: NextApiRequest, res: NextApiResponse) => {
		const key = 'id'
		if (!req.query || !isKeyof(req.query, key)) {
			return res.status(500).send(badOrNoResp(key, req.query))
		}
		if (!isNum(+req.query[key])) {
			return res.status(500).send(badOrNoResp(key, req.query, true))
		}
		return handler(req, res)
	}
