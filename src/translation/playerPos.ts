export const playerPositionTrans = {
	'it-IT': {
		gk: '',
		cb: '',
		rwb: '',
		rb: '',
		lwb: '',
		lb: '',
		cdm: '',
		cm: '',
		lm: '',
		rm: '',
		cam: '',
		rw: '', // right wing
		lw: '', // left wing
		cf: '', // central front
		rf: '', // right front
		lf: '', // left front
		st: '' // attacker
	},
	'hu-HU': {
		gk: '',
		cb: '',
		rwb: '',
		rb: '',
		lwb: '',
		lb: '',
		cdm: '',
		cm: '',
		lm: '',
		rm: '',
		cam: '',
		rw: '', // right wing
		lw: '', // left wing
		cf: '', // central front
		rf: '', // right front
		lf: '', // left front
		st: '' // attacker
	},
	'en-US': {
		gk: 'GK',
		cb: 'CB',
		rwb: 'RWB',
		rb: 'RB',
		lwb: 'LWB',
		lb: 'LB',
		cdm: 'CDM',
		cm: 'CM',
		lm: 'LM',
		rm: 'RM',
		cam: 'CAM',
		rw: 'RW', // right wing
		lw: 'LW', // left wing
		cf: 'CF', // central front
		rf: 'RF', // right front
		lf: 'LF', // left front
		st: 'ST' // attacker
	},
	'de-DE': {
		gk: 'TW',
		cb: 'IV',
		rwb: 'RAV',
		rb: 'RV',
		lwb: 'LAV',
		lb: 'LV',
		cdm: 'ZDM',
		cm: 'ZM',
		lm: 'LM',
		rm: 'RM',
		cam: 'ZOM',
		rw: 'RF', // right wing
		lw: 'LF', // left wing
		cf: 'MS', // central front
		rf: 'RS', // right front
		lf: 'LS', // left front
		st: 'ST' // attacker
	},
	'fr-FR': {
		gk: 'G',
		cb: 'DC',
		rwb: 'DDA',
		rb: 'DD',
		lwb: 'DGA',
		lb: 'DG',
		cdm: 'MDC',
		cm: 'MC',
		lm: 'MD',
		rm: 'MR',
		cam: 'MOC',
		rw: 'AD', // right wing
		lw: 'AG', // left wing
		cf: 'AT', // central front
		rf: 'AVD', // right front
		lf: 'AVG', // left front
		st: 'BU' // attacker
	},
	'nl-NL': {
		gk: 'DM',
		cb: 'CV',
		rwb: 'RVV',
		rb: 'RA',
		lwb: 'LVV',
		lb: 'LA',
		cdm: 'CVM',
		cm: 'CM',
		lm: 'LM',
		rm: 'RM',
		cam: 'CAM',
		rw: 'RB', // right wing
		lw: 'LB', // left wing
		cf: 'CA', // central front
		rf: 'RV', // right front
		lf: 'LV', // left front
		st: 'SP' // attacker
	},
}