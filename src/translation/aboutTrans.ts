
export const aboutTrans = {
	'it-IT': {
		metatags: {
			title: '',
			metaTitle: '',
			description: ''
		},
		titles: {
			page: '',
			intro: '',
			behind: '',
			toolbox: ' ',
			contact: ''
		},
		intro: '"Virtual Pro Space"',
		intro2: '',
		gitlab: '',
		gitlab2: '',
		react: '',
		next: '',
		mui: ''
	},
	'en-US': {
		metatags: {
			title: 'About VPS',
			metaTitle: 'About VPS | Building features with FIFA Pro Clubs data',
			description: 'We are building pro clubs features on top of the data powered by EA Sports, which we are NOT affiliated with or sponsored by.'
		},
		titles: {
			page: 'About us',
			intro: 'Introduction',
			behind: 'Behind Virtual Pro Space',
			toolbox: 'Project Toolbox',
			contact: 'Contact us'
		},
		intro: '"Virtual Pro Space" is a browser based open source platform that enables you fast access to any FIFA pro clubs/players/matches statistics on PS4, PS5, XBOXONE, XBSXS & PC.',
		intro2: 'We are also building features on top of the data powered by EA Sports, which we are NOT affiliated with or sponsored by. Check our newest release notes:',
		ethos: 'Because Open Source plays a major part in how software is being built, we see it as a matter of course to maintain this project public and provide a transparent workflow for developers all over the world, who might get inspired and give the same effort back to the community by creating valuable features for this project.',
		gitlab: ' is a web-based DevOps lifecycle tool that provides a ',
		gitlab2: ' repository manager providing wiki, issue-tracking, continuous integration and deployment pipeline features, using an open source license.',
		react: 'React is a JavaScript library created by the Facebook developer team for building user interfaces. It is designed from the start for gradual adoption. Learn what React is all about on their ',
		next: 'Next.js is an open-source development framework built on top of Node.js enabling React web applications functionalities such as server-side rendering and generating static websites. ',
		mui: 'Material-UI is a simple and customizable component library to build faster, beautiful, and more accessible React applications without much styling efforts. For more information, visit their '
	},
	'hu-HU': {
		metatags: {
			title: '',
			metaTitle: '',
			description: ''
		},
		titles: {
			page: '',
			intro: '',
			behind: '',
			toolbox: ' ',
			contact: ''
		},
		intro: '"Virtual Pro Space"',
		intro2: '',
		gitlab: '',
		gitlab2: '',
		react: '',
		next: '',
		mui: ''
	},
	'de-DE': {
		metatags: {
			title: 'Über VPS',
			metaTitle: 'Über VPS | Verfolge die Entstehung einer dringend benötigte FIFA Pro CLubs Platform',
			description: 'Wir liefern dringend benötigte FIFA Pro Clubs Funktionalitäten. Besuche uns, teile deine Ideen, verfolge die Entstehung und mach auf uns Aufmerksam.'
		},
		titles: {
			page: 'Über uns',
			intro: 'Einleitung',
			behind: 'Hinter Virtual Pro Space',
			toolbox: 'Werkzeugkasten',
			contact: 'Kontaktieren Sie uns'
		},
		intro: '"Virtual Pro Space" ist eine Browser basierte open source Platform die Ihnen schnellen Zugang zu jedem FIFA Pro Club/Spieler und zu den letzen 10 Partien ermöglicht für PS4, PS5, XBOXONE, XBSXS & PC.',
		intro2: 'Wir bauen auch Funktionen auf den Daten von EA Sports auf, von denen wir NICHT gesponsert werden. Sehen Sie sich unsere neuesten Versionshinweise an:',
		ethos: 'Da Open Source eine große Rolle bei der Entwicklung von Software spielt, sehen wir es als selbstverständlich an, dieses Projekt öffentlich zu halten und einen transparenten Workflow für Entwickler auf der ganzen Welt bereitzustellen, die sich inspirieren lassen und den gleichen Aufwand zurückgeben können durch die Schaffung wertvoller Funktionen für dieses Projekt.',
		gitlab: ' ist ein web-basierter DevOps lifecycle tool mit einem ',
		gitlab2: ' Projekt manager, Problem-verfolgung, fortfahrende code integration und Pipeline Funktionalität, alles unter einer Open Source Lizenz.',
		react: 'React ist eine JavaScript-Bibliothek, die vom Facebook-Entwicklerteam zum einfachen und intuitivem Erstellen von Benutzeroberflächen entwickelt wurde. Erfahre, worum es bei React geht auf ihrer ',
		next: 'Next.js ist ein Open-Source-Entwicklungsframework, das auf Node.js basiert und React-Apps wie serverseitiges Rendering und das Generieren statischer Webseiten ermöglicht. ',
		mui: 'Material-UI ist eine anpassbare Komponenten Bibliothek, um schnellere und zugänglichere React-Anwendungen zu erstellen. Für weitere Informationen, besuchen Sie ihre '
	},
	'fr-FR': {
		metatags: {
			title: 'À propos de VPS',
			metaTitle: 'À propos de VPS | Suivez la création d\'une plateforme FIFA Pro CLubs si nécessaire',
			description: 'FIFA pro clubs fonctionnalités en plus des données fournies par EA Sports, avec lesquelles nous ne sommes PAS affiliés ni parrainés.'
		},
		titles: {
			page: 'À propos de nous',
			intro: 'Introduction',
			behind: 'Derrière Virtual Pro Space',
			toolbox: 'Boîte à outils',
			contact: 'Nous contacter'
		},
		intro: '"Virtual Pro Space" est une plateforme open source basée sur un navigateur qui vous permet d\'accéder rapidement toutes les statistiques de FIFA pro clubs/joueurs/matchs sur PS4, PS5, XBOXONE, XBSXS et PC.',
		intro2: 'Nous développons également des fonctionnalités en plus sur les données fournies par EA Sports, avec lesquelles nous ne sommes PAS affiliés ni parrainés. Consultez nos dernières notes de version:',
		ethos: 'Parce que l\'Open Source joue un rôle majeur dans la façon dont le logiciel est construit, nous maintenons ce projet public pour fournir un flux de travail transparent pour les développeurs du monde entier, qui pourraient s\'inspirer et redonner le même effort en créant des fonctionnalités précieuses pour ce projet.',
		gitlab: ' est un logiciel libre de forge basé sur ',
		gitlab2: ' proposant les fonctionnalités de wiki, un système de suivi des bugs, l’intégration continue et la livraison continue.',
		react: 'React est une bibliothèque JavaScript créée par l\'équipe de développeurs de Facebook pour créer des interfaces. C\'est conçu dès le départ pour une adoption progressive. Découvrez React sur leur ',
		next: 'Next.js est un développement tool open source construit sur Node.js permettant les fonctionnalités des React-Apps telles que le "serverside rendering" et la génération de sites statiques. ',
		mui: 'Material-UI est une bibliothèque de composants simple et personnalisable pour créer des applications React plus rapides, plus belles et plus accessibles sans trop d\'efforts de style. Pour plus d\'informations, visitez leur '
	},
	'nl-NL': {
		metatags: {
			title: 'Over VPS',
			metaTitle: 'Over VPS | Functies bouwen met FIFA Pro Clubs-gegevens',
			description: 'We bouwen functies voor profclubs bovenop de gegevens van EA Sports, waar we NIET bij zijn aangesloten of door worden gesponsord.'
		},
		titles: {
			page: 'Over ons',
			intro: 'Invoering',
			behind: 'Achter Virtual Pro Space',
			toolbox: 'Gereedschapskist ',
			contact: 'Contact'
		},
		intro: '"Virtual Pro Space" is een browsergebaseerd open source-platform waarmee je snel toegang hebt tot alle FIFA pro clubs/spelers/wedstrijden statistieken op PS4, PS5, XBOXONE, XBSXS & PC.',
		intro2: 'We bouwen ook functies bovenop de gegevens die worden mogelijk gemaakt door EA Sports, waar we NIET aan zijn gelieerd of door worden gesponsord. Bekijk onze nieuwste release-opmerkingen:',
		ethos: 'Omdat Open Source een grote rol speelt in de manier waarop software wordt gebouwd, zien we het als een vanzelfsprekendheid om dit project openbaar te houden en een transparante workflow te bieden voor ontwikkelaars over de hele wereld, die misschien geïnspireerd raken en dezelfde inspanning teruggeven aan de gemeenschap door waardevolle functies voor dit project te creëren.',
		gitlab: ' is een webgebaseerde DevOps-levenscyclustool die een ',
		gitlab2: ' repository manager providing wiki, issue-tracking, continuous integration and deployment pipeline features, using an open source license.',
		react: 'React is een JavaScript-bibliotheek die is gemaakt door het Facebook-ontwikkelaarsteam voor het bouwen van gebruikersinterfaces. Het is vanaf het begin ontworpen voor geleidelijke acceptatie. Ontdek waar React allemaal over gaat op hun ',
		next: 'Next.js is een open-source ontwikkelingsraamwerk dat bovenop Node.js is gebouwd en dat functionaliteiten van React-webapplicaties mogelijk maakt, zoals server-side rendering en het genereren van statische websites.',
		mui: 'Material-UI is een eenvoudige en aanpasbare componentenbibliotheek om snellere, mooiere en meer toegankelijke React-applicaties te bouwen zonder veel stylinginspanningen. Ga voor meer informatie naar hun '
	},
}
