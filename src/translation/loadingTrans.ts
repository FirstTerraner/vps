export const loadingPageTrans = {
	'it-IT': {
		waiting: '',
		eaSlow: '',
	},
	'hu-HU': {
		waiting: '',
		eaSlow: '',
	},
	'en-US': {
		waiting: 'Waiting for EA...',
		eaSlow: '(They\'re slow)',
	},
	'de-DE': {
		waiting: 'Auf EA warten...',
		eaSlow: '(Die sind lahm)',
	},
	'fr-FR': {
		waiting: 'Attendre EA...',
		eaSlow: '(Ils sont lents)',
	},
	'nl-NL': {
		waiting: 'Wacht op EA...',
		eaSlow: '(Ze zijn traag)',
	},
}