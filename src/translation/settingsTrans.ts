export const settingsPageTrans = {
	'it-IT': {
		chooseLang: 'Scegli una lingua',
		metaTitle: 'Personalizza il tuo VPS sito web e molto altro a breve.',
		metaDes: 'Dai un\'occhiata alle funzionalità che stiamo attualmente aggiungendo. Open source.',
	},
	'hu-HU': {
		chooseLang: '',
		metaTitle: '',
		metaDes: '',
	},
	'en-US': {
		chooseLang: 'Choose a language',
		metaTitle: 'Personalize your VPS site. Choose your language and more preferences coming soon.',
		metaDes: 'Have a look at features we are currently implementing. Open source.',
	},
	'de-DE': {
		chooseLang: 'Sprache auswählen',
		metaTitle: 'Personalisiere deine VPS Seite. Wähle eine Sprache aus und viel mehr demnächst.',
		metaDes: 'Werfe einen Blick auf die Funktionalitäten die wir derzeit einbauen. Open Source.',
	},
	'fr-FR': {
		chooseLang: 'Choisissez une langue',
		metaTitle: 'Personnalisez votre site VPS. Choisissez votre langue et d\'autres préférences à venir.',
		metaDes: 'Jetez un œil aux fonctionnalités que nous implémentons actuellement. Open source.',
	},
	'nl-NL': {
		chooseLang: 'Kies een taal',
		metaTitle: 'Personaliseer je VPS-site. Kies uw taal en binnenkort meer voorkeuren.',
		metaDes: 'Bekijk de functies die we momenteel implementeren. Open source.',
	},
}