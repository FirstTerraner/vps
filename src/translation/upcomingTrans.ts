export const upcomingTrans = {
	'it-IT': {
		pageTitle: '',
		metaTitle: '',
		metaDes: '',
		userAcc: '',
		userPromo: '',
		issueInfo: '',
		issueTracker: '',

	},
	'en-US': {
		pageTitle: 'Future implementations',
		metaTitle: 'Have a look at features we are currently implementing. Open source.',
		metaDes: 'We are aiming to provide a great user experience along with features that makes the game mode more valuable. ',
		userAcc: 'User account',
		userPromo: 'Signing up opens the door for new features like tournaments, own leaderboards, achievement badges, status, chat...',
		issueInfo: 'For suggestions or bug reports, please use the',
		issueTracker: 'issue tracker',

	},
	'hu-HU': {
		pageTitle: '',
		metaTitle: '',
		metaDes: '',
		userAcc: '',
		userPromo: '',
		issueInfo: '',
		issueTracker: '',

	},
	'de-DE': {
		pageTitle: 'Zukünftige Implementation',
		metaTitle: 'Werfe einen Blick auf die Funktionalitäten die wir derzeit einbauen. Open Source.',
		metaDes: 'Unser Ziel ist es, eine großartige Benutzererfahrung zusammen mit Funktionen zu bieten, die den Spielmodus wertvoller machen.',
		userAcc: 'Benutzerkonto',
		userPromo: 'Eine Benutzer Registrierung öffnet die Tür für eigene Turniere, Bestenlisten, Errungenschaften, Nutzer Status, Chats usw...',
		issueInfo: 'Für Vorschläge oder Bug Hinweise, bitte verwende die',
		issueTracker: 'Problemverfolgung',

	},
	'fr-FR': {
		pageTitle: 'Implémentations futures',
		metaTitle: 'Jetez un œil aux fonctionnalités que nous implémentons actuellement. Open source.',
		metaDes: 'Nous visons à offrir une excellente expérience utilisateur ainsi que des fonctionnalités qui rendent le mode de jeu plus précieux.',
		userAcc: 'Compte d\'utilisateur',
		userPromo: 'L\'inscription ouvre la porte à de nouvelles fonctionnalités telles que les tournois, les propres classements, les badges de réussite, le statut, le chat ect...',
		issueInfo: 'Pour des suggestions ou des rapports d\'erreurs, veuillez utiliser le',
		issueTracker: 'traqueur d\'incidents',

	},
	'nl-NL': {
		pageTitle: 'Toekomstige implementaties',
		metaTitle: 'Bekijk de functies die we momenteel implementeren. Open source.',
		metaDes: 'We streven ernaar om een ​​geweldige gebruikerservaring te bieden, samen met functies die de spelmodus waardevoller maken.',
		userAcc: 'Gebruikers account',
		userPromo: 'Gebruikersregistratie opent de deur naar nieuwe functies zoals toernooien, eigen leaderboards, prestatiebadges, status, chat...',
		issueInfo: 'Alsjeblieft gebruik voor suggesties of bugrapporten de',
		issueTracker: 'probleemtracker',

	},
}