export const searchPageTrans = {
	'it-IT': {
		metaDes: '',

	},
	'hu-HU': {
		metaDes: '',

	},
	'en-US': {
		metaDes: 'Search any club by name & favorize them for fast access. Optimal smartphone view. Open source.',

	},
	'de-DE': {
		metaDes: 'Durchsuche die Pro Clubs Datenbank & favorisiere Vereine & Spieler für schnellen Zugang. Optimale Übersicht für Smartphones.',

	},
	'fr-FR': {
		metaDes: 'Recherchez n\'importe quel club par son nom et privilégiez-le pour un accès rapide. Vue optimale du smartphone. Open source.',

	},
	'nl-NL': {
		metaDes: 'Zoek een club op naam en geef ze een voorkeur voor snelle toegang. Optimaal smartphonebeeld. Open source.',

	},
}