export const constructionTrans = {
	'it-IT': {
		underCons: '',
	},
	'hu-HU': {
		underCons: '',
	},
	'en-US': {
		underCons: 'This page is under construction.',
	},
	'de-DE': {
		underCons: 'Diese Seite wird derzeit aufgebaut.',
	},
	'fr-FR': {
		underCons: 'Cette page est en cours de construction.',
	},
	'nl-NL': {
		underCons: 'Deze pagina is in opbouw.',
	},
}