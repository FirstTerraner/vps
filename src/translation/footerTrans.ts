export const footerTrans = {
	'it-IT': {
		txt1: '',
		txt2: '',
	},
	'hu-HU': {
		txt1: '',
		txt2: '',
	},
	'en-US': {
		txt1: 'is not affiliated with EA Sports.',
		txt2: 'Licensed under the AGPL v3 License. ',
	},
	'de-DE': {
		txt1: 'wird nicht von EA Sports gesponsert.',
		txt2: 'Lizensiert unter der AGPL v3 Lizenz. ',
	},
	'fr-FR': {
		txt1: 'n\'est pas sponsorisé par EA Sports.',
		txt2: 'Sous licence AGPL v3. ',
	},
	'nl-NL': {
		txt1: 'wordt niet gesponsord door EA Sports.',
		txt2: 'Gelicentieerd onder AGPL v3 licentie. ',
	},
}