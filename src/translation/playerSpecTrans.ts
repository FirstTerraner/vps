export const playerSpecialsTrans = {
	'it-IT': {
		sniper: '',
		finisher: '', // clinical finisher
		aerialThreat: '', // aerial threat (tall and head skilled)
		freeKick: '', // freekick specialist
		dribbler: '',
		playmaker: '',
		crosser: '',
		tackler: '',
		speedster: '',
		tactician: '',
		acrobat: '',
		compForward: '', // complete forward
		compMid: '', // complete midfielder
		compDef: '', // complete defender
	},
	'hu-HU': {
		sniper: '',
		finisher: '', // clinical finisher
		aerialThreat: '', // aerial threat (tall and head skilled)
		freeKick: '', // freekick specialist
		dribbler: '',
		playmaker: '',
		crosser: '',
		tackler: '',
		speedster: '',
		tactician: '',
		acrobat: '',
		compForward: '', // complete forward
		compMid: '', // complete midfielder
		compDef: '', // complete defender
	},
	'en-US': {
		sniper: 'Sniper',
		finisher: 'Clinical finisher', // clinical finisher
		aerialThreat: 'Aerial threat', // aerial threat (tall and head skilled)
		freeKick: 'Freekick specialist', // freekick specialist
		dribbler: 'Dribbler',
		playmaker: 'Playmaker',
		crosser: 'Crosser',
		tackler: 'Tackler',
		speedster: 'Speedster',
		tactician: 'Tactician',
		acrobat: 'Acrobat',
		compForward: 'Complete forward', // complete forward
		compMid: 'Complete midfielder', // complete midfielder
		compDef: 'Complete defender', // complete defender
	},
	'de-DE': {
		sniper: 'Scharfschütze',
		finisher: 'Klinischer Abschluss', // clinical finisher
		aerialThreat: 'Lufthoheit', // aerial threat (tall and head skilled)
		freeKick: 'Freistoßspezialist', // freekick specialist
		dribbler: 'Dribbler',
		playmaker: 'Spielmacher',
		crosser: 'Flankengeber',
		tackler: 'Tackler',
		speedster: 'Flitzer',
		tactician: 'Architect',
		acrobat: 'Akrobat',
		compForward: 'Kompletter Angreifer', // complete forward
		compMid: 'Kompletter Mittelfelder', // complete midfielder
		compDef: 'Kompletter Verteidiger', // complete defender
	},
	'fr-FR': {
		sniper: 'Tireur d\'élite',
		finisher: 'Finisseur clinique', // clinical finisher
		aerialThreat: 'Menace aérienne', // aerial threat (tall and head skilled)
		freeKick: 'Coups francs spécialist', // freekick specialist
		dribbler: 'Dribbleur',
		playmaker: 'Meneur de jeu',
		crosser: 'Flanceur',
		tackler: 'Tacleur',
		speedster: 'Speedster',
		tactician: 'Architecte',
		acrobat: 'Acrobat',
		compForward: 'Attaquant complet', // complete forward
		compMid: 'Défenseur complet', // complete midfielder
		compDef: 'Milieu de terrain complet', // complete defender
	},
	'nl-NL': {
		sniper: 'Sluipschutter',
		finisher: 'klinische afmaker', // clinical finisher
		aerialThreat: 'Luchtgevaar', // aerial threat (tall and head skilled)
		freeKick: 'Vrije trap specialist', // freekick specialist
		dribbler: 'Dribbelaar',
		playmaker: 'Spelmaker',
		crosser: 'Flank gever',
		tackler: 'Tackelaar',
		speedster: 'Snelheidsduivel',
		tactician: 'Tacticus',
		acrobat: 'Acrobaat',
		compForward: 'Volledige aanvaller', // complete forward
		compMid: 'Volledige middenvelder', // complete midfielder
		compDef: 'Volledige verdediger', // complete defender
	},
}