import { IFavClub } from '../models/interfaces'
import { isArr, toJsonStr } from '../models/typeGuards'
import { getFromLocalStorage, removeFromLocalStorage, setLocalStorage } from './localStorage'

export const getFavClubsLocalStorage = () => getFromLocalStorage<IFavClub[]>('favClubs')

export const getFavClubById = (id: string) => getFromLocalStorage<IFavClub>(id, 'favClub')

export const isInFavClubs = (id: string) => !!getFavClubById(id)

export const addFavClubLocalStorage = (fav: IFavClub) => {
	const favs = getFavClubsLocalStorage() || []
	if (!isArr(favs)) {
		return
	}
	favs?.push(fav)
	setLocalStorage('favClubs', toJsonStr(Array.from(new Set<IFavClub>(favs))) || '')
	setLocalStorage(fav.clubId, toJsonStr(fav) || '', 'favClub')
}

export const removeFavClubLocalStorage = (id: string) => {
	const favClubs = getFavClubsLocalStorage()
	if (!isArr(favClubs)) {
		return
	}
	setLocalStorage('favClubs', toJsonStr(favClubs.filter(x => x.clubId !== id) || []) || '')
	return removeFromLocalStorage(id, 'favClub')
}