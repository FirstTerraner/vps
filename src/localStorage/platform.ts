import { TPlatformType } from 'proclubs-api/dist'
import { getFromLocalStorage, setLocalStorage } from './localStorage'

export const getPlatformLocalStorage = () => getFromLocalStorage<TPlatformType>('platform')

export const setPlatformLocalStorage = (platform: string) => setLocalStorage('platform', platform)