import { IFavPlayer } from '../models/interfaces'
import { isArr, toJsonStr } from '../models/typeGuards'
import { getFromLocalStorage, removeFromLocalStorage, setLocalStorage } from './localStorage'

export const getFavPlayersLocalStorage = () => getFromLocalStorage<IFavPlayer[]>('favPlayers')

export const getFavPlayerByName = (name: string) => getFromLocalStorage<IFavPlayer>(name, 'favPlayer')

export const isInFavPlayers = (name: string) => !!getFavPlayerByName(name)

export const addFavPlayerLocalStorage = (fav: IFavPlayer) => {
	const favs = getFavPlayersLocalStorage() || []
	if (!isArr(favs)) {
		return
	}
	favs?.push(fav)
	setLocalStorage('favPlayers', toJsonStr(Array.from(new Set<IFavPlayer>(favs))) || '')
	setLocalStorage(fav.playerName, toJsonStr(fav) || '', 'favPlayer')
}

export const removeFavPlayerLocalStorage = (name: string) => {
	const favPlayers = getFavPlayersLocalStorage()
	if (!isArr(favPlayers)) {
		return
	}
	setLocalStorage('favPlayers', toJsonStr(favPlayers.filter(x => x.playerName !== name) || []) || '')
	return removeFromLocalStorage(name, 'favPlayer')
}