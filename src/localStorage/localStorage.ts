import { cTo } from '../models/typeGuards'

export function getFromLocalStorage<T>(key: string, prefix?: string) {
	if (!key || !localStorage) { return null }
	try {
		const resp = localStorage.getItem(prefix ? `${prefix}||${key}` : key)
		if (!resp) { return null }
		// console.log('local storage response: ', resp)
		const jsonResp: T | null = cTo<T>(resp)
		return jsonResp ? jsonResp : resp
	} catch (e) { console.log(e) }
	return null
}
export function setLocalStorage(key: string, val: string, prefix?: string) {
	if (!key || !localStorage) { return null }
	try { return localStorage.setItem(prefix ? `${prefix}||${key}` : key, val) } catch (e) { console.log(e) }
	return null
}
export function removeFromLocalStorage(key: string, prefix?: string) {
	if (!key || !localStorage) { return null }
	try { return localStorage.removeItem(prefix ? `${prefix}||${key}` : key) } catch (e) { console.log(e) }
	return null
}
export function getAllKeysByPrefixFromLocalStorage(prefix: string) {
	return getAllKeysFromLocalStorage()?.filter(x => x?.trim() && x?.startsWith(prefix))
}
export function getAllKeysFromLocalStorage() {
	if (!localStorage) { return null }
	const keys = []
	for (let i = 0; i < localStorage.length; i++) { keys.push(localStorage.key(i)) }
	return keys.filter(x => x?.trim())
}