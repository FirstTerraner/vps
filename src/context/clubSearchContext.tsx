import { useRouter } from 'next/router'
import { isPlatformType, TPlatformType } from 'proclubs-api/dist'
import { IClubSearch } from 'proclubs-api/dist/model/club'
import React from 'react'
import { getPlatformLocalStorage, setPlatformLocalStorage } from '../localStorage/platform'

const useClubSearch = () => {

	const router = useRouter()

	const [tabIdx, setTabIdx] = React.useState(0)

	const [platform, setplatform] = React.useState<TPlatformType>()

	const [clubs, setClubsSearch] = React.useState<IClubSearch[] | undefined | null>()

	const [leaderTabIdx, setLeaderTabIdx] = React.useState(0)

	React.useEffect(() => {
		if (isPlatformType(router.query.platform)) {
			setplatform(router.query.platform)
			return
		}
		const localPlat = getPlatformLocalStorage()
		// setTabIdx(handleTabIdxRoute())
		if (!isPlatformType(localPlat)) {
			// set platform in local storage to ps4 as default
			setPlatformLocalStorage('ps4')
			setplatform('ps4')
			return
		}
		setplatform(localPlat)
		return () => setClubsSearch(undefined)
	}, [])

	return {
		clubs,
		platform,
		tabIdx,
		leaderTabIdx,
		setLeaderTab(idx: number) {
			setLeaderTabIdx(idx)
		},
		setTabsIdx(idx: number) {
			setTabIdx(idx)
		},
		setPlat(plat: TPlatformType) {
			setplatform(plat)
		},
		setClubs(clubss?: IClubSearch[] | null) {
			setClubsSearch(clubss)
		},
		removeAllClubs() {
			setClubsSearch([])
		}
	}
}

type useClubSearchType = ReturnType<typeof useClubSearch>
const ClubSearchContext = React.createContext<useClubSearchType>({
	clubs: undefined,
	platform: 'ps4',
	tabIdx: 0,
	leaderTabIdx: 0,
	setLeaderTab: (idx: number) => console.log(idx),
	setTabsIdx: (idx: number) => console.log(idx),
	setPlat: (plat: TPlatformType) => console.log(plat),
	setClubs: (clubs?: IClubSearch[]) => console.log(clubs),
	removeAllClubs: () => console

})
export const useClubSearchContext = () => React.useContext(ClubSearchContext)
export const ClubSearchProvider = ({ children }: { children: React.ReactNode }) => (
	<ClubSearchContext.Provider value={useClubSearch()} >
		{children}
	</ClubSearchContext.Provider>
)