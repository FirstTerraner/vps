import { useRouter } from 'next/router'
import React from 'react'
import { getLanguageCookie, setLanguageCookie } from '../cookies/language'
import { isTLocale, TLocale } from '../models/translations'
import { aboutTrans } from '../translation/aboutTrans'
import { attributeTrans } from '../translation/attributes'
import { clubPageTrans } from '../translation/clubTrans'
import { constructionTrans } from '../translation/constructionPageTrans'
import { footerTrans } from '../translation/footerTrans'
import { homepageTrans } from '../translation/homepageTrans'
import { loadingPageTrans } from '../translation/loadingTrans'
import { playerPositionTrans } from '../translation/playerPos'
import { playerSpecialsTrans } from '../translation/playerSpecTrans'
import { searchPageTrans } from '../translation/searchPageTrans'
import { settingsPageTrans } from '../translation/settingsTrans'
import { singleWords } from '../translation/singleWords'
import { upcomingTrans } from '../translation/upcomingTrans'

const defaultLang = 'en-US'

const defaultTranslated = {
	aboutPage: aboutTrans[defaultLang],
	attributes: attributeTrans[defaultLang],
	clubPage: clubPageTrans[defaultLang],
	construction: constructionTrans[defaultLang],
	footer: footerTrans[defaultLang],
	homepage: homepageTrans[defaultLang],
	loading: loadingPageTrans[defaultLang],
	playerPos: playerPositionTrans[defaultLang],
	playerSpecial: playerSpecialsTrans[defaultLang],
	searchPage: searchPageTrans[defaultLang],
	settingsPage: settingsPageTrans[defaultLang],
	singleWords: singleWords[defaultLang],
	upcomingPage: upcomingTrans[defaultLang]
}

const useTrans = () => {

	const router = useRouter()

	// TODO add proper typeguard for type TLocale
	const routerLang = router.locale as TLocale

	const routerPath = router.asPath

	const [localeTrans, setLocaleTrans] = React.useState<TLocale | null>()

	const [translation, setTranslation] = React.useState(defaultTranslated)

	React.useEffect(() => {

		const cookieLang = getLanguageCookie()

		// console.log('cookieLang: ', cookieLang)
		if (cookieLang && isTLocale(cookieLang)) {
			setLocaleTrans(cookieLang as TLocale)
		}

		if (!cookieLang) {
			setLanguageCookie(routerLang, 365)
			setLocaleTrans(routerLang)
			return
		}

		if (cookieLang && routerLang && cookieLang !== routerLang && !routerPath.includes('[')) {
			setLocaleTrans(isTLocale(cookieLang) ? cookieLang as TLocale : null)
			void router.replace(routerPath, routerPath, { locale: cookieLang })
		}

	}, [routerLang, routerPath])

	React.useEffect(() => {

		if (!localeTrans || !isTLocale(localeTrans)) { return }

		setTranslation({
			aboutPage: aboutTrans[localeTrans],
			attributes: attributeTrans[localeTrans],
			clubPage: clubPageTrans[localeTrans],
			construction: constructionTrans[localeTrans],
			footer: footerTrans[localeTrans],
			homepage: homepageTrans[localeTrans],
			loading: loadingPageTrans[localeTrans],
			playerPos: playerPositionTrans[localeTrans],
			playerSpecial: playerSpecialsTrans[localeTrans],
			searchPage: searchPageTrans[localeTrans],
			settingsPage: settingsPageTrans[localeTrans],
			singleWords: singleWords[localeTrans],
			upcomingPage: upcomingTrans[localeTrans]
		})

	}, [localeTrans])

	return {
		localeT: localeTrans,
		translated: translation,
		setLocaleT(local: TLocale) {
			setLocaleTrans(local)
		}
	}
}
type useTransType = ReturnType<typeof useTrans>
const TransContext = React.createContext<useTransType>({
	localeT: defaultLang,
	translated: defaultTranslated,
	setLocaleT: (locale: TLocale) => console.log(locale)
})
export const useTransContext = () => React.useContext(TransContext)
export const TransProvider = ({ children }: { children: React.ReactNode }) => (
	<TransContext.Provider value={useTrans()} >
		{children}
	</TransContext.Provider>
)