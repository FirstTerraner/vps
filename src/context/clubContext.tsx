import React from 'react'
import { IClubOverviewState, ISquadOverviewState, TMatchesState } from '../models/interfaces'

const squadDefault = {
	memberCareer: null,
	memberStats: null
}

/**
 *
 * @deprecated
 *
 */
const useClub = () => {
	const [clubInfo, setClubInfo] = React.useState<IClubOverviewState | null>()
	const [squad, setSquad] = React.useState<ISquadOverviewState>(squadDefault)
	const [matches, setMatches] = React.useState<TMatchesState>([])
	const [cupMatches, setCupMatches] = React.useState<TMatchesState>([])
	return {
		clubInfo,
		setClubInfos(club: IClubOverviewState) {
			setClubInfo(club)
		},
		squad,
		setSquads(squads: ISquadOverviewState) {
			setSquad(squads)
		},
		matches,
		setMatchs(matchs: TMatchesState) {
			setMatches(matchs)
		},
		cupMatches,
		setCupMatchs(matchs: TMatchesState) {
			setCupMatches(matchs)
		}
	}
}
type useClubType = ReturnType<typeof useClub>
const ClubContext = React.createContext<useClubType>({
	clubInfo: null,
	setClubInfos: (club: IClubOverviewState) => console.log(club),
	squad: { memberStats: null, memberCareer: null },
	setSquads: (squad: ISquadOverviewState) => console.log(squad),
	matches: [],
	setMatchs: (matchs: TMatchesState) => console.log(matchs),
	cupMatches: [],
	setCupMatchs: (matchs: TMatchesState) => console.log(matchs)
})
export const useClubContext = () => React.useContext(ClubContext)
export const ClubProvider = ({ children }: { children: React.ReactNode }) => (
	<ClubContext.Provider value={useClub()} >
		{children}
	</ClubContext.Provider>
)