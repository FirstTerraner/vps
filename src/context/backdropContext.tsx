import React from 'react'

const useBackdrop = () => {
	const [isOpen, setIsOpen] = React.useState(false)
	const [isNavOpen, setIsNavOpen] = React.useState(false)
	return {
		isBackdropOpen: isOpen,
		withNav: isNavOpen,
		toggleBackdrop() {
			setIsOpen(prev => !prev)
		},
		toggleNav() {
			setIsNavOpen(prev => !prev)
		},
		openNav() {
			setIsNavOpen(true)
		},
		closeNav() {
			setIsNavOpen(false)
		},
		open() {
			setIsOpen(true)
		},
		close() {
			setIsOpen(false)
		}
	}
}

type useBackdropType = ReturnType<typeof useBackdrop>

const BackdropContext = React.createContext<useBackdropType>({
	isBackdropOpen: false,
	withNav: false,
	toggleBackdrop: () => console,
	toggleNav: () => console,
	openNav: () => console,
	closeNav: () => console,
	open: () => console,
	close: () => console
})

export const useBackdropContext = () => React.useContext(BackdropContext)

export const BackdropProvider = ({ children }: { children: React.ReactNode }) => (
	<BackdropContext.Provider value={useBackdrop()} >
		{children}
	</BackdropContext.Provider>
)