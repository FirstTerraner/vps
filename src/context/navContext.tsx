import React from 'react'
/**
 *
 * @deprecated
 *
 */
const useNav = () => {
	const [isNavOpen, setIsNavOpen] = React.useState(false)
	return {
		isNavOpen,
		toggleNav() {
			setIsNavOpen(prev => !prev)
		},
		openNav() {
			setIsNavOpen(true)
		},
		closeNav() {
			setIsNavOpen(false)
		}
	}
}
type useNavType = ReturnType<typeof useNav>
const NavContext = React.createContext<useNavType>({
	isNavOpen: false,
	toggleNav: () => console,
	openNav: () => console,
	closeNav: () => console
})
export const useNavContext = () => React.useContext(NavContext)
export const NavProvider = ({ children }: { children: React.ReactNode }) => (
	<NavContext.Provider value={useNav()} >
		{children}
	</NavContext.Provider>
)