import axios from 'axios'
import { WrappedNodeRedisClient } from 'handy-redis'
import { getClubInfo, getClubMatchHistory, getClubMembers, getClubMemberStats, getClubStats, TPlatformType } from 'proclubs-api/dist'
import { IClubInfo, IClubMatches, IClubMemberCareer, IClubMemberStats, IClubSearch, IClubStats } from 'proclubs-api/dist/model/club'
import { TClubProps, TMatchProps, TMemberProps } from './models/interfaces'
import { toJsonStr, isStr, cTo } from './models/typeGuards'

export const fifaVersion = '22'

// TODO add search api to serversideprops
const baseReq = axios.create({ baseURL: '/api/proclubs', timeout: 5000 })

const fetchRoute = async <T>(route: string) => {
	try {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		// const cached: T = cache.get(route)
		// console.log('frontend cached: ', cached)
		// if (cached) {
		// 	return cached
		// }
		// TODO add cancelTokenSource -> get(url, {})
		const resp = await baseReq.get<T>(route)
		// console.log('response in cacheFetch: ', resp)
		// bad status code or null resp
		if ((resp?.status && (resp?.status < 200 || resp?.status > 299)) || !resp?.data) { return null }
		// add to cache
		// cache.put(route, resp.data, 1000 * 60 * 3) // 3mins
		return resp.data
	} catch (e) { console.error('url: ', route, 'error: ', e) }
	return null
}

/* export const testCache = async <T>(route: string, clubId?: number) => {
	if (!clubId) { return null }
	try {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const cached: T = cache.get(route)
		// console.log('test cached: ', cached)
		if (cached) { return cached }
		const resp = await getClubInfo(clubId)
		// console.log('test cache resp: ', resp)
		if (!resp) { return null }
		cache.put(route, resp, 1000 * 60 * 5)
		return resp
	} catch (e) { console.log(e) }
	return null
} */

/* export const getClubIdByName = async (platform: TPlatformType, clubName: string) => {
	if (!clubName) { return null }
	const resp = await cacheFetch<IClubId>(`/${platform}/get_club_id_by_name/${encodeURIComponent(clubName)}`)
	if (!resp) { return null }
	return resp
} */

/**
 * @param platform represents the club platform
 * @param clubName represents the club name
 * @returns an array of clubs
 */
export const getClubsByName = async (platform: TPlatformType, clubName: string) => {
	if (!clubName) { return null }
	const resp = await fetchRoute<IClubSearch[]>(`/${platform}/get_club_by_name/${encodeURIComponent(clubName)}`)
	if (!resp) { return null }
	return resp
}

export const addRecordByName = async (platform: TPlatformType, clubName: string) => {
	if (!platform || !clubName) { // || empty obj || !IRecClub
		return null
	}
	const resp = await fetchRoute<IClubSearch[] | 0 | 1 | 2 | 3>(`/${platform}/add_record_by_name/${encodeURIComponent(clubName)}`)
	return resp
}

export const addRecordById = async (platform: TPlatformType, id: string) => {
	if (!platform || !id) { // || empty obj || !IRecClub
		return null
	}
	const resp = await fetchRoute<IClubSearch[] | 0 | 1 | 2 | 3>(`/${platform}/add_record_by_id/${id}`)
	return resp
}

/* ServerSideProps helper */

export const getClubInfoProps = async (client: WrappedNodeRedisClient, plat: TPlatformType, clubId: string, setExPromArr: Promise<'OK'>[]) => {

	let clubInfo: TClubProps
	const [cachedInfos, cachedStats] = await client.mget(
		`clubInfo-${plat}-${clubId}`,
		`clubStats-${plat}-${clubId}`
	)
	if (!cachedInfos || !cachedStats) {
		console.log('No club infos cache. fetching data now.')
		clubInfo = await Promise.all([
			getClubInfo(plat, +clubId),
			getClubStats(plat, +clubId)
		])
		// set cache
		if (clubInfo[0]) {
			const clubInfoStr = toJsonStr(clubInfo[0])
			if (isStr(clubInfoStr) && clubInfoStr.length > 0) {
				setExPromArr.push(client.setex(`clubInfo-${plat}-${clubId}`, 60 * 5, clubInfoStr))
			}
		}
		if (clubInfo[1]) {
			const clubStatsStr = toJsonStr(clubInfo[1])
			if (isStr(clubStatsStr) && clubStatsStr.length > 0) {
				setExPromArr.push(client.setex(`clubStats-${plat}-${clubId}`, 60 * 5, clubStatsStr))
			}
		}
		return clubInfo
	}
	console.log('Cache hit for club infos.')
	const clubInfoJson = cTo<IClubInfo | null>(cachedInfos)
	const clubStatsJson = cTo<IClubStats | null>(cachedStats)
	clubInfo = [clubInfoJson, clubStatsJson]
	return clubInfo
}

export const getClubMatchesProps = async (client: WrappedNodeRedisClient, plat: TPlatformType, clubId: string, setExPromArr: Promise<'OK'>[]) => {

	let clubMatches: TMatchProps

	const [cachedDivMatches, cachedCupMatches] = await client.mget(
		`clubInfo-${plat}-${clubId}-gameType9`,
		`clubInfo-${plat}-${clubId}-gameType13`
	)
	if (!cachedDivMatches || !cachedCupMatches) {
		console.log('No matches cache. fetching data now.')
		clubMatches = await Promise.all([
			getClubMatchHistory(plat, +clubId, 'gameType9'),
			getClubMatchHistory(plat, +clubId, 'gameType13')
		])
		// set cache
		if (clubMatches[0]) {
			const divMatchStr = toJsonStr(clubMatches[0])
			if (isStr(divMatchStr) && divMatchStr.length > 0) {
				setExPromArr.push(client.setex(`clubInfo-${plat}-${clubId}-gameType9`, 60 * 5, divMatchStr))
			}
		}
		if (clubMatches[1]) {
			const cupMatchStr = toJsonStr(clubMatches[1])
			if (isStr(cupMatchStr) && cupMatchStr.length > 0) {
				setExPromArr.push(client.setex(`clubInfo-${plat}-${clubId}-gameType13`, 60 * 5, cupMatchStr))
			}
		}
		return clubMatches
	}
	console.log('cache hit for club matches.')
	const divMatchesJson = cTo<IClubMatches[] | null>(cachedDivMatches)
	const cupMatchesJson = cTo<IClubMatches[] | null>(cachedCupMatches)
	clubMatches = [divMatchesJson, cupMatchesJson]
	return clubMatches
}

export const getClubMembersProps = async (client: WrappedNodeRedisClient, plat: TPlatformType, clubId: string, setExPromArr: Promise<'OK'>[]) => {

	let clubMembers: TMemberProps

	const [cachedMemberStats, cachedMemberCareer] = await client.mget(
		`memberStats-${clubId}`,
		`memberCareer-${clubId}`
	)

	if (!cachedMemberStats || !cachedMemberCareer) {
		console.log('No member cache. fetching data now')
		clubMembers = await Promise.all([
			getClubMemberStats(plat, +clubId),
			getClubMembers(plat, +clubId)
		])
		console.log('new fetched members: ', clubMembers)
		// set cache
		if (clubMembers[0]) {
			const memberStatsStr = toJsonStr(clubMembers[0])
			if (isStr(memberStatsStr) && memberStatsStr.length > 0) {
				setExPromArr.push(client.setex(`memberStats-${clubId}`, 60 * 5, memberStatsStr))
			}
		}
		if (clubMembers[1]) {
			const memberCareerStr = toJsonStr(clubMembers[1])
			if (isStr(memberCareerStr) && memberCareerStr.length > 0) {
				setExPromArr.push(client.setex(`memberCareer-${clubId}`, 60 * 5, memberCareerStr))
			}
		}
		return clubMembers

	}
	console.log('cache hit for club members')
	const memberStatsJson = cTo<IClubMemberCareer[] | null>(cachedMemberStats)
	const memberCareerJson = cTo<IClubMemberStats[] | null>(cachedMemberCareer)
	clubMembers = [memberStatsJson, memberCareerJson]
	return clubMembers
}












/* export const getClubMembersStats = async (platform: TPlatformType, clubId: number) => {
	if (!clubId) { return null }
	const resp = await cacheFetch<IClubMemberStats[]>(`/${platform}/get_club_memberstats_by_id/${clubId}`)
	// console.log('squad member stats: ', resp)
	if (!resp) { return null }
	return resp
} */
/* export const getClubMembersCareer = async (platform: TPlatformType, clubId: number) => {
	if (!clubId) { return null }
	const resp = await cacheFetch<IClubMemberCareer[]>(`/${platform}/get_club_membercareer_by_id/${clubId}`)
	// console.log('squad member career: ', resp)
	if (!resp) { return null }
	return resp
} */
/* export const getClubStats = async (platform: TPlatformType, clubId: number, gametype: TGameType) => {
	if (!clubId) { return null }
	const resp = await cacheFetch<IClubStats>(`/${platform}/${gametype}/get_stats_by_id/${clubId}`)
	if (!resp) { return null }
	return resp
} */
/* export const getClubMatchHistory = async (platform: TPlatformType, clubId: number, gametype: TGameType) => {
	if (!clubId) { return null }
	const resp = await cacheFetch<IClubMatches[]>(`/${platform}/${gametype}/get_match_history_by_id/${clubId}`)
	if (!resp) { return null }
	return resp
} */
/* export const getClubInfos = async (platform: TPlatformType, clubId: number) => {
	if (!clubId) { return null }
	const resp = await cacheFetch<IClubInfo>(`/${platform}/get_clubinfo_by_id/${clubId}`)
	if (!resp) { return null }
	return resp
} */
/* export const getClubStats = async (platform: TPlatformType, clubId: number) => {
	if (!clubId) { return null }
	const resp = await cacheFetch<IClubStats>(`/${platform}/get_seasonrank_by_id/${clubId}`)
	if (!resp) { return null }
	return resp
} */
/* export const getOverallLeaderboard = async (platform: TPlatformType) => {
	const resp = await cacheFetch<IClubRankLeaderboard[]>(`/${platform}/get_overall_leaderboard`)
	if (!resp) { return null }
	return resp
} */
/* export const getSeasonLeaderboard = async (platform: TPlatformType) => {
	const resp = await cacheFetch<IClubRankLeaderboard[]>(`/${platform}/get_season_leaderboard`)
	if (!resp) { return null }
	return resp
} */