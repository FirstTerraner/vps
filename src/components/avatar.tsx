import Avatar from '@mui/material/Avatar'
import PermIdentityIcon from '@mui/icons-material/PermIdentity'
import { blueGrey } from '@mui/material/colors'

/**
 * @deprecated
 */

export const LetterAvatar = ({ fontS, txt, icon }: { fontS?: string, txt?: string, icon?: boolean }) => {
	const sizes = () => {
		if (!fontS) { return '' }
		switch (fontS) {
			case 'small': return 3
			case 'medium': return 6
			case 'large': return 9
			default: return ''
		}
	}
	return (
		<div style={{ display: 'flex', margin: '0' }}>
			<Avatar
				sx={{ bgcolor: blueGrey[500], width: sizes() }}
			>
				{icon ? <PermIdentityIcon fontSize='large' color='action' /> : txt || ''}
			</Avatar>
		</div>
	)
}

export default LetterAvatar