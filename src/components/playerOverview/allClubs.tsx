import { IClubMemberCareer } from 'proclubs-api/dist/model/club'
import { Paper } from '@mui/material'
import DirectionsRunIcon from '@mui/icons-material/DirectionsRun'
import SportsSoccerOutlinedIcon from '@mui/icons-material/SportsSoccerOutlined'
import StarIcon from '@mui/icons-material/Star'
import TimelineOutlinedIcon from '@mui/icons-material/TimelineOutlined'
import VisibilityIcon from '@mui/icons-material/Visibility'
import React from 'react'
import LinearDeterminate from '../progress'
import { useTransContext } from '../../context/transContext'

const AllClubsStats = ({ memberStats }: { memberStats: IClubMemberCareer }) => {

	const { translated } = useTransContext()

	return (
		<Paper className='playerClubPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>{translated.singleWords.allClubs}</p>
			</div>
			<div className='panelBody'>
				<div className='columnContainer width100'>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span className='teamHistory'><DirectionsRunIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.matches}</span>
						<span>{memberStats.gamesPlayed}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span className='teamHistory'><SportsSoccerOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.goals}</span>
						<span>{memberStats.goals}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span className='teamHistory'><VisibilityIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.assists}</span>
						<span>{memberStats.assists}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span className='teamHistory'><TimelineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.goals} {translated.singleWords.perMatch}</span>
						<span>{(+memberStats.goals / +memberStats.gamesPlayed).toFixed(1)}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span className='teamHistory'><TimelineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.assists} {translated.singleWords.perMatch}</span>
						<span>{(+memberStats.assists / +memberStats.gamesPlayed).toFixed(1)}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span className='teamHistory'><StarIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.clubPage.sotm}</span>
						<span>{memberStats.manOfTheMatch}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span className='teamHistory'><TimelineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.clubPage.sotm} %</span>
						<span>{(+memberStats.manOfTheMatch / +memberStats.gamesPlayed * 100).toFixed(1)}%</span>
					</div>
					<LinearDeterminate progress={+memberStats.manOfTheMatch / +memberStats.gamesPlayed * 100} width={100} />
				</div>
			</div>
		</Paper>
	)
}

export default AllClubsStats