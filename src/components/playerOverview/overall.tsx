import { Paper } from '@mui/material'
import { useTransContext } from '../../context/transContext'
import { getAttrColor, getPos, getPosColor, getSkillPoints, ratingColor } from '../../helpers'
import { IClubMember } from '../../models/interfaces'
import SmallChip from '../chip'
import LinearDeterminate from '../progress'
import ErrorIcon from '@mui/icons-material/Error'

const PlayerOverall = ({ clubMember }: { clubMember: IClubMember }) => {

	const { translated } = useTransContext()

	// const clubTrans = clubPageTrans[localeT && isTLocale(localeT) ? localeT : 'en-US']
	if (!clubMember.career || !clubMember.stats) { return null }
	return (
		<Paper className='playerClubPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				{/* position */}
				<div className='flexContainer itemsCenter'>
					<div className={`posColor mr-05 ${getPosColor(clubMember.stats.favoritePosition)}`}></div>
					<p className='flexContainer itemsCenter'>{getPos(translated.playerPos, clubMember.stats?.proPos)}</p>
				</div>
				{/* height */}
				{clubMember.career.proHeight ?
					<p className='panelHeaderSmallTxt'>{clubMember.career.proHeight} cm</p>
					: ''
				}
				{/* overall */}
				{+clubMember.career.proOverall > 0 ?
					<div className={`attrBox ${getAttrColor(+clubMember.career.proOverall)}`} >{+clubMember.career.proOverall}</div>
					: ''
				}
				{/* rating avg */}
				{clubMember.stats &&
					<div className={`${ratingColor(+clubMember.stats.ratingAve)} attrBox`}>
						{clubMember.stats.ratingAve}
					</div>
				}
			</div>
			<SmallChip icon={<ErrorIcon />} label={`${translated.singleWords.attributes} ${translated.singleWords.currently} ${translated.attributes.notAccurate}`} />
			<div className='columnContainer' style={{ padding: '0 1em 1em 1em' }}>
				<p>{translated.singleWords.skillPoints} <span className='bold'>{getSkillPoints(+clubMember.stats.gamesPlayed)} / 110</span></p>
				<LinearDeterminate progress={+clubMember.stats.gamesPlayed > 100 ? 100 : +clubMember.stats.gamesPlayed} width={100} />
			</div>
		</Paper>
	)
}

export default PlayerOverall