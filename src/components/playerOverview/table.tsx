
import CheckIcon from '@mui/icons-material/Check'
import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody } from '@mui/material'
import React from 'react'
import { useTransContext } from '../../context/transContext'
import { endResultColor, getPosColor, ratingColor, wasPlayerPresentLastMatches } from '../../helpers'
import { IRecentMatchesData, TMatchesState } from '../../models/interfaces'
import { isNum } from '../../models/typeGuards'

const createData = (
	score: string,
	oppScore: string,
	timeAgo: string,
	position: string,
	rating: string,
	gkSaves: string,
	sotm: string,
	passes: string,
	passRate: string,
	goals: string,
	assists: string,
	shots: string,
	shotRate: string,
	tackles: string,
	tackleRate: string,
	redCards: string
) => ({ score, oppScore, timeAgo, position, rating, gkSaves, sotm, passes, passRate, goals, assists, shots, shotRate, tackles, tackleRate, redCards })

const createRows = (clubId: string, matches: TMatchesState, playerName: string) => {
	// rows for the row array state
	const playerFormRows: IRecentMatchesData[] = []
	if (!matches || matches.length === 0) { return playerFormRows }
	// tslint:disable-next-line: prefer-for-of
	for (let i = 0; i < matches.length; i++) {
		const playerArr = Object.values(matches[i].players[clubId])
		for (let j = 0; j < playerArr.length; j++) {
			const player = playerArr[j]
			if (player.playername === playerName) {
				// add data obj to rows array
				const passRate = (+player.passesmade / +player.passattempts * 100).toFixed(1)
				const goalRate = (+player.goals / +player.shots * 100).toFixed(1)
				const tackleRate = (+player.tacklesmade / +player.tackleattempts * 100).toFixed(1)
				playerFormRows.push(
					createData(
						player.SCORE,
						player.goalsconceded,
						`${matches[i].timeAgo.number}${matches[i].timeAgo.unit[0]}.`,
						player.pos,
						player.rating.slice(0, -1),
						player.saves,
						player.mom === '0' ? 'No' : 'Yes',
						player.passattempts,
						isNum(+passRate) ? `${passRate}%` : '--',
						player.goals,
						player.assists,
						player.shots,
						isNum(+goalRate) ? `${goalRate}%` : '--',
						player.tackleattempts,
						isNum(+tackleRate) ? `${tackleRate}%` : '--',
						player.redcards === '0' ? 'No' : player.redcards
					)
				)
				break
			}
			// last turn in loop & matchRows.length === idx of divMatches means player was not present in match
			if (j + 1 === playerArr.length && playerFormRows.length === i) {
				playerFormRows.push(
					// add empty data obj to rows array
					createData(
						player.SCORE,
						player.goalsconceded,
						`${matches[i].timeAgo.number}${matches[i].timeAgo.unit[0]}.`,
						'--',
						'--',
						'--',
						'--',
						'--',
						'--',
						'--',
						'--',
						'--',
						'--',
						'--',
						'--',
						'--'
					)
				)
			}
		}
	}
	return playerFormRows
}

// main table component
const RecentFormTable = ({ matches, clubId, playerName }: { clubId: string, matches: TMatchesState, playerName: string }) => {

	const { translated } = useTransContext()

	// create row array as state
	const [rows, setRows] = React.useState<IRecentMatchesData[]>([])

	// fill rows with match stats
	React.useEffect(() => {
		if (!matches || matches.length === 0) { return }
		// setRows([])
		const playerFormRows = createRows(clubId, matches, playerName)
		// console.log('matchRows: ', playerFormRows)
		if (playerFormRows.length === 0) { return }
		setRows(playerFormRows)
	}, [playerName])

	if (!wasPlayerPresentLastMatches(matches, playerName, clubId)) {
		return <div className='containerCentered'><p>{translated.clubPage.noMatches}</p></div>
	}

	return (
		<TableContainer style={{ boxShadow: 'none', backgroundColor: 'transparent', padding: '0 0 2em 0' }} component={Paper}>
			<Table aria-label='simple table' padding='normal'>
				<TableHead>
					<TableRow>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }}>{translated.singleWords.score}</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }}>{translated.singleWords.time}</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }}>Pos.</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.rating}</TableCell>
						{rows.some(row => row.position === 'goalkeeper') &&
							<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.gkSaves}</TableCell>
						}
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>SOTM</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.passes}</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.passAccuracy}</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.goals}</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.assists}</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.shots}</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.shotAccuracy}</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.tackles}</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.tackleAccuracy}</TableCell>
						<TableCell style={{ padding: '6px', fontWeight: 'bold' }} align='right'>{translated.singleWords.redCards}</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{rows.length > 0 && rows.map((row, i) => (
						<TableRow key={`tableRow-${i}`}>

							{/* score cell */}
							<TableCell component='th' scope='row' style={{ padding: '2px 3px' }} >
								<span
									className={`lastResults ${endResultColor(+row.score, +row.oppScore) || ''}`}
								>
									{row.score} - {row.oppScore}
								</span>
							</TableCell>

							<TableCell align='right' style={{ padding: '8px 2px' }} >{row.timeAgo}</TableCell>
							<TableCell
								align='right'
								style={{ padding: '8px 2px' }}
							>
								<div className={`posColor ${getPosColor(row.position)}`}></div>
							</TableCell>
							<TableCell align='right' style={{ padding: '8px 2px' }} >
								<span className={`${ratingColor(+row.rating)} attrBox`}>
									{row.rating}
								</span>
							</TableCell>
							{rows.some(row => row.position === 'goalkeeper') &&
								<TableCell align='right' style={{ padding: '8px 2px' }} >{row.gkSaves}</TableCell>
							}
							<TableCell align='right' style={{ padding: '8px 2px' }} >
								{row.sotm === 'Yes' ? <CheckIcon color='primary' fontSize='small' /> : row.sotm}
							</TableCell>
							<TableCell align='right' style={{ padding: '8px 2px' }} >{row.passes}</TableCell>
							<TableCell align='right' style={{ padding: '8px 2px' }} >{row.passRate}</TableCell>
							<TableCell align='right' style={{ padding: '8px 2px' }} >{row.goals}</TableCell>
							<TableCell align='right' style={{ padding: '8px 2px' }} >{row.assists}</TableCell>
							<TableCell align='right' style={{ padding: '8px 2px' }} >{row.shots}</TableCell>
							<TableCell align='right' style={{ padding: '8px 2px' }} >{row.shotRate}</TableCell>
							<TableCell align='right' style={{ padding: '8px 2px' }} >{row.tackles}</TableCell>
							<TableCell align='right' style={{ padding: '8px 2px' }} >{row.tackleRate}</TableCell>
							<TableCell align='right' style={{ padding: '8px 2px' }} >{row.redCards === 'No' ? translated.singleWords.no : row.redCards === 'Yes' ? translated.singleWords.yes : row.redCards}</TableCell>
						</TableRow>
					))}
				</TableBody>
			</Table>
		</TableContainer>
	)
}

export default RecentFormTable