import { Paper } from '@mui/material'
import { useTransContext } from '../../../context/transContext'
import { getAttrColor } from '../../../helpers'
import { IVProAttr } from '../../../models/interfaces'

const Physical = ({ attr }: { attr: IVProAttr }) => {

	const { translated } = useTransContext()

	return (
		<Paper className='playerClubPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>{translated.attributes.physical}</p>
			</div>
			<div style={{ padding: '0 1em 1em 1em' }} className='columnContainer'>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.jumping}</span>
					<div className={`attrBox ${getAttrColor(+attr.jumping)}`} >{attr.jumping}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.stamina}</span>
					<div className={`attrBox ${getAttrColor(+attr.stamina)}`} >{attr.stamina}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.strength} </span>
					<div className={`attrBox ${getAttrColor(+attr.strength)}`} >{attr.strength}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.reaction} </span>
					<div className={`attrBox ${getAttrColor(+attr.reactions)}`} >{attr.reactions}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.aggression}</span>
					<div className={`attrBox ${getAttrColor(+attr.aggression)}`} >{attr.aggression}</div>
				</div>
			</div>
		</Paper>
	)
}

export default Physical