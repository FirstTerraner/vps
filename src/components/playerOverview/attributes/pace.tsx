import { Paper } from '@mui/material'
import { useTransContext } from '../../../context/transContext'
import { getAttrColor } from '../../../helpers'
import { IVProAttr } from '../../../models/interfaces'

const Pace = ({ attr }: { attr: IVProAttr }) => {

	const { translated } = useTransContext()

	return (
		<Paper className='playerClubPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>{translated.attributes.pace}</p>
			</div>
			<div style={{ padding: '0 1em 1em 1em' }} className='columnContainer'>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.acceleration}</span>
					<div className={`attrBox ${getAttrColor(+attr.acceleration)}`} >{attr.acceleration}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.sprintSpeed}</span>
					<div className={`attrBox ${getAttrColor(+attr.sprintSpeed)}`} >{attr.sprintSpeed}</div>
				</div>
			</div>
		</Paper>
	)
}

export default Pace