import { Paper } from '@mui/material'
import { useTransContext } from '../../../context/transContext'
import { getAttrColor } from '../../../helpers'
import { IVProAttr } from '../../../models/interfaces'

const GoalKeeping = ({ attr }: { attr: IVProAttr }) => {

	const { translated } = useTransContext()

	return (
		<Paper className='playerClubPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>{translated.attributes.goalKeeping}</p>
			</div>
			<div style={{ padding: '0 1em 1em 1em' }} className='columnContainer'>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.diving}</span>
					<div className={`attrBox ${getAttrColor(+attr.gkDiving)}`} >{attr.gkDiving}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween'>
					<span>{translated.attributes.handling} </span>
					<div className={`attrBox ${getAttrColor(+attr.gkHandling)}`} >{attr.gkHandling}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween'>
					<span>{translated.attributes.kicking}</span>
					<div className={`attrBox ${getAttrColor(+attr.gkKicking)}`} >{attr.gkKicking}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween'>
					<span>{translated.attributes.reflexes}</span>
					<div className={`attrBox ${getAttrColor(+attr.gkReflexes)}`} >{attr.gkReflexes}</div>
				</div>
			</div>
		</Paper>
	)
}

export default GoalKeeping