import { Paper } from '@mui/material'
import { useTransContext } from '../../../context/transContext'
import { getAttrColor } from '../../../helpers'
import { IVProAttr } from '../../../models/interfaces'

const Shooting = ({ attr }: { attr: IVProAttr }) => {

	const { translated } = useTransContext()

	return (
		<Paper className='playerClubPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>{translated.attributes.shooting}</p>
			</div>
			<div style={{ padding: '0 1em 1em 1em' }} className='columnContainer'>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.finishing}</span>
					<div className={`attrBox ${getAttrColor(+attr.finishing)}`} >{attr.finishing}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.freeKick}</span>
					<div className={`attrBox ${getAttrColor(+attr.freekickAccuracy)}`} >{attr.freekickAccuracy}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.heading}</span>
					<div className={`attrBox ${getAttrColor(+attr.headingAccuracy)}`} >{attr.headingAccuracy}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.shotPower} </span>
					<div className={`attrBox ${getAttrColor(+attr.shotPower)}`} >{attr.shotPower}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.longShots} </span>
					<div className={`attrBox ${getAttrColor(+attr.longShots)}`} >{attr.longShots}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.volleys}</span>
					<div className={`attrBox ${getAttrColor(+attr.volleys)}`} >{attr.volleys}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.penalties}</span>
					<div className={`attrBox ${getAttrColor(+attr.penalties)}`} >{attr.penalties}</div>
				</div>
			</div>
		</Paper>
	)
}

export default Shooting