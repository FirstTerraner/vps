import { Paper } from '@mui/material'
import { useTransContext } from '../../../context/transContext'
import { getAttrColor } from '../../../helpers'
import { IVProAttr } from '../../../models/interfaces'

const Dribbling = ({ attr }: { attr: IVProAttr }) => {

	const { translated } = useTransContext()

	return (
		<Paper className='playerClubPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>{translated.attributes.dribbling}</p>
			</div>
			<div style={{ padding: '0 1em 1em 1em' }} className='columnContainer'>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.agility}</span>
					<div className={`attrBox ${getAttrColor(+attr.agility)}`} >{attr.agility}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.balance}</span>
					<div className={`attrBox ${getAttrColor(+attr.balance)}`} >{attr.balance}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.attackPos}</span>
					<div className={`attrBox ${getAttrColor(+attr.attackPosition)}`} >{attr.attackPosition}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.ballControl}</span>
					<div className={`attrBox ${getAttrColor(+attr.ballControl)}`} >{attr.ballControl}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.dribbling}</span>
					<div className={`attrBox ${getAttrColor(+attr.dribbling)}`} >{attr.dribbling}</div>
				</div>
			</div>
		</Paper>
	)
}

export default Dribbling