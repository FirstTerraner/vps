import { Paper } from '@mui/material'
import { useTransContext } from '../../../context/transContext'
import { getAttrColor } from '../../../helpers'
import { IVProAttr } from '../../../models/interfaces'

const Passing = ({ attr }: { attr: IVProAttr }) => {

	const { translated } = useTransContext()

	return (
		<Paper className='playerClubPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>{translated.attributes.passing}</p>
			</div>
			<div style={{ padding: '0 1em 1em 1em' }} className='columnContainer'>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.vision}</span>
					<div className={`attrBox ${getAttrColor(+attr.vision)}`} >{attr.vision}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.crossing}</span>
					<div className={`attrBox ${getAttrColor(+attr.crossing)}`} >{attr.crossing}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.longPass} </span>
					<div className={`attrBox ${getAttrColor(+attr.longPass)}`} >{attr.longPass}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.shortPass} </span>
					<div className={`attrBox ${getAttrColor(+attr.shortPass)}`} >{attr.shortPass}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.curve}</span>
					<div className={`attrBox ${getAttrColor(+attr.curve)}`} >{attr.curve}</div>
				</div>
			</div>
		</Paper>
	)
}

export default Passing