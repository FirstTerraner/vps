import { Paper } from '@mui/material'
import { useTransContext } from '../../../context/transContext'
import { getAttrColor } from '../../../helpers'
import { IVProAttr } from '../../../models/interfaces'

const Defending = ({ attr }: { attr: IVProAttr }) => {

	const { translated } = useTransContext()

	return (
		<Paper className='playerClubPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>{translated.attributes.defending}</p>
			</div>
			<div style={{ padding: '0 1em 1em 1em' }} className='columnContainer'>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.interceptions}</span>
					<div className={`attrBox ${getAttrColor(+attr.interceptions)}`} >{attr.interceptions}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.marking}</span>
					<div className={`attrBox ${getAttrColor(+attr.marking)}`} >{attr.marking}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.standTackle}</span>
					<div className={`attrBox ${getAttrColor(+attr.standTackle)}`} >{attr.standTackle}</div>
				</div>
				<div className='flexContainer itemsCenter justifyBetween mb-05'>
					<span>{translated.attributes.slideTackle}</span>
					<div className={`attrBox ${getAttrColor(+attr.slideTackle)}`} >{attr.slideTackle}</div>
				</div>
			</div>
		</Paper>
	)
}

export default Defending