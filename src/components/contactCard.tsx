import { Paper } from '@mui/material'
import { ReactElement } from 'react'

const ContactCard = ({ header, txt }: { header: string, txt: ReactElement<any, any> }) => (
	<Paper className='svgPaper paperBlueGreen width100'>
		<div className='leaderboardWrap'>
			<div className='leaderWrap z2'>
				<h3 className='titleTxt'>{header}</h3>
				<div className='mt-1 z2'>
					{txt}
				</div>
			</div>
		</div>
	</Paper>
)

export default ContactCard