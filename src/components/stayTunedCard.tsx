import { Paper } from '@mui/material'
import { useTransContext } from '../context/transContext'

const StayTunedCard = ({ svg1Src, svg2Src }: { svg1Src: string, svg2Src: string }) => {

	const { translated } = useTransContext()

	return (
		<Paper className='svgPaper paperBlueGreen width100'>
			<div className='leaderboardWrap'>
				<div className='leaderWrap z2'>
					<h3 className='titleTxt'>{translated.homepage.stayTuned}</h3>
					<div className='mt-1 z2'>
						<p>{translated.homepage.promo}</p>
					</div>
				</div>
				{/* svg */}
				<div className='svgWrapScissorBall'>
					<img src={svg1Src} alt='' />
				</div>
				<div className='svgWrapScissor'>
					<img src={svg2Src} alt='' />
				</div>
			</div>
		</Paper>
	)
}

export default StayTunedCard