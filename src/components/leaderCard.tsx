import { Paper } from '@mui/material'
import React from 'react'
import { useRouter } from 'next/router'
import { setPlatformLocalStorage } from '../localStorage/platform'
// import FormatListNumberedOutlinedIcon from '@mui/icons-material/FormatListNumberedOutlined'
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown'
import FiberNewIcon from '@mui/icons-material/FiberNew'
import { Button, Menu, MenuItem } from '@mui/material'
import { useTransContext } from '../context/transContext'

const LeaderboardCard = ({ svgSrc }: { svgSrc: string }) => {

	const router = useRouter()

	const { translated } = useTransContext()

	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

	const [leaderType, setLeaderType] = React.useState('season')

	const handleLeaderTxt = (leader: string) => {
		if (!leader) { return '--' }
		if (leader === 'season') { return translated.singleWords.seasons }
		if (leader === 'clubs') { return translated.singleWords.club }
		return translated.singleWords.record
	}

	const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
		setAnchorEl(event.currentTarget)
	}

	return (
		<Paper className='svgPaper paperBlueGreen'>
			<div className='leaderboardWrap'>
				<div className='leaderWrap z2'>
					<div className='flexContainer itemsCenter justifyCenter width100'>
						<div
							aria-controls='simple-menu3'
							aria-haspopup='true'
							onClick={handleClick}
							className='flexContainer itemsCenter'
						>
							<Button
								color='inherit'
								style={{ fontWeight: 'bold', fontSize: 17, margin: 0 }}
								aria-label='directions'
							>
								{handleLeaderTxt(leaderType)}
								<ArrowDropDownIcon fontSize='small' />
							</Button>
						</div>
						<Menu
							id='simple-menu3'
							anchorEl={anchorEl}
							keepMounted
							open={Boolean(anchorEl)}
							onClose={() => setAnchorEl(null)}
						>
							<MenuItem onClick={() => {
								setLeaderType('season')
								setAnchorEl(null)
							}}>
								<span>{translated.singleWords.seasons} </span>
							</MenuItem>
							<MenuItem onClick={() => {
								setLeaderType('clubs')
								setAnchorEl(null)
							}}>
								<span>{translated.singleWords.club}</span>
							</MenuItem>
							<MenuItem onClick={() => {
								setLeaderType('record')
								setAnchorEl(null)
							}}>
								<span className='flexContainer itemsCenter'><FiberNewIcon color='success' sx={{ marginRight: .5 }} />{translated.singleWords.record}</span>
							</MenuItem>
						</Menu>
						<h3 className='titleTxt'>{translated.singleWords.leaderboards}</h3>
					</div>
					<>
						<div className='flexContainer justifyCenter'>
							<p
								onClick={() => {
									setPlatformLocalStorage('ps4')
									void router.push(`/ps4/${leaderType}/leaderboards`)
								}}
								className='href mr-05'
							>PS4</p>
							<p
								onClick={() => {
									setPlatformLocalStorage('ps5')
									void router.push(`/ps5/${leaderType}/leaderboards`)
								}}
								className='href mr-05'
							>PS5</p>
							<p
								onClick={() => {
									setPlatformLocalStorage('xboxone')
									void router.push(`/xboxone/${leaderType}/leaderboards`)
								}}
								className='href mr-05'
							>XB1</p>
							<p
								onClick={() => {
									setPlatformLocalStorage('xbox-series-xs')
									void router.push(`/xbox-series-xs/${leaderType}/leaderboards`)
								}}
								className='href'
							>XBSXS</p>
							<p
								onClick={() => {
									setPlatformLocalStorage('pc')
									void router.push(`/pc/${leaderType}/leaderboards`)
								}}
								className='href ml-05'
							>PC</p>
						</div>
					</>
				</div>
				{/* svg */}
				<div className='svgWrap'>
					<img src={svgSrc} alt='' />
				</div>
			</div>
		</Paper>
	)
}

export default LeaderboardCard