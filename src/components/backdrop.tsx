import { Backdrop } from '@mui/material'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'
import { useBackdropContext } from '../context/backdropContext'
import { useTransContext } from '../context/transContext'
import { pkg } from '../helpers'

const SimpleBackdrop = () => {

	const router = useRouter()

	const { translated } = useTransContext()

	const { isBackdropOpen, close } = useBackdropContext()

	const [isInfoOpen, setIsInfoOpen] = React.useState(false)

	return (
		<Backdrop
			sx={{ color: '#fafafa', zIndex: 4, backgroundColor: '#000', opacity: '.85', padding: '1em', textAlign: 'center' }}
			open={isBackdropOpen}
			onClick={close}
		>
			<div className='searchPaper columnContainer itemsCenter'>
				{!isInfoOpen &&
					<>
						<h2 style={{ color: '#999' }}>{translated.homepage.backdropTitle}</h2>
						<p style={{ color: '#999' }} className='pointer'>
							{translated.homepage.backdrop1}
						</p>
						<p style={{ color: '#999' }}>
							{translated.homepage.backdrop2}
						</p>
						<p style={{ color: '#999' }} >
							{translated.homepage.backdrop3}
						</p>
						{!router.asPath.includes('leaderboards') &&
							<p style={{ color: '#999' }} >
								{translated.homepage.backdrop4}
							</p>
						}
						<div className='flexContainer itemsCenter'>
							<h3
								style={{ color: '#999' }}
								className='href pointer mr-1'
								onClick={e => {
									e.stopPropagation()
									setIsInfoOpen(true)
								}}
							>
								{translated.singleWords.moreInfo}
							</h3>
							<h3
								style={{ color: '#999' }}
								className='href pointer'
							>
								{translated.singleWords.close}
							</h3>
						</div>
					</>
				}
				{isInfoOpen &&
					<>
						<p style={{ color: '#999' }} >
							{translated.homepage.backdrop5}
						</p>
						<p style={{ color: '#999' }} >
							{translated.homepage.backdropInfo}
							<br />
							{translated.singleWords.currently} v{pkg.version}
						</p>
						<p style={{ color: '#999' }} >
							<p style={{ color: '#999' }} >
								{translated.homepage.backdrop6}
							</p>
							<Link href='https://gitlab.com/FirstTerraner/vps/-/issues/10'><a className='href whiteTxt' target='_blank'>{translated.homepage.issueTracker}</a></Link>.
						</p>

						<h3
							style={{ color: '#999' }}
							className='href pointer'
							onClick={() => setIsInfoOpen(false)}
						>
							OK
						</h3>
					</>
				}

			</div>
		</Backdrop >
	)
}

export default SimpleBackdrop