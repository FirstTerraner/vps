import { Backdrop } from '@mui/material'
import React from 'react'
import { useBackdropContext } from '../context/backdropContext'

const NavDrop = () => {

	const { withNav, closeNav } = useBackdropContext()

	return (
		<Backdrop
			style={{ color: '#fafafa', zIndex: 4, backgroundColor: '#000', opacity: '.85', padding: '1em', textAlign: 'center' }}
			open={withNav}
			onClick={closeNav}
			className='gonimate backdropFadeIn'
			key={withNav ? '1' : '0'}
		>
		</Backdrop >
	)
}

export default NavDrop