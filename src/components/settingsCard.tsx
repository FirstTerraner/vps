import { FormControlLabel, FormGroup, Paper, Switch } from '@mui/material'
import FiberNewIcon from '@mui/icons-material/FiberNew'
import { useTransContext } from '../context/transContext'
import LanguageMenu from './langDropdown'

const SettingsCard = () => {

	const { translated } = useTransContext()

	return (
		<Paper className='searchPaper paperBlueGreen'>
			<div
				className='overviewPanelHead width95 pointer'
				style={{ padding: '0 .7em' }}
			>
				<div className='flexContainer itemsCenter width100'>
					<div className='mr-05 flexContainer'>
						<FiberNewIcon color='success' fontSize='large' />
					</div>
					<p className='panelHeaderTxt'>{translated.singleWords.general}</p>
				</div>
			</div>
			<div className='width100'>
				<div className='columnContainer itemsCenter'>
					<div className='flexContainer itemsCenter justifyBetween' style={{ width: '95%' }}>
						<p>{translated.settingsPage.chooseLang}</p>
						<LanguageMenu />
					</div>
					<div className='flexContainer itemsCenter justifyBetween' style={{ width: '95%' }}>
						<p>{translated.singleWords.darkMode}</p>
						<FormGroup>
							{/* <FormControlLabel
								control={<Switch defaultChecked />}
								label="Label"
								disabled
							/> */}
							<FormControlLabel disabled control={<Switch />} label={translated.singleWords.off} />
						</FormGroup>
					</div>
				</div>
			</div>

		</Paper>
	)
}

export default SettingsCard