/* eslint-disable quotes */
import CircularProgress from '@mui/material/CircularProgress'
import { useRouter } from 'next/router'
import Image from 'next/image'
import React from 'react'
import appNameLight from '../../public/imgs/appNameLight.png'
import { useTransContext } from '../context/transContext'

const Loading = () => {

	const router = useRouter()

	const { translated } = useTransContext()

	const [loading, setLoading] = React.useState(false)

	const [targetUrl, setTargetUrl] = React.useState('')

	React.useEffect(() => {
		const handleStart = (url: string) => {
			setTargetUrl(url)
			return (url !== router.asPath) && setLoading(true)
		}
		const handleComplete = (url: string) => (
			url === router.asPath ||
			router.asPath.includes('season/leaderboards') ||
			router.asPath.includes('club/leaderboards') ||
			router.asPath.includes('record/leaderboards') ||
			router.asPath.includes('squad') ||
			router.asPath.includes('/search/') ||
			router.locale
		) && setLoading(false)

		router.events.on('routeChangeStart', handleStart)
		router.events.on('routeChangeComplete', handleComplete)
		router.events.on('routeChangeError', handleComplete)
		// router.events.on('hashChangeStart', handleStart)
		// router.events.on('hashChangeComplete', handleComplete)
		return () => {
			router.events.off('routeChangeStart', handleStart)
			router.events.off('routeChangeComplete', handleComplete)
			router.events.off('routeChangeError', handleComplete)
			// router.events.off('hashChangeStart', handleStart)
			// router.events.off('hashChangeComplete', handleComplete)
		}
	})

	if (!loading) { return null }

	return (
		<div className='loadingLayer gonimate fadeIn'>
			<CircularProgress />
			<div style={{ margin: '1em 0 0 0' }}>
				<Image
					src={appNameLight.src}
					alt='Virtual Pro Space'
					width={200}
					height={30}
				/>
			</div>
			{((targetUrl.includes('club') && !targetUrl.includes('clubname')) && !targetUrl.includes('record') ||
				targetUrl.includes('season/leaderboards') ||
				targetUrl.includes('clubs/leaderboards')) &&
				<h3 className='whiteTxt txtCenter'>
					{translated.loading.waiting}
					<br />
					<span style={{ fontSize: '12px' }}>{translated.loading.eaSlow}</span>
				</h3>
			}
		</div>
	)
}

export default Loading