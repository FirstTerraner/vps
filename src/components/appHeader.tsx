import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import { useRouter } from 'next/router'
import { isPlatformType } from 'proclubs-api/dist'
import { cleanPlatform, clubCrestUrl, nationUrl } from '../helpers'
import { IHeaderNavProps } from '../models/interfaces'
import Image from 'next/image'
import HeaderNav from './headerNav'
import appLogoLight from '../../public/imgs/appLogoLight.png'
import appNameLight from '../../public/imgs/appNameLight.png'

const AppHeader = ({ pageName, clubInfo, clubMember, showFav, showPlat }: IHeaderNavProps) => {

	const router = useRouter()

	// console.log('clubInfo: ', clubInfo)

	return (
		<>
			{/* header navigation contains the favorites state logic */}
			{/* position absolute */}
			<HeaderNav clubInfo={clubInfo} clubMember={clubMember} showFav={showFav} showPlat={showPlat} pageName={pageName} />

			{/* overview image and page info */}
			<div className='overviewLayer whiteTxt'>

				{clubInfo ?
					<>
						<div>
							<Image
								src={clubCrestUrl(clubInfo.teamId)} // club.clubInfo.customKit.isCustomTeam === '0' ? club.clubInfo.teamId : +club.clubInfo.customKit.crestAssetId
								alt='crest'
								width={clubMember ? 50 : 70}
								height={clubMember ? 50 : 70}
								className='z2 pointer'
								onClick={() => {
									if (router.query.name) {
										void router.push(`/${router.query.platform?.toString() || ''}/club/${clubInfo.clubId}`)
									}
								}}
							/>
						</div>

						<h3 className='z2' style={{ margin: '.1em 0' }}>{clubInfo.name}</h3>

						{clubMember &&
							<div className='flexContainer itemsCenter z2'>
								{clubMember.proNationality.length > 0 ?
									<div>
										<Image
											src={nationUrl(clubMember.proNationality)}
											alt='nation'
											width={30}
											height={20}
										/>
									</div>
									:
									<PersonOutlineIcon />
								}
								<h2 style={{ margin: '0 0 0 .5em' }}>{clubMember.name}</h2>
							</div>
						}

						<span
							className='mb-05 mt-05 z2'
							style={{ fontSize: '13px' }}
						>
							{isPlatformType(router.query.platform) && cleanPlatform(router.query.platform)}
						</span>
					</>
					:
					<>
						<div style={{ margin: '0 0 .5em 0' }}>
							<Image
								src={appLogoLight.src}
								alt='logo'
								width={40}
								height={55}
								className='mb-1 z2'
							/>
						</div>
						<div>
							<Image
								src={appNameLight.src}
								alt='Virtual Pro Space'
								width={155}
								height={25}
								className='z2'
							/>
						</div>

						<h3 className='pageName z2'>{pageName}</h3>
					</>
				}

			</div>

			{/* dark the image */}
			<div className='overviewShadowed'></div>

		</>
	)
}

export default AppHeader