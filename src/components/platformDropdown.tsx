import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown'
import { Button, Menu, MenuItem } from '@mui/material'
import { useRouter } from 'next/router'
import { isPlatformType, TPlatformType } from 'proclubs-api/dist'
import React from 'react'
import { upperCasePlatform } from '../helpers'
import { getPlatformLocalStorage, setPlatformLocalStorage } from '../localStorage/platform'

const PlatformDropdown = ({ padding }: { padding: boolean }) => {

	const router = useRouter()

	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

	const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
		setAnchorEl(event.currentTarget)
	}

	const [platform, setPlatform] = React.useState<TPlatformType>()

	React.useEffect(() => {
		const localPlat = getPlatformLocalStorage()
		if (!isPlatformType(localPlat) || !isPlatformType(router.query.platform)) {
			// set platform in local storage to ps4 as default
			setPlatformLocalStorage('ps4')
			return
		}
		setPlatform(localPlat || router.query.platform || 'ps4')
	}, [])

	return (
		<>
			<div
				aria-controls='simple-menu2'
				aria-haspopup='true'
				onClick={handleClick}
			>

				<Button
					color='primary'
					style={{ padding: padding ? '10px' : '0' }}
					aria-label='directions'
				>
					{router.asPath.includes('leaderboards') && isPlatformType(router.query.platform) ?
						upperCasePlatform(router.query.platform)
						:
						isPlatformType(platform) ? upperCasePlatform(platform) : 'PS4'
					}
					<ArrowDropDownIcon fontSize='small' />
				</Button>

			</div>

			<Menu
				id='simple-menu2'
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={() => setAnchorEl(null)}
			>

				<MenuItem onClick={() => {
					setPlatform('ps4')
					setPlatformLocalStorage('ps4')
					setAnchorEl(null)
					if (router.asPath.includes('leaderboards')) {
						const leaderType = router.query.leader?.toString()
						void router.replace(`/ps4/${leaderType || ''}/leaderboards`)
					}
				}}>
					<span>PS4</span>
				</MenuItem>

				<MenuItem onClick={() => {
					setPlatform('ps5')
					setPlatformLocalStorage('ps5')
					setAnchorEl(null)
					if (router.asPath.includes('leaderboards')) {
						const leaderType = router.query.leader?.toString()
						void router.replace(`/ps5/${leaderType || ''}/leaderboards`)
					}
				}}>
					<span>PS5</span>
				</MenuItem>

				<MenuItem onClick={() => {
					setPlatform('xboxone')
					setPlatformLocalStorage('xboxone')
					setAnchorEl(null)
					if (router.asPath.includes('leaderboards')) {
						const leaderType = router.query.leader?.toString()
						void router.replace(`/xboxone/${leaderType || ''}/leaderboards`)
					}
				}}>
					<span>XB1</span>
				</MenuItem>

				<MenuItem onClick={() => {
					setPlatform('xbox-series-xs')
					setPlatformLocalStorage('xbox-series-xs')
					setAnchorEl(null)
					if (router.asPath.includes('leaderboards')) {
						const leaderType = router.query.leader?.toString()
						void router.replace(`/xbox-series-xs/${leaderType || ''}/leaderboards`)
					}
				}}>
					<span>XBSXS</span>
				</MenuItem>

				<MenuItem onClick={() => {
					setPlatform('pc')
					setPlatformLocalStorage('pc')
					setAnchorEl(null)
					if (router.asPath.includes('leaderboards')) {
						const leaderType = router.query.leader?.toString()
						console.log('leaderType: ', leaderType)
						void router.replace(`/pc/${leaderType || ''}/leaderboards`)
					}
				}}>
					<span>PC</span>
				</MenuItem>

			</Menu>
		</>
	)
}

export default PlatformDropdown