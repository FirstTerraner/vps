import React from 'react'
import GradeIcon from '@mui/icons-material/Grade'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import ExpandLessIcon from '@mui/icons-material/ExpandLess'
import { IHighestClub } from '../models/interfaces'
import { Paper } from '@mui/material'
import Image from 'next/image'
import { clubCrestUrl, upperCasePlatform } from '../helpers'
import { useTransContext } from '../context/transContext'

const highestRecordCard = ({ highestRecords }: { highestRecords: IHighestClub }) => {

	const { translated } = useTransContext()

	// const translated.singleWords = singleWords[localeT && isTLocale(localeT) ? localeT : 'en-US']

	const [isHighestOpen, seIsHighestOpen] = React.useState(false)

	return (
		<Paper className='clubSearchPaper paperBlueGreen'>
			<div
				className='flexContainer itemsCenter width100 pointer'
				onClick={() => seIsHighestOpen(prev => !prev)}
			>
				<p className='panelHeaderTxt ml-05'>Top {translated.singleWords.record}</p>
				{isHighestOpen ? <ExpandLessIcon /> : <ExpandMoreIcon />}
			</div>
			{isHighestOpen &&
				<div className='clubSearchWrap m-05'>
					<div className='flexContainer itemsCenter'>
						<div style={{ marginLeft: '.3em' }}><GradeIcon color='primary' /></div>
						<div className='shortClubInfoRow ml-05'>
							<div className='mr-05'>
								<Image
									src={clubCrestUrl(highestRecords.clubInfo.teamId)}
									alt='crest'
									width={40}
									height={40}
								/>
							</div>
							{upperCasePlatform(highestRecords.plat)} | {highestRecords.name.length > 10 ? highestRecords.name.substring(0, 10) + '...' : highestRecords.name}
						</div>
					</div>
					<div className='flexContainer itemsCenter' style={{ marginRight: '.3em' }}>
						<div className='shortClubInfoRow'>
							<span>{highestRecords.wins}&nbsp;&#8226;&nbsp;{highestRecords.ties}&nbsp;&#8226;&nbsp;{highestRecords.losses}</span>
						</div>
					</div>
				</div>
			}
			{/* {isHighestOpen && highestRecords?.map((recordClub, i) => (

			))} */}
		</Paper>
	)
}

export default highestRecordCard