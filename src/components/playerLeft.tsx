import FlightTakeoffIcon from '@mui/icons-material/FlightTakeoff'
import { useRouter } from 'next/router'
import { useTransContext } from '../context/transContext'
import { IPlayerLeftProps } from '../models/interfaces'

const PlayerLeft = ({ clubName }: IPlayerLeftProps) => {
	// console.log('playersArray: ', playersArr)
	const router = useRouter()

	const { translated } = useTransContext()

	return (
		<div className='pageWrapper'>
			<div className='playerLeft'>
				<div style={{ marginTop: '5em' }} className='columnContainer itemsCenter'>
					<FlightTakeoffIcon fontSize='large' color='primary' />
					<h3 style={{ textAlign: 'center' }}><span className='bold'>{router.query.name}</span> {translated.singleWords.hasLeft} {clubName}.</h3>
				</div>
				<p style={{ textAlign: 'center', padding: '0 1em' }}>
					{translated.clubPage.findPlayer} {translated.clubPage.join}
				</p>
			</div>
		</div>
	)
}

export default PlayerLeft