import React from 'react'
import MenuIcon from '@mui/icons-material/Menu'
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined'
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined'
import FormatListNumberedOutlinedIcon from '@mui/icons-material/FormatListNumberedOutlined'
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined'
import CodeOutlinedIcon from '@mui/icons-material/CodeOutlined'
import AddTaskOutlinedIcon from '@mui/icons-material/AddTaskOutlined'
import SettingsIcon from '@mui/icons-material/Settings'
import CloseIcon from '@mui/icons-material/Close'
import AttachMoneyOutlinedIcon from '@mui/icons-material/AttachMoneyOutlined'
import { useRouter } from 'next/router'
import { getPlatformLocalStorage } from '../localStorage/platform'
import { useBackdropContext } from '../context/backdropContext'
import { Box, SpeedDial, SpeedDialAction } from '@mui/material'
import { useTransContext } from '../context/transContext'
// import { makeStyles } from '@mui/styles'

/* USESTYLES TO OVERWRITE THE TOOLTIP IN THE SPEED-DIAL-ACTION COMPONENT DOES NOT WORK WHILE TOOLTIPOPEN IS SET TO TRUE! */

/* const useStyles = makeStyles({
	tooltip: {
		backgroundColor: 'red',
		color: '#fafafa',
		whiteSpace: 'nowrap',
		maxWidth: 'none'
	}
}) */

/*

ENDED UP OVERWRITING CSS PROP AS FOLLOW:

.MuiSpeedDialAction-staticTooltipLabel, .MuiSpeedDialAction-fab, .MuiSpeedDialAction-fabClosed {
	transition: all .3s ease-in-out;
	background-color: #333!important;
	color: #fafafa!important;
}

.MuiSpeedDialAction-staticTooltipLabel {
	max-width: none;
	white-space: nowrap;
	cursor: pointer
}

.MuiSpeedDialAction-fab:hover, .MuiSpeedDialAction-fabClosed:hover {
	background-color: #666!important;
	color: #fafafa!important;
}

*/

const getActions = (pageNames: string[]) => {
	if (!pageNames || pageNames.length < 5) { return [] }
	return [
		{ icon: <HomeOutlinedIcon />, name: pageNames[0], path: '/' },
		{ icon: <SearchOutlinedIcon />, name: pageNames[1], path: '/club/search/clubname' },
		{ icon: <FormatListNumberedOutlinedIcon />, name: pageNames[2], path: '/record/leaderboards' },
		{ icon: <InfoOutlinedIcon />, name: pageNames[3], path: '/about' },
		{ icon: <AddTaskOutlinedIcon />, name: pageNames[4], path: '/upcoming' },
		{ icon: <CodeOutlinedIcon />, name: pageNames[5], path: '/contribute' },
		{ icon: <AttachMoneyOutlinedIcon />, name: pageNames[6], path: '/donations' },
		/* { icon: <FlashOnOutlinedIcon />, name: pageNames[7], path: '/release' }, */
		{ icon: <SettingsIcon />, name: pageNames[7], path: '/settings' },
	]
}

const Nav = () => {

	// const classes = useStyles()

	const router = useRouter()

	const { translated } = useTransContext()

	const actions = getActions(['Home', translated.singleWords.search, translated.singleWords.leaderboards, translated.singleWords.aboutUs, translated.singleWords.soon, translated.singleWords.contribute, translated.singleWords.donate, translated.singleWords.settings])

	const { withNav, openNav, closeNav } = useBackdropContext()

	// console.log(withNav)

	return (
		<Box style={{ position: 'fixed', bottom: 9, right: 9, zIndex: 5 }}>
			<SpeedDial
				ariaLabel='Menu'
				style={{ position: 'absolute', bottom: 9, right: 9 }}
				icon={withNav ? <CloseIcon /> : <MenuIcon />}
				onOpen={openNav}
				onClose={closeNav}
			>
				{actions.map(action => (
					<SpeedDialAction
						key={action.name}
						icon={action.icon}
						tooltipTitle={action.name}
						tooltipOpen
						/* TooltipClasses={classes} */
						onClick={() => {
							closeNav()
							const platform = router.query.platform?.toString() || getPlatformLocalStorage() || 'ps5'
							void router.push(action.path.includes('/search/') || action.path.includes('/leaderboards') ? `/${platform}${action.path}` : action.path)
						}}
					/>
				))}
			</SpeedDial>
		</Box>
	)
}

export default Nav