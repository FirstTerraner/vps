import { Paper, Button } from '@mui/material'
import { useRouter } from 'next/router'
import { useTransContext } from '../context/transContext'

const SoonCard = ({ svgSrc }: { svgSrc: string }) => {

	const router = useRouter()

	const { translated } = useTransContext()

	return (
		<Paper className='svgPaper paperBlueGreen'>
			<div className='leaderboardWrap'>
				<div className='leaderWrap z2'>
					<div className='flexContainer itemsCenter justifyCenter width100'>
						<div className='flexContainer mr-05'>🧪</div>
						<h3 className='titleTxt'>{translated.singleWords.soon}</h3>
					</div>
					<div className='mt-1 z2'>
						<Button
							variant='contained'
							color='primary'
							className='blueBtn'
							/* endIcon={<CodeOutlinedIcon />} */
							onClick={() => router.push('/upcoming')}
						>
							{'< '}{translated.singleWords.buildFeatures}{' />'}
						</Button>
					</div>
				</div>
				{/* svg */}
				<div className='svgWrapR'>
					<img src={svgSrc} alt='' />
				</div>
			</div>
		</Paper>
	)
}

export default SoonCard