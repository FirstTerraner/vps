// libs
import { Paper } from '@mui/material'
import { useRouter } from 'next/router'
import Image from 'next/image'
import React from 'react'
// icons
import SmallChip from './chip'
import PersonIcon from '@mui/icons-material/Person'
import SecurityIcon from '@mui/icons-material/Security'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import HighlightOffOutlinedIcon from '@mui/icons-material/HighlightOffOutlined'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import ExpandLessIcon from '@mui/icons-material/ExpandLess'
// helpers
import { nationUrl, upperCasePlatform } from '../helpers'
import { getFavClubsLocalStorage, removeFavClubLocalStorage } from '../localStorage/favClubs'
import { getFavPlayersLocalStorage, removeFavPlayerLocalStorage } from '../localStorage/favPlayers'
import { IFavClub, IFavPlayer } from '../models/interfaces'
import { isArr } from '../models/typeGuards'
import appLogo from '../../public/imgs/appLogo.png'
import { useTransContext } from '../context/transContext'


const FavClubs = () => {

	const router = useRouter()

	const { translated } = useTransContext()

	const [favClubs, setFavClubs] = React.useState<IFavClub[]>([])

	const [favPlayers, setFavPlayers] = React.useState<IFavPlayer[]>([])

	const [isFavOpen, setIsFavOpen] = React.useState(false)

	React.useEffect(() => {
		const favClubsLocal = getFavClubsLocalStorage()
		if (isArr(favClubsLocal)) { setFavClubs(favClubsLocal) }
		const favPlayersLocal = getFavPlayersLocalStorage()
		if (!isArr(favPlayersLocal)) { return }
		setFavPlayers(favPlayersLocal)
	}, [])

	return (
		<Paper className='favPaper paperBlueGreen'>
			<div
				className='overviewPanelHead pointer width95'
				onClick={() => setIsFavOpen(favOpen => !favOpen)}
			>
				<div className='flexContainer itemsCenter'>
					<div className='flexContainer itemsCenter'>
						<div className='mr-05 flexContainer'>
							<FavoriteBorderIcon />
						</div>
						<p className='panelHeaderTxt'>{translated.homepage.favs}</p>
					</div>
					{(favClubs.length > 0 || favPlayers.length > 0) &&
						<p>{isFavOpen ? <ExpandLessIcon /> : <ExpandMoreIcon />}</p>
					}
				</div>

				<div className='flexContainer itemsCenter'>
					<div className='mr-05'>
						<SmallChip icon={<SecurityIcon />} label={`${favClubs.length}`} />
					</div>
					<SmallChip icon={<PersonIcon />} label={`${favPlayers.length}`} />
				</div>
			</div>
			<div className='columnContainer itemsCenter width100 ovHidden'>
				{favClubs.length > 0 || favPlayers.length > 0 ?
					isFavOpen ?
						<div
							className='allFavsWrap gonimate fadeInDown'
							key={isFavOpen ? 'openFav' : 'closedFav'}
						>
							{favClubs.length > 0 && favClubs.map(favClub => (
								<div
									key={`favClubId-${favClub.clubId}`}
									className='favClubWrap pointer'
									onClick={() => {
										void router.push(`/${favClub.platform || ''}/club/${favClub.clubId}`)
									}}
								>
									<div className='flexContainer itemsCenter'>
										{favClub.crestUrl.length > 0 &&
											<div style={{ margin: '0 .5em 0 0' }}>
												<Image
													src={favClub.crestUrl}
													alt='club crest'
													width={50}
													height={50}
												/>
											</div>
										}
										<p>{favClub.clubName}</p>
									</div>
									<div className='flexContainer itemsCenter'>
										<p>{upperCasePlatform(favClub.platform)}</p>
										<p>
											<HighlightOffOutlinedIcon
												fontSize='small'
												color='error'
												style={{ margin: '.2em 0 0 .5em' }}
												onClick={e => {
													e.stopPropagation()
													removeFavClubLocalStorage(favClub.clubId)
													const favs = getFavClubsLocalStorage()
													setFavClubs(isArr(favs) ? favs : [])
												}}
											/>
										</p>
									</div>
								</div>
							))}
							{favPlayers.length > 0 && favPlayers.map(player => (
								<div
									key={`favPlayer-${player.playerName}`}
									className='favClubWrap pointer'
									onClick={() => {
										void router.push(`/${player.platform}/club/${player.clubId}/squad/${player.playerName}`)
									}}
								>
									<div className='flexContainer itemsCenter'>
										<PersonOutlineIcon fontSize='large' sx={{ marginLeft: 1 }} />
										<div className='flexContainer itemsCenter'>
											{player.nationUrl !== '' ?
												<div style={{ margin: '0 .5em 0 1em' }}>
													<Image
														src={nationUrl(player.nationUrl)}
														alt='nation'
														width={30}
														height={20}
													/>
												</div>
												:
												<div style={{ margin: '0 .5em 0 1.2em' }}>
													<Image
														src={appLogo.src}
														alt='nation'
														width={20}
														height={27}
													/>
												</div>
											}
											<p>{player.playerName}</p>
										</div>
									</div>
									<div className='flexContainer itemsCenter'>
										<p>{upperCasePlatform(player.platform)}</p>
										<p>
											<HighlightOffOutlinedIcon
												fontSize='small'
												color='error'
												style={{ margin: '.2em 0 0 .5em' }}
												onClick={e => {
													e.stopPropagation()
													removeFavPlayerLocalStorage(player.playerName)
													const favs = getFavPlayersLocalStorage()
													setFavPlayers(isArr(favs) ? favs : [])
												}}
											/>
										</p>
									</div>
								</div>
							))}
						</div>
						:
						null
					:
					<p className='ml-05 mr-05'>{translated.homepage.saveFavs}</p>
				}
			</div>
		</Paper>

	)
}

export default FavClubs