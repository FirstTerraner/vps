import { Button, Menu, MenuItem } from '@mui/material'
import React from 'react'
import { useTransContext } from '../context/transContext'
import { setLanguageCookie } from '../cookies/language'
import { getLangFromPathLocale } from '../helpers'
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown'

const LanguageMenu = () => {

	const { localeT, setLocaleT } = useTransContext()

	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

	const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
		setAnchorEl(event.currentTarget)
	}

	return (
		<>
			<div
				aria-controls='simple-menu2'
				aria-haspopup='true'
				onClick={handleClick}
			>
				<Button
					color='primary'
					aria-label='directions'
					style={{ padding: '0' }}
				>
					{getLangFromPathLocale(localeT)}
					<ArrowDropDownIcon fontSize='small' />
				</Button>

			</div>
			<Menu
				id='simple-menu3'
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={() => setAnchorEl(null)}
			>

				<MenuItem onClick={() => {
					setLocaleT('en-US')
					setLanguageCookie('en-US', 365)
					setAnchorEl(null)
				}}>
					<span>🇬🇧 English</span>
				</MenuItem>

				<MenuItem onClick={() => {
					setLocaleT('de-DE')
					setLanguageCookie('de-DE', 365)
					setAnchorEl(null)
				}}>
					<span>🇩🇪 German</span>
				</MenuItem>

				<MenuItem onClick={() => {
					setLocaleT('fr-FR')
					setLanguageCookie('fr-FR', 365)
					setAnchorEl(null)
				}}>
					<span>🇫🇷 French</span>
				</MenuItem>

				<MenuItem onClick={() => {
					setLocaleT('nl-NL')
					setLanguageCookie('nl-NL', 365)
					setAnchorEl(null)
				}}>
					<span>🇳🇱 Dutch</span>
				</MenuItem>

			</Menu></>

	)
}

export default LanguageMenu