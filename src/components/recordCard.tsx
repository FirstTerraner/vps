import AddOutlinedIcon from '@mui/icons-material/AddOutlined'
import ExpandLessIcon from '@mui/icons-material/ExpandLess'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import FiberNewIcon from '@mui/icons-material/FiberNew'
import NewReleasesIcon from '@mui/icons-material/NewReleases'
import { CircularProgress, Divider, IconButton, InputBase, Paper } from '@mui/material'
import { useRouter } from 'next/router'
import { isPlatformType } from 'proclubs-api/dist'
import { IClubSearch } from 'proclubs-api/dist/model/club'
import React from 'react'
import { addRecordById, addRecordByName } from '../proclubsApis'
import { useBackdropContext } from '../context/backdropContext'
import { getPlatformLocalStorage } from '../localStorage/platform'
import { isArr } from '../models/typeGuards'
import SmallChip from './chip'
import ClubResult from './clubResult'
import PlatformDropdown from './platformDropdown'
import { useTransContext } from '../context/transContext'

const AddRecordCard = () => {

	const router = useRouter()

	const { translated } = useTransContext()

	const [isCardOpen, setIsCardOpen] = React.useState(false)

	// search states
	const [msg, setMsg] = React.useState('')

	const [clubName, setClubName] = React.useState('')

	const [clubAdded, setClubAdded] = React.useState(false)

	const [isSearching, setIsSearching] = React.useState(false)

	// results states
	const [recClubs, setRecClubs] = React.useState<null | IClubSearch[]>()

	const handleRecResp = (resp: 0 | 2 | 1 | IClubSearch[] | 3 | null) => {
		setIsSearching(false)
		if (!resp) {
			setMsg('No club found.')
			return
		}
		if (resp === 2) {
			setMsg('Club already exists in record leaderboard.')
			return
		}
		if (resp === 3) {
			setMsg('Club record is too low.')
			return
		}
		if (resp === 1) {
			setClubAdded(true)
			setMsg('Record club added successfully!')
			return
		}
		if (isArr(resp) && resp.length > 1) {
			setRecClubs(resp)
			setMsg('Found more than one club. Please choose one.')
		}
	}

	// backdrop container for more infos
	const { toggleBackdrop } = useBackdropContext()

	return (
		<>
			<Paper className='searchPaper paperBlueGreen'>
				<div
					className='overviewPanelHead width95 pointer'
					style={{ padding: '0 .7em' }}
					onClick={() => setIsCardOpen(prev => !prev)}
				>
					<div className='flexContainer itemsCenter width100'>
						<div className='mr-05 flexContainer'><FiberNewIcon color='success' fontSize='large' /></div>
						<p className='panelHeaderTxt'>{translated.homepage.addRec}</p>
						{isCardOpen ? <ExpandLessIcon /> : <ExpandMoreIcon />}
					</div>
					<SmallChip icon={<NewReleasesIcon />} label='BETA' />
				</div>

				{isCardOpen ?
					<>
						<p onClick={toggleBackdrop} className='href'>{translated.homepage.howItWorks}</p>
						<div className='searchInputWrapper'>
							<PlatformDropdown padding />
							<Divider style={{ height: '28 px', margin: '4px' }} orientation='vertical' />
							<InputBase
								style={{ marginLeft: '5px', flex: '1' }}
								type='search'
								name='search'
								autoComplete='on'
								placeholder='Clubname'
								onChange={e => {
									setClubName(e.target.value)
								}}
								onKeyUp={async e => {
									// reset states while tiping
									setClubAdded(false)
									setMsg('')
									if (e.key !== 'Enter') { return }
									setIsSearching(true)
									// if key is enter
									if (clubName.length < 5) {
										setMsg(translated.homepage.min5)
										return
									}
									const plat = getPlatformLocalStorage()
									if (!isPlatformType(plat)) {
										setMsg(translated.homepage.badResp)
										return
									}
									const recordResp = await addRecordByName(plat, clubName)
									// console.log('test recordResp: ', recordResp)
									handleRecResp(recordResp)
								}}
							/>
							<IconButton
								style={{ padding: '10px' }}
								aria-label='search'
								onClick={async () => {
									console.log('icon button')
									if (clubName.length < 5) {
										setMsg(translated.homepage.min5)
										return
									}
									const plat = getPlatformLocalStorage()
									if (!isPlatformType(plat)) {
										setMsg(translated.homepage.badResp)
										return
									}
									setIsSearching(true)
									const recordResp = await addRecordByName(plat, clubName)
									// console.log('test recordResp: ', recordResp)
									handleRecResp(recordResp)
								}}
							>
								{isSearching ? <CircularProgress size={20} /> : <AddOutlinedIcon color='primary' />}
							</IconButton>
						</div>

						{/* notification msgs */}
						{msg.length > 0 &&
							<>
								<p className={clubAdded ? 'greenTxt txtCenter' : 'redTxt txtCenter'}>
									{isSearching ?
										<CircularProgress size={15} />
										:
										<span>
											{msg + ' '}
											<span
												className='href' onClick={() => {
													setClubAdded(false)
													setMsg('')
												}}
											>
												{translated.singleWords.close}
											</span>
										</span>
									}
									<br />
									{clubAdded &&
										<span
											onClick={() => {
												if (router.asPath.includes('leaderboards')) {
													void router.reload()
													return
												}
												void router.push(`/${router.query.platform?.toString() || getPlatformLocalStorage() || 'ps4'}/record/leaderboards`)
											}}
											className='href'
										>
											{translated.singleWords.show}
										</span>
									}
								</p>
								{recClubs && recClubs?.length > 0 && recClubs.map((club, i) => (
									<div
										className='rankingResultsWrap'
										key={i}
										onClick={async e => {
											e.stopPropagation()

											setRecClubs([club])

											const plat = getPlatformLocalStorage()
											if (!isPlatformType(plat)) {
												setMsg('No platform found in local storage.')
												return
											}
											setIsSearching(true)
											const recordResp = await addRecordById(plat, club.clubId)
											// console.log('test recordResp by ID: ', recordResp)
											handleRecResp(recordResp)
										}}
									>
										<ClubResult club={club} i={i} setRec isRec fullWidth />
									</div>
								))
								}
							</>
						}
					</>

					: null
				}

			</Paper>

		</>
	)
}

export default AddRecordCard