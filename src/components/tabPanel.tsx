
import { Box } from '@mui/material'
import { ITabPanelProps } from '../models/interfaces'
import { isNum } from '../models/typeGuards'

const TabPanel = (props: ITabPanelProps) => {
	if (!props.index && !isNum(props.index)) { return null }
	const { children, value, index } = props
	return (
		<div
			role='tabpanel'
			hidden={value !== index}
			id={`scrollable-auto-tabpanel-${index}`}
			aria-labelledby={`scrollable-auto-tab-${index}`}
		/* {...other} */
		>
			{value === index && (
				<Box style={{ padding: '24px 0' }} p={3}>
					{children}
				</Box>
			)}
		</div>
	)
}

export default TabPanel

export const a11yProps = (index: number) => ({
	id: `scrollable-auto-tab-${index}`,
	'aria-controls': `scrollable-auto-tabpanel-${index}`
})