import { getPosColor } from '../helpers'

const PositionNote = ({ pos, marginBottom }: { marginBottom: boolean, pos: string }) => (
	<div className='flexContainer itemsCenter ml-05'>
		<div className={`${marginBottom ? 'mb-1' : ''} posColor ${getPosColor(pos)}`}></div>
		<span className={`${marginBottom ? 'mb-1' : ''}`}>= {pos}</span>
	</div>
)

export default PositionNote