import { Badge, Paper, Tooltip } from '@mui/material'
// import EmojiEventsIcon from '@mui/icons-material/EmojiEvents'
import { useRouter } from 'next/router'
import Image from 'next/image'
import React from 'react'
import { clubCrestUrl, getRecPoints } from '../helpers'
import SmallChip from './chip'
import { getPlatformLocalStorage } from '../localStorage/platform'
import { IClubRankLeaderboard } from 'proclubs-api/dist/model/leaderboard'
import { IHighestClub, IRecClub, isIRecClub } from '../models/interfaces'
import { IClubSearch } from 'proclubs-api/dist/model/club'
import GradeIcon from '@mui/icons-material/Grade'

/**
 * @param club The club object to display
 * @param i The index of the club in array
 * @param setRec click event behavior on club result
 * @param isRec Indicates if the user should be redirected to custom record club overview or default club overview
 * @returns JSX single Club result component
 */
const ClubResult = (
	{ club, i, setRec, isRec, fullWidth, highestRec }: {
		club: IClubRankLeaderboard | IRecClub | IClubSearch,
		i: number,
		setRec?: boolean,
		isRec?: boolean,
		fullWidth?: boolean,
		highestRec?: IHighestClub | null
	}
) => {

	const router = useRouter()

	return (
		<Tooltip
			key={`${club.clubInfo.name}-${i}`}
			title={
				router.asPath.includes('/record/leaderboards') ?
					`${+club.wins > 0 ? getRecPoints(+club.wins, +club.ties) : '0'} Points${isIRecClub(club) ? club.hasLost ? ' - Has lost' : !club.hasLost && ' - Is climbing' : ''}`
					:
					`${club.wins} - ${club.ties} - ${club.losses}`
			}
		>
			<Paper className={`paperBlueGreen ${isRec && fullWidth ? 'width100 m-05' : 'clubSearchPaper'}`} >

				<span
					onClick={() => {
						if (setRec) {
							// if the component is used to indicate a record club array
							return
						}
						const routerPlat = router.query.platform?.toString()
						if (isRec) {
							// if it is a record club result, redirect to record club page
							void router.push(`/${routerPlat || getPlatformLocalStorage() || ''}/record/leaderboards/club/${club.clubId}`)
							return
						}
						void router.push(`/${routerPlat || getPlatformLocalStorage() || ''}/club/${club.clubId}`)
					}}
					className='width100'
				>
					<Badge
						badgeContent=' '
						color={isIRecClub(club) && club.hasLost ? 'error' : 'success'}
						variant='dot'
						invisible={!isIRecClub(club)}
						sx={{ width: '100%' }}
					>
						<div className='clubSearchWrap pointer' >
							<div className='flexContainer itemsCenter'>
								{highestRec && isRec && club.clubId === highestRec.clubId
									?
									<GradeIcon color='primary' fontSize='large' sx={{ marginRight: -.5 }} />
									:
									<div style={{ marginLeft: '.3em' }}><SmallChip label={(i + 1).toString()} /></div>
								}
								<div style={{ padding: '.5em' }}>
									<Image
										src={clubCrestUrl(club.clubInfo.teamId)} // club.clubInfo.customKit.isCustomTeam === '0' ? club.clubInfo.teamId : +club.clubInfo.customKit.crestAssetId
										alt='crest'
										width={40}
										height={40}
									/>
								</div>
								<div className='shortClubInfoRow'>
									{club.clubInfo.name}
								</div>
							</div>
							<div className='flexContainer itemsCenter' style={{ marginRight: '.3em' }}>
								<div className='shortClubInfoRow'>
									{
										isRec ?
											!club.wins || club.wins === '0'
												?
												<span>0 wins</span>
												:
												<span>{club.wins}&nbsp;&#8226;&nbsp;{club.ties}&nbsp;&#8226;&nbsp;{club.losses}</span>
											:
											<>
												{/* its not a record club */}
												<span>{club.rankingPoints ? `${club.rankingPoints} Pts.` : ''}</span>
											</>
									}
								</div>
							</div>
						</div>
					</Badge>
				</span>
			</Paper>
		</Tooltip>
	)
}
export default ClubResult