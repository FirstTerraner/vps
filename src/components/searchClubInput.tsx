import PolicyOutlinedIcon from '@mui/icons-material/PolicyOutlined'
import SearchIcon from '@mui/icons-material/Search'
import { CircularProgress, Divider, IconButton, InputBase, Paper } from '@mui/material'
import { useRouter } from 'next/router'
import { isPlatformType } from 'proclubs-api/dist'
import React from 'react'
import { getClubsByName } from '../proclubsApis'
import { useClubSearchContext } from '../context/clubSearchContext'
import { getPlatformLocalStorage } from '../localStorage/platform'
import PlatformDropdown from './platformDropdown'
import { useTransContext } from '../context/transContext'

const SearchClubInput = () => {

	const router = useRouter()

	const { translated } = useTransContext()

	// search states
	const [msg, setMsg] = React.useState('')

	const [clubName, setClubName] = React.useState('')

	const [isSearching, setIsSearching] = React.useState(false)

	// TODO use serversideprops instead of context
	const { setClubs } = useClubSearchContext()


	return (
		<>
			<Paper className='searchPaper paperBlueGreen'>
				<div className='overviewPanelHead width95'>
					<div className='flexContainer itemsCenter'>
						<div className='mr-05 flexContainer'>
							<SearchIcon />
						</div>
						<p className='panelHeaderTxt'>{translated.homepage.searchClub}</p>
					</div>
				</div>

				<div className='searchInputWrapper'>
					<PlatformDropdown padding />
					<Divider style={{ height: '28 px', margin: '4px' }} orientation='vertical' />
					<InputBase
						style={{ marginLeft: '5px', flex: '1' }}
						type='search'
						name='search'
						autoComplete='on'
						placeholder='Clubname'
						onChange={e => {
							setClubName(e.target.value)
						}}
						onKeyUp={async e => {
							setMsg('')
							if (e.key !== 'Enter') { return }
							if (clubName.length < 5) {
								setMsg(translated.homepage.min5)
								return
							}
							const plat = getPlatformLocalStorage()
							if (!isPlatformType(plat)) {
								setMsg(translated.homepage.badResp)
								return
							}
							setIsSearching(true)
							const clubsResp = await getClubsByName(plat, clubName)
							setClubs(clubsResp)
							if (!clubsResp || !clubsResp.length) {
								setMsg(translated.homepage.noClub)
								setIsSearching(false)
								return
							}
							setIsSearching(false)
							if (clubsResp.length === 1) {
								void router.push({ pathname: `/${plat}/club/${clubsResp[0].clubId}` })
								return
							}
							if (clubsResp.length > 1) {
								void router.push({ pathname: `/${plat}/club/search/${clubName}` })
							}
						}}
					/>
					<IconButton
						style={{ padding: '10px' }}
						aria-label='search'
						onClick={async () => {
							setMsg('')
							if (clubName.length < 5) {
								setMsg(translated.homepage.min5)
								return
							}
							const plat = getPlatformLocalStorage()
							if (!isPlatformType(plat)) {
								setMsg(translated.homepage.badResp)
								return
							}
							setIsSearching(true)
							const clubsResp = await getClubsByName(plat, clubName)
							setClubs(clubsResp)
							if (!clubsResp || !clubsResp.length) {
								setMsg(translated.homepage.noClub)
								setIsSearching(false)
								return
							}
							setIsSearching(false)
							if (clubsResp.length === 1) {
								void router.push({ pathname: `/${plat}/club/${clubsResp[0].clubId}` })
								return
							}
							if (clubsResp.length > 1) {
								void router.push({ pathname: `/${plat}/club/search/${clubName}` })
							}
						}}
					>
						{isSearching ? <CircularProgress size={20} /> : <PolicyOutlinedIcon color='primary' />}
					</IconButton>
				</div>

				{msg.length > 0 &&
					<p className='redTxt'>
						{msg}
					</p>
				}

				{/* {clubs?.length === 0 && !isSearching && <p className='redTxt'>No clubs found.</p>}
				{isBool(validClubName) && !validClubName && !isSearching && <p className='redTxt'>Minimum 5 characters.</p>} */}

			</Paper>

		</>
	)
}
export default SearchClubInput