import { Paper } from '@mui/material'
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined'
import EmojiEventsIcon from '@mui/icons-material/EmojiEvents'
import ExpandLessOutlinedIcon from '@mui/icons-material/ExpandLessOutlined'
import ExpandMoreOutlinedIcon from '@mui/icons-material/ExpandMoreOutlined'
import ListOutlinedIcon from '@mui/icons-material/ListOutlined'
import RemoveCircleOutlineOutlinedIcon from '@mui/icons-material/RemoveCircleOutlineOutlined'
import RemoveOutlinedIcon from '@mui/icons-material/RemoveOutlined'
import SportsSoccerOutlinedIcon from '@mui/icons-material/SportsSoccerOutlined'
import TimelineOutlinedIcon from '@mui/icons-material/TimelineOutlined'
import React from 'react'
import { IClubOverviewState } from '../../models/interfaces'
import Notice from '../notice'
import { useTransContext } from '../../context/transContext'

const ClubOverall = ({ club }: { club: IClubOverviewState }) => {

	const { translated } = useTransContext()

	if (!club?.stats) {
		return <Notice header={`Club "${club?.infos?.name || ''}"`} txt='No matches found.' svgSrc='../../svgs/scissorBall.svg' />
	}

	return (
		<Paper className='overviewPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>Details</p>
			</div>
			<div className='panelBody'>
				<div className='width100'>
					<div className='columnContainer'>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><SportsSoccerOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.total} {translated.singleWords.matches}</span>
							<span>{club.stats.totalGames}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><ListOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.total} {translated.singleWords.seasons}</span>
							<span>{club.stats.seasons}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><EmojiEventsIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.seasons} {translated.singleWords.titles}</span>
							<span>{club.stats.titlesWon}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><ExpandLessOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.promotion}</span>
							<span>{club.stats.promotions}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><RemoveOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.hold}</span>
							<span>{club.stats.holds}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><ExpandMoreOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.relegation}</span>
							<span>{club.stats.relegations}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><AddCircleOutlineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.total} {translated.singleWords.goals}</span>
							<span>{club.stats.alltimeGoals}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><RemoveCircleOutlineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.total} {translated.singleWords.goalsAgainst}</span>
							<span>{club.stats.alltimeGoalsAgainst}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><TimelineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.goals} {translated.singleWords.perMatch}</span>
							<span>{(+club.stats.alltimeGoals / club.stats.totalGames).toFixed(1)}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><TimelineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />{translated.singleWords.goalsAgainst} {translated.singleWords.perMatch}</span>
							<span>{(+club.stats.alltimeGoalsAgainst / club.stats.totalGames).toFixed(1)}</span>
						</div>
					</div>
				</div>
			</div>
		</Paper>
	)
}

export default ClubOverall