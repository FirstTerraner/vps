import { IClubMemberCareer } from 'proclubs-api/dist/model/club'
import { Paper } from '@mui/material'
import PeopleAltIcon from '@mui/icons-material/PeopleAlt'
import router from 'next/router'
import { getPosColor } from '../../helpers'
import SmallChip from '../chip'
import PositionNote from '../positionNote'
import { useTransContext } from '../../context/transContext'

const SquadList = ({ memberArr, title }: { memberArr: IClubMemberCareer[] | null, title: string }) => {

	const { translated } = useTransContext()

	if (!memberArr) { return null }

	return (
		<Paper className='playerClubPaper paperBlueGreen'>
			{memberArr.length > 0 &&
				<>
					<div className='overviewPanelHead'>
						<p className='panelHeaderTxt'>{title}</p>
						<SmallChip icon={<PeopleAltIcon />} label={`${memberArr.length}`} />
					</div>
					<PositionNote pos={`${translated.singleWords.any}`} marginBottom />
					<div className='squadList'>
						{memberArr.map(member => (
							<Paper
								key={`playerList-${member.name}`}
								style={{ margin: '.2em 0', padding: '.5em .5em .3em 0' }}
								className='pointer'
								onClick={() => {
									const currentPlat = router.query.platform?.toString()
									const currentClubId = router.query.id?.toString()
									if (!currentPlat || !currentClubId) { return }
									void router.push(`/${currentPlat}/club/${currentClubId}/squad/${member.name}`)
								}}
							>
								<div className='flexContainer itemsCenter'>
									<div className={`posColor mr-1 ${getPosColor(member.favoritePosition)}`}></div>
									<span>{member.name}</span>
								</div>
							</Paper>
						))}
					</div>
				</>
			}
		</Paper>
	)
}

export default SquadList