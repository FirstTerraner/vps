import { Paper, Tooltip } from '@mui/material'
import React from 'react'
import { clubCrestUrl, endResultColor } from '../../helpers'
import { IClubOverviewState, IMembersOpenState, TMatchesState } from '../../models/interfaces'
import { isStr } from '../../models/typeGuards'
import SmallChip from '../chip'
import MatchOverview from './matchOverview'
import Image from 'next/image'
import appLogo from '../../../public/imgs/appLogo.png'
import { useTransContext } from '../../context/transContext'

const RecentResults = ({ matches, pageTitle, club }: { matches?: TMatchesState, pageTitle: string, club: IClubOverviewState }) => {

	const { translated } = useTransContext()

	const [isMatchOpen, setIsMatchOpen] = React.useState<IMembersOpenState | undefined>()

	const [selectedMatch, setSelectedMatch] = React.useState<string | undefined>()
	// console.log('matches: ', matches)
	React.useEffect(() => {
		if (!matches || matches.length === 0) { return }
		const matchOpenState: IMembersOpenState = {}
		let key: string
		// tslint:disable-next-line: prefer-for-of
		for (let i = 0; i < matches.length; i++) {
			key = matches[i].matchId
			matchOpenState[key] = false
		}
		setIsMatchOpen(matchOpenState)
	}, [matches])

	// console.log('matches: ', matches)

	return (
		<>
			<Paper className='overviewPaper paperBlueGreen relative ovHidden' style={{ paddingBottom: '.5em' }}>
				<img className='matchesBall' src='../../../svgs/scissorBall.svg' alt='' />
				<div className='overviewPanelHead'>
					<div className='flexContainer justifyBetween itemsCenter width100'>
						<p className='panelHeaderTxt'>{pageTitle}</p>
						{pageTitle.includes('Division') && club?.stats &&
							<div className='smallChip'><SmallChip label={`DIV ${club.stats.currentDivision}`} /></div>
						}
					</div>
				</div>
				<div className='panelBody'>
					<div className='playersWrap'>
						{matches?.map((match, i) => {

							if (!club || !club.infos) { return null }

							if (!match.clubs[club.infos.clubId]) { return null }

							const teamA = match.clubs[club.infos.clubId]

							const opponent = match.clubs[+Object.keys(match.clubs)[0] === club.infos.clubId ?
								+Object.keys(match.clubs)[1] : +Object.keys(match.clubs)[0]]

							return (
								<Tooltip key={i} title={`${match.timeAgo.number} ${match.timeAgo.unit} ago`} >

									<div
										className='lastMatches pointer'
										id={match.matchId}
										onClick={() => {
											if (match && teamA.details && opponent.details) {
												setIsMatchOpen(prev => ({ [match.matchId]: !prev?.[match.matchId] }))
												setSelectedMatch(match.matchId)
											}
										}}
									>
										{match && teamA.details && opponent.details ?
											<>
												{teamA.details.clubId === club.infos.clubId
													?
													<span
														className={`lastResults ${endResultColor(+teamA.goals, +teamA.goalsAgainst) || ''}`}
													>
														{isMatchOpen?.[match.matchId] ? translated.singleWords.close : `${teamA.goals} - ${teamA.goalsAgainst}`}
													</span>
													:
													<span
														className={`lastResults ${endResultColor(+opponent.goalsAgainst, +opponent.goals) || ''}`}
													>
														{isMatchOpen?.[match.matchId] ? translated.singleWords.close : `${opponent.goals} - ${opponent.goalsAgainst}`}
													</span>
												}
											</>
											: <span className='lastResults'>N/A</span>
										}
										{opponent.details ?
											<div style={{ marginTop: '.5em' }}>
												<Image
													src={clubCrestUrl(opponent.details.teamId)}
													width={30}
													height={30}
													alt='img'
												/>
											</div>
											:
											<div style={{ marginTop: '.2em' }}>
												<Image
													src={appLogo.src}
													width={20}
													height={30}
													alt='img'
												/>
											</div>
										}
									</div>
									{/* match.clubs[Object.keys(match.clubs)[match.clubs[Object.keys(match.clubs)[0]].details.clubId === club.infos.clubId ? 1 : 0]].details */}
								</Tooltip>
							)
						})}
					</div>
				</div>

				<p className='mb-1 ml-1'>{translated.clubPage.clickMatch}</p>

			</Paper>

			{/* single match overview */}
			{
				isStr(selectedMatch) && isMatchOpen?.[selectedMatch] && matches && matches.length > 0 &&
				<MatchOverview club={club} matches={matches} selectedMatch={selectedMatch} />
			}
		</>
	)
}

export default RecentResults