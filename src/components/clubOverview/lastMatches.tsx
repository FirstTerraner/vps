import { Paper } from '@mui/material'
import NavigateNextIcon from '@mui/icons-material/NavigateNext'
import { scoreColorWTL, wdlChar } from '../../helpers'
import { IClubOverviewState } from '../../models/interfaces'
import Notice from '../notice'

// this component does not show match results.
// it only shows a char like W, L, D for win, losses, ties
const LastMatches = ({ club, setTabIdxCB, headerTxt }: {
	club: IClubOverviewState, setTabIdxCB?: (_event: React.SyntheticEvent, newIdx: unknown) => void, headerTxt: string
}) => {

	if (!club?.stats) {
		return <Notice header={`Club "${club?.infos?.name || ''}"`} txt='No matches yet.' svgSrc='../../svgs/scissorBall.svg' />
	}

	return (
		<Paper className='overviewPaper paperBlueGreen'>
			<div className='overviewPanelHead pointer' onClick={e => setTabIdxCB?.(e, 2)}>
				<p className='panelHeaderTxt'>{headerTxt}</p>
				{setTabIdxCB && <NavigateNextIcon />}
			</div>
			<div className='panelBody'>
				<div className='flexContainer justifyBetween itemsCenter width100'>
					{club.stats.lastMatch6 &&
						<div className={`recentResults ${scoreColorWTL(club.stats.lastMatch6)}`} >
							{wdlChar(club.stats.lastMatch6)}
						</div>
					}
					{club.stats.lastMatch5 &&
						<div className={`recentResults ${scoreColorWTL(club.stats.lastMatch5)}`} >
							{wdlChar(club.stats.lastMatch5)}
						</div>
					}
					{club.stats.lastMatch4 &&
						<div className={`recentResults ${scoreColorWTL(club.stats.lastMatch4)}`} >
							{wdlChar(club.stats.lastMatch4)}
						</div>
					}
					{club.stats.lastMatch3 &&
						<div className={`recentResults ${scoreColorWTL(club.stats.lastMatch3)}`} >
							{wdlChar(club.stats.lastMatch3)}
						</div>
					}
					{club.stats.lastMatch2 &&
						<div className={`recentResults ${scoreColorWTL(club.stats.lastMatch2)}`} >
							{wdlChar(club.stats.lastMatch2)}
						</div>
					}
					{club.stats.lastMatch1 &&
						<div className={`recentResults ${scoreColorWTL(club.stats.lastMatch1)}`} >
							{wdlChar(club.stats.lastMatch1)}
						</div>
					}
					{club.stats.lastMatch0 &&
						<div className={`recentResults ${scoreColorWTL(club.stats.lastMatch0)}`} >
							{wdlChar(club.stats.lastMatch0)}
						</div>
					}
				</div>
			</div>
		</Paper>
	)
}

export default LastMatches