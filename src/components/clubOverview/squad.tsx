import { IClubMemberStats } from 'proclubs-api/dist/model/club'
import { Button, Paper } from '@mui/material'
import PeopleAltIcon from '@mui/icons-material/PeopleAlt'
import { useRouter } from 'next/router'
import React from 'react'
import { getAttrColor, getMemberPosCount, getPos, getPosColor, nationUrl, ratingColor } from '../../helpers'
import { IClubOverviewState, IMemberPosCount, IMembersOpenState, ISquadOverviewState } from '../../models/interfaces'
import { isStr } from '../../models/typeGuards'
import PlayerClubStats from './playerClubStats'
import appLogo from '../../../public/imgs/appLogo.png'
import Image from 'next/image'
import { useTransContext } from '../../context/transContext'
import Notice from '../notice'

const SquadOverview = ({ squad, club }: { squad?: ISquadOverviewState, club: IClubOverviewState }) => {

	const router = useRouter()

	const { translated } = useTransContext()

	const [isMemberOpen, setIsMemberOpen] = React.useState<IMembersOpenState | undefined>()

	const [selectedMemberName, setSelectedMemberName] = React.useState<string | undefined>()

	const [posCount, setPosCount] = React.useState<IMemberPosCount | undefined>()

	React.useEffect(() => {
		if (!squad || !squad.memberCareer) { return }
		const memberOpenState: IMembersOpenState = {}
		let key: string
		// tslint:disable-next-line: prefer-for-of
		for (let i = 0; i < squad.memberCareer.length; i++) {
			key = squad.memberCareer[i].name //  `member${i}`
			memberOpenState[key] = false
		}
		setIsMemberOpen(memberOpenState)
		setPosCount(getMemberPosCount(squad.memberCareer))
	}, [squad])

	if (!squad) { return null }

	return (
		<>
			{squad.memberStats && squad.memberStats.length > 0 ?
				<Paper className='squadPaper paperBlueGreen'>
					<div className='overviewPanelHead'>
						<div className='flexContainer itemsCenter'>
							<p className='panelHeaderTxt'>Squad</p>
						</div>
						{squad.memberStats &&
							<div className='flexContainer itemsCenter'>
								<div className='posColorBig bgGrey posTxt' style={{ padding: '0 1em', borderRadius: '15px' }}>
									<PeopleAltIcon fontSize='small' style={{ margin: '0 .5em 0 0' }} />
									{squad.memberStats.length}
								</div>
								<div>
									{posCount &&
										<div className='flexContainer itemsCenter'>
											<div className='posColorBig bgOrange posTxt'>{posCount.gk}</div>
											<div className='posColorBig bgYellow posTxt'>{posCount.def}</div>
											<div className='posColorBig bgGreen posTxt'>{posCount.mid}</div>
											<div className='posColorBig bgBlue posTxt'>{posCount.front}</div>
										</div>
									}
								</div>
							</div>
						}
					</div>
					<div className='panelBody'>
						<div className='playersWrap'>
							{squad.memberStats && squad.memberStats.length > 0 &&
								squad.memberStats.map((member, i) => (
									<Paper className='playersCareerWrap' key={`member-${i}`} style={{ border: '1px solid rgb(200, 200, 200)' }}>
										<div className='playerPanelHead'>
											<div className='columnContainer'>
												<span className='panelHeaderSmallTxt'>{member.name}</span>
												<span className='panelHeaderTxt'>{squad.memberCareer?.[i].proName || '--'}</span>
											</div>
											<div className='flexContainer itemsCenter'>
												<p className='flexContainer itemsCenter'>{getPos(translated.playerPos, member.proPos)}</p>
												<div className={`posColor ${getPosColor(member.favoritePosition)}`} style={{ marginRight: 0 }}></div>
											</div>
										</div>
										<div className='mt-05 mr-1 ml-1'>
											<div className='flexContainer justifyBetween width100'>
												<div className='flexContainer itemsCenter justifyBetween  width100'>
													{squad.memberCareer?.[i].proNationality ?
														<div className='flexContainer itemsCenter'>
															<div style={{ marginRight: '.5em' }}>
																<Image
																	src={nationUrl(squad.memberCareer?.[i].proNationality)}
																	alt='--'
																	width={30}
																	height={20}
																/>
															</div>
															<span className='panelHeaderSmallTxt mb-05'>{squad.memberCareer?.[i].proHeight + ' cm'}</span>
														</div>
														:
														<Image
															src={appLogo.src}
															alt='N/A'
															width={20}
															height={30}
														/>
													}
													<div className='flexContainer itemsCenter'>
														{squad.memberCareer && +squad.memberCareer[i].proOverall > 0 &&
															<div className={`attrBox mr-05 ${getAttrColor(+squad.memberCareer[i].proOverall)}`} >{squad.memberCareer[i].proOverall}</div>
														}
														<span className={`${ratingColor(+member.ratingAve)} attrBox`}>{member.ratingAve}</span>
													</div>
												</div>
											</div>
										</div>
										<hr />
										<div className='columnContainer mr-1 ml-1'>
											<div className='flexContainer itemsCenter justifyBetween mb-05'>
												<span>{translated.singleWords.total} {translated.singleWords.matches}:</span>
												<span>{member.gamesPlayed}</span>
											</div>
											<div className='mb-05'>
												<span>({translated.singleWords.allClubs})</span>
											</div>
											<div className='mb-05 width100'>
												<Button
													disabled={!squad.memberCareer?.length || squad.memberCareer?.[i].gamesPlayed === '0'}
													variant='outlined'
													color='primary'
													style={{ minWidth: '100%', maxWidth: '100%', margin: '0 0 .5em 0' }}
													id={member.name}
													onClick={() => {
														setIsMemberOpen(prev => ({ [member.name]: !prev?.[member.name] }))
														setSelectedMemberName(member.name)
													}}
												>
													{
														!squad.memberCareer?.length || squad.memberCareer?.[i].gamesPlayed === '0' ?
															'no club matches'
															:
															!isMemberOpen?.[member.name] ? translated.clubPage.openCareer : translated.singleWords.close
													}
												</Button>
												<Button
													variant='contained'
													className={!squad.memberStats?.length || member.gamesPlayed === '0' ? 'disabled width100' : 'bgBlueBtn'}
													onClick={() => {
														const currentPlat = router.query.platform?.toString()
														const currentClubId = router.query.id?.toString()
														if (!currentPlat || !currentClubId) { return }
														void router.push(`/${currentPlat}/club/${currentClubId}/squad/${member.name}`)
													}}
												>
													{translated.singleWords.moreInfo}
												</Button>
											</div>
										</div>
									</Paper>
								))
							}
						</div>
					</div>
				</Paper>
				:
				<Notice
					header={translated.singleWords.squad}
					txt={translated.clubPage.notAvailable}
					svgSrc='../../../svgs/scissorBall.svg'
				/>
			}

			{/* member club career */}
			{isStr(selectedMemberName) &&
				isMemberOpen?.[selectedMemberName] &&
				squad.memberCareer?.map(member => {
					if (member.name === selectedMemberName) {
						// TODO fix member types in api lib
						return <PlayerClubStats
							key={`${selectedMemberName}-${member.name}`}
							member={member as unknown as IClubMemberStats || null}
							clubName={club?.infos?.name || ''}
							selectedMember={selectedMemberName}
						/>
					}
					return null
				})
			}

		</>
	)
}

export default SquadOverview