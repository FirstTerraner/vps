import { IClubMemberStats } from 'proclubs-api/dist/model/club'
import { Paper } from '@mui/material'
import React from 'react'
import LinearDeterminate from '../progress'
import { useTransContext } from '../../context/transContext'

const PlayerClubStats = (
	{ member, clubName, selectedMember }: { clubName: string, member: IClubMemberStats | null, selectedMember: string }
) => {

	const { translated } = useTransContext()

	if (!member) { return null }

	return (
		<Paper className='playerClubPaper gonimate fadeIn paperBlueGreen' >
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>{translated.clubPage.currentClub}</p>
				<p>{selectedMember}</p>
			</div>
			<div className='panelBody'>
				<div className='columnContainer width100'>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.matches} {translated.singleWords.for} {`"${clubName}"`}</span>
						<span>{member.gamesPlayed}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.goals}</span>
						<span>{member.goals}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.goals} {translated.singleWords.perMatch}</span>
						<span>{(+member.goals / +member.gamesPlayed).toFixed(1)}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.shotAccuracy}</span>
						<span>{member.shotSuccessRate}%</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<LinearDeterminate progress={+member.shotSuccessRate} width={100} />
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.assists}</span>
						<span>{member.assists}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.assists} {translated.singleWords.perMatch}</span>
						<span>{(+member.assists / +member.gamesPlayed).toFixed(1)}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.clubPage.sotm}</span>
						<span>{member.manOfTheMatch}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.passes}</span>
						<span>{member.passesMade}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.passes} {translated.singleWords.perMatch}</span>
						<span>{(+member.passesMade / +member.gamesPlayed).toFixed(1)}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.passAccuracy}</span>
						<span>{member.passSuccessRate}%</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<LinearDeterminate progress={+member.passSuccessRate} width={100} />
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.tacklesMade}</span>
						<span>{member.tacklesMade}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.tackles} {translated.singleWords.perMatch}</span>
						<span>{(+member.tacklesMade / +member.gamesPlayed).toFixed(1)}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.tackleAccuracy}</span>
						<span>{member.tackleSuccessRate}%</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<LinearDeterminate progress={+member.tackleSuccessRate} width={100} />
					</div>
					{(member.proPos === '0' || member.favoritePosition === 'defender') &&
						<div className='flexContainer itemsCenter justifyBetween'>
							<span>{translated.singleWords.cleansheets}</span>
							<span>{member.proPos === '0' ? `${member.cleanSheetsGK}` : `${member.cleanSheetsDef}`}</span>
						</div>
					}
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.redCards}</span>
						<span>{member.redCards}</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{translated.singleWords.winRate}</span>
						<span>{member.winRate}%</span>
					</div>
					<div className='flexContainer itemsCenter justifyBetween'>
						<LinearDeterminate progress={+member.winRate} width={100} />
					</div>
				</div>
			</div>
		</Paper>
	)
}

export default PlayerClubStats