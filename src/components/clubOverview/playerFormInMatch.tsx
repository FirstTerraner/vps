import { IClubMatches } from 'proclubs-api/dist/model/club'
import { Button, Paper } from '@mui/material'
import SportsSoccerOutlinedIcon from '@mui/icons-material/SportsSoccerOutlined'
import StarBorderOutlinedIcon from '@mui/icons-material/StarBorderOutlined'
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined'
import router from 'next/router'
import React from 'react'
import { ratingColor } from '../../helpers'
import LinearDeterminate from '../progress'
import { useTransContext } from '../../context/transContext'

const PlayerFormInMatch = ({ clubId, match, playerArr }: { clubId: string, match: IClubMatches, playerArr: string[] }) => {

	const { translated } = useTransContext()

	const [isMemberOpen, setIsMemberOpen] = React.useState({ currentId: -1 })

	return (
		<>
			{playerArr.map((player, i) => (
				<Paper
					key={`home${i}`}
					className='overviewPaper mb-05 pointer'
					style={{ padding: '.2em .5em', border: '1px solid #e4e4e4', minHeight: '45px' }}
					id={player}
					onClick={e => {
						const id = +e.currentTarget.id
						setIsMemberOpen(prev => prev.currentId === id ? { currentId: -1 } : { currentId: id })
					}}
				>
					<div className='flexContainer itemsCenter justifyBetween'>
						<span>{match.players[clubId][player].playername}</span>
						<span className={`${ratingColor(+match.players[clubId][player].rating)} attrBox`}>
							{match.players[clubId][player].rating.slice(0, -1)}
						</span>
					</div>
					<div className='flexContainer itemsCenter mt-05' style={{ justifyContent: 'right' }}>
						{match.players[clubId][player].mom === '1' && <StarBorderOutlinedIcon fontSize='small' style={{ color: 'orange' }} />}
						{[+match.players[clubId][player].assists].map(entry => {
							const Assists: React.ReactNode[] = []
							for (let j = 0; j < entry; j++) {
								Assists.push(<VisibilityOutlinedIcon fontSize='small' style={{ color: '#999' }} />)
							}
							return Assists.map((assist, k) => <div key={`assist${k}`}>{assist}</div>)
						})}
						{[+match.players[clubId][player].goals].map(entry => {
							const Goals: React.ReactNode[] = []
							for (let j = 0; j < entry; j++) {
								Goals.push(<SportsSoccerOutlinedIcon fontSize='small' style={{ color: '#999' }} />)
							}
							return Goals.map((goal, k) => <div key={`goal${k}`}>{goal}</div>)
						})}
					</div>
					{isMemberOpen.currentId === +player &&
						<div key={isMemberOpen.currentId} className='columnContainer gonimate fadeIn'>
							<div className='mt-05'>{translated.singleWords.passes} <span className='bold'>{match.players[clubId][player].passattempts}</span></div>
							<div>{translated.singleWords.passesMade} <span className='bold'>{match.players[clubId][player].passesmade}</span></div>
							<div>{translated.singleWords.assists} <span className='bold'>{match.players[clubId][player].assists}</span></div>
							<div>
								{translated.singleWords.passAccuracy}
								<span className='bold'>
									{match.players[clubId][player].passesmade === '0' && match.players[clubId][player].passattempts === '0'
										?
										` ${0}%`
										:
										` ${(+match.players[clubId][player].passesmade / +match.players[clubId][player].passattempts * 100).toFixed(1)}%`
									}
								</span>
							</div>
							<LinearDeterminate
								progress={
									match.players[clubId][player].passesmade === '0' && match.players[clubId][player].passattempts === '0'
										?
										0
										:
										+match.players[clubId][player].passesmade / +match.players[clubId][player].passattempts * 100
								}
								width={100}
							/>
							<div className='mt-1'>{translated.singleWords.shots} <span className='bold'>{match.players[clubId][player].shots}</span></div>
							<div>{translated.singleWords.goals} <span className='bold'>{match.players[clubId][player].goals}</span></div>
							<div>
								{translated.singleWords.shotAccuracy}
								<span className='bold'>
									{match.players[clubId][player].goals === '0' && match.players[clubId][player].shots === '0'
										?
										` ${0}%`
										:
										` ${(+match.players[clubId][player].goals / +match.players[clubId][player].shots * 100).toFixed(1)}%`
									}
								</span>
							</div>
							<LinearDeterminate
								progress={
									match.players[clubId][player].goals === '0' && match.players[clubId][player].shots === '0'
										?
										0
										:
										+match.players[clubId][player].goals / +match.players[clubId][player].shots * 100
								}
								width={100}
							/>
							<div className='mt-1'>{translated.singleWords.tackles} <span className='bold'>{match.players[clubId][player].tackleattempts}</span></div>
							<div>{translated.singleWords.tacklesMade} <span className='bold'>{match.players[clubId][player].tacklesmade}</span></div>
							<div>
								{translated.singleWords.tackleAccuracy}
								<span className='bold'>
									{match.players[clubId][player].tackleattempts === '0' && match.players[clubId][player].tacklesmade === '0'
										?
										` ${0}%`
										:
										` ${(+match.players[clubId][player].tacklesmade / +match.players[clubId][player].tackleattempts * 100).toFixed(1)}%`
									}
								</span>
							</div>
							<LinearDeterminate
								progress={
									match.players[clubId][player].tackleattempts === '0' && match.players[clubId][player].tacklesmade === '0'
										?
										0
										:
										+match.players[clubId][player].tacklesmade / +match.players[clubId][player].tackleattempts * 100
								}
								width={100}
							/>
							<div className='mt-1'>{translated.singleWords.redCards} <span className='bold'>{match.players[clubId][player].redcards === '0' ? translated.singleWords.no : translated.singleWords.yes}</span></div>
							<Button
								variant='outlined'
								color='primary'
								style={{ margin: '.5em 0 0 0' }}
								onClick={e => {
									e.stopPropagation()
									const currentPlat = router.query.platform?.toString()
									const currentClubId = router.query.id?.toString()
									if (!currentPlat || !currentClubId) { return }
									void router.push(`/${currentPlat}/club/${currentClubId}/squad/${match.players[clubId][player].playername}`)
								}}
							>
								{translated.singleWords.moreInfo}
							</Button>
						</div>
					}
				</Paper>
			))}
		</>
	)
}

export default PlayerFormInMatch