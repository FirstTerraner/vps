import { Paper } from '@mui/material'
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined'
import ChevronRightOutlinedIcon from '@mui/icons-material/ChevronRightOutlined'
import ExpandLessOutlinedIcon from '@mui/icons-material/ExpandLessOutlined'
import ListOutlinedIcon from '@mui/icons-material/ListOutlined'
import RemoveCircleOutlineOutlinedIcon from '@mui/icons-material/RemoveCircleOutlineOutlined'
import SportsSoccerOutlinedIcon from '@mui/icons-material/SportsSoccerOutlined'
import TimelineOutlinedIcon from '@mui/icons-material/TimelineOutlined'
import { IClubOverviewState } from '../../models/interfaces'

const DivisionForm = ({ club }: { club: IClubOverviewState }) => {

	if (!club || !club.stats) { return null }

	return (
		<Paper className='overviewPaper paperBlueGreen'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>Division {club.stats.currentDivision} Form</p>
			</div>
			<div className='panelBody'>
				<div className='width100'>
					<div className='columnContainer'>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><SportsSoccerOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />Matches</span>
							<span>{club.stats.gamesPlayed}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><ExpandLessOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />Points</span>
							<span>{club.stats.points}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><ChevronRightOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />Projected Points</span>
							<span>{club.stats.projectedPoints === -1 ? '0' : club.stats.projectedPoints}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><ListOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />History</span>
							<span>{club.stats.seasonWins}&nbsp;&#8226;&nbsp;{club.stats.seasonTies}&nbsp;&#8226;&nbsp;{club.stats.seasonLosses}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><AddCircleOutlineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />Goals</span>
							<span>{club.stats.goals}</span>
						</div>
						<div className='flexContainer justifyBetween'>
							<span className='teamHistory'><RemoveCircleOutlineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />Goals Against</span>
							<span>{club.stats.goalsAgainst}</span>
						</div>
						{+club.stats.gamesPlayed > 0 &&
							<div className='flexContainer justifyBetween'>
								<span className='teamHistory'><TimelineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />Avg. Goals</span>
								<span>{(+club.stats.goals / +club.stats.gamesPlayed).toFixed(2)}</span>
							</div>
						}
						{+club.stats.gamesPlayed > 0 &&
							<div className='flexContainer justifyBetween'>
								<span className='teamHistory'><TimelineOutlinedIcon fontSize='small' style={{ margin: '0 .5em 0 0', color: '#999' }} />Avg. Goals Against</span>
								<span>{(+club.stats.goalsAgainst / +club.stats.gamesPlayed).toFixed(2)}</span>
							</div>
						}
					</div>
				</div>
			</div>
		</Paper>
	)
}

export default DivisionForm