import { IClubMatches } from 'proclubs-api/dist/model/club'
import { useTransContext } from '../../context/transContext'
import { validVal } from '../../helpers'
import LinearDeterminate from '../progress'

const MatchForm = ({ match, clubId, oppId }: { clubId: string, match: IClubMatches, oppId: string }) => {

	const { translated } = useTransContext()

	return (
		<>
			<div className='flexContainer itemsCenter justifyCenter'>
				<span className='bold'>{match.aggregate[clubId]['shots']}</span>
				<span className='mr-1 ml-1'>{translated.singleWords.shots}</span>
				<span className='bold'>{match.aggregate[oppId]['shots']}</span>
			</div>
			<div className='flexContainer itemsCenter justifyCenter'>
				<span className='bold'>{validVal((match.aggregate[clubId]['goals'] / match.aggregate[clubId]['shots'] * 100))}%</span>
				<span className='mr-1 ml-1'>{translated.singleWords.shotAccuracy}</span>
				<span className='bold'>{validVal((match.aggregate[oppId]['goals'] / match.aggregate[oppId]['shots'] * 100))}%</span>
			</div>
			<div className='flexContainer itemsCenter justifyCenter mb-1 width100'>
				<LinearDeterminate progress={match.aggregate[clubId]['goals'] / match.aggregate[clubId]['shots'] * 100 || 0} width={47} />
				<div className='mr-05 ml-05'></div>
				<LinearDeterminate progress={match.aggregate[oppId]['goals'] / match.aggregate[oppId]['shots'] * 100 || 0} width={47} />
			</div>
			<div className='flexContainer itemsCenter justifyCenter'>
				<span className='bold'>{match.aggregate[clubId]['passattempts']}</span>
				<span className='mr-1 ml-1'>{translated.singleWords.passes}</span>
				<span className='bold'>{match.aggregate[oppId]['passattempts']}</span>
			</div>
			<div className='flexContainer itemsCenter justifyCenter'>
				<span className='bold'>{match.aggregate[clubId]['passesmade']}</span>
				<span className='mr-1 ml-1'>{translated.singleWords.passesMade}</span>
				<span className='bold'>{match.aggregate[oppId]['passesmade']}</span>
			</div>
			<div className='flexContainer itemsCenter justifyCenter'>
				<span className='bold'>{validVal((match.aggregate[clubId]['passesmade'] / match.aggregate[clubId]['passattempts'] * 100))}%</span>
				<span className='mr-1 ml-1'>{translated.singleWords.passAccuracy}</span>
				<span className='bold'>{validVal((match.aggregate[oppId]['passesmade'] / match.aggregate[oppId]['passattempts'] * 100))}%</span>
			</div>
			<div className='flexContainer itemsCenter justifyCenter mb-1 width100'>
				<LinearDeterminate progress={match.aggregate[clubId]['passesmade'] / match.aggregate[clubId]['passattempts'] * 100 || 0} width={47} />
				<div className='mr-05 ml-05'></div>
				<LinearDeterminate progress={match.aggregate[oppId]['passesmade'] / match.aggregate[oppId]['passattempts'] * 100 || 0} width={47} />
			</div>
			<div className='flexContainer itemsCenter justifyCenter'>
				<span className='bold'>{match.aggregate[clubId]['tackleattempts']}</span>
				<span className='mr-1 ml-1'>{translated.singleWords.tackles}</span>
				<span className='bold'>{match.aggregate[oppId]['tackleattempts']}</span>
			</div>
			<div className='flexContainer itemsCenter justifyCenter'>
				<span className='bold'>{match.aggregate[clubId]['tacklesmade']}</span>
				<span className='mr-1 ml-1'>{translated.singleWords.tacklesMade}</span>
				<span className='bold'>{match.aggregate[oppId]['tacklesmade']}</span>
			</div>
			<div className='flexContainer itemsCenter justifyCenter'>
				<span className='bold'>{validVal((match.aggregate[clubId]['tacklesmade'] / match.aggregate[clubId]['tackleattempts'] * 100))}%</span>
				<span className='mr-1 ml-1'>{translated.singleWords.tackleAccuracy}</span>
				<span className='bold'>{validVal((match.aggregate[oppId]['tacklesmade'] / match.aggregate[oppId]['tackleattempts'] * 100))}%</span>
			</div>
			<div className='flexContainer itemsCenter justifyCenter mb-1 width100'>
				<LinearDeterminate progress={match.aggregate[clubId]['tacklesmade'] / match.aggregate[clubId]['tackleattempts'] * 100 || 0} width={47} />
				<div className='mr-05 ml-05'></div>
				<LinearDeterminate progress={match.aggregate[oppId]['tacklesmade'] / match.aggregate[oppId]['tackleattempts'] * 100 || 0} width={47} />
			</div>
			<div className='flexContainer itemsCenter justifyCenter'>
				<span className='bold'>{match.aggregate[clubId]['redcards']}</span>
				<span className='mr-1 ml-1'>{translated.singleWords.redCards}</span>
				<span className='bold'>{match.aggregate[oppId]['redcards']}</span>
			</div>
		</>
	)
}

export default MatchForm