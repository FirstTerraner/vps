import { IClubMatches } from 'proclubs-api/dist/model/club'
import { AppBar, Paper, Tab, Tabs } from '@mui/material'
import PeopleIcon from '@mui/icons-material/People'
import { useRouter } from 'next/router'
import React from 'react'
import { useClubSearchContext } from '../../context/clubSearchContext'
import { clubCrestUrl } from '../../helpers'
import { IClubOverviewState } from '../../models/interfaces'
import { isNum } from '../../models/typeGuards'
import TabPanel, { a11yProps } from '../tabPanel'
import MatchForm from './matchForm'
import PlayerFormInMatch from './playerFormInMatch'
import Image from 'next/image'
import { useTransContext } from '../../context/transContext'

const MatchOverview = ({ matches, selectedMatch, club }: { matches: IClubMatches[], selectedMatch: string, club: IClubOverviewState }) => {

	const router = useRouter()

	const { translated } = useTransContext()

	const { platform } = useClubSearchContext()

	const [tabIdx, setTabIdx] = React.useState(0)

	const handleTabChange = (_event: React.SyntheticEvent, newValue: unknown) => {
		setTabIdx(isNum(newValue) ? newValue : 0)
	}
	// console.log('matches in matchOverview: ', matches)
	return (
		<>
			{matches.map((match, i) => {
				if (!club || !club.infos) { return null }
				if (match.matchId !== selectedMatch || !match.clubs[club.infos.clubId.toString()]) { return null }
				const teamA = match.clubs[club.infos.clubId.toString()]
				const oppId = +Object.keys(match.clubs)[0] === club.infos.clubId ? +Object.keys(match.clubs)[1] : +Object.keys(match.clubs)[0]
				const lastOpponent = match.clubs[oppId]
				return (
					<Paper
						key={`${selectedMatch}-${i}`}
						className='overviewPaper gonimate fadeIn paperBlueGreen'
					>
						{/* match overview header */}
						<div className='flexContainer itemsCenter justifyCenter relative pt-2 pb-2'>
							<div className='flexContainer itemsCenter partA'>
								<p className='matchOverviewHead'>{teamA.details.name}</p>
								<div style={{ marginLeft: '.5em' }}>
									<Image
										src={clubCrestUrl(teamA.details.teamId)} // teamA.details.customKit.isCustomTeam === '0' ? teamA.details.teamId : +teamA.details.customKit.crestAssetId
										alt='crest'
										width={30}
										height={30}
									/>
								</div>
							</div>
							<div className='results'>
								<span>{teamA.goals}</span>
								<span>&nbsp;:&nbsp;</span>
								<span>{lastOpponent.goals}</span>
							</div>
							<div className='flexContainer itemsCenter partB'>
								<div style={{ marginRight: '.5em' }}>
									<Image
										src={clubCrestUrl(lastOpponent.details.teamId)} // lastOpponent.details.customKit.isCustomTeam === '0' ? lastOpponent.details.teamId : +lastOpponent.details.customKit.crestAssetId
										alt='crest'
										width={30}
										height={30}
									/>
								</div>
								<p
									className='matchOverviewHead pointer'
									style={{ color: '#004883', textDecoration: 'underline' }}
									onClick={() => {
										void router.push(`/${router.query.platform?.toString() || platform || 'ps4'}/club/${lastOpponent.details.clubId}`)
									}}
								>
									{lastOpponent.details.name}
								</p>
							</div>
						</div>
						<div className='panelBody justifyCenter'>
							<div className='columnContainer width100'>
								<div className='flexContainer itemsCenter justifyCenter mb-1'>
									<span className='mr-05'>{new Date(match.timestamp * 1000).toDateString().substr(new Date(match.timestamp * 1000).toDateString().indexOf(' ') + 1)}</span>
									<span className='panelHeaderSmallTxt'> - {`${match.timeAgo.number} ${match.timeAgo.unit} ago`}</span>
								</div>
								<AppBar position='static' color='transparent' elevation={0}>
									<Tabs
										value={tabIdx}
										onChange={handleTabChange}
										indicatorColor='primary'
										textColor='inherit'
										variant='fullWidth'
										scrollButtons='auto'
									>
										<Tab label={<span style={{ color: tabIdx === 0 ? 'royalblue' : 'inherit' }}>{translated.singleWords.overview}</span>} {...a11yProps(0)} />
										<Tab
											label={
												<span
													className='flexContainer itemsCenter'
													style={{ color: tabIdx === 1 ? 'royalblue' : 'inherit' }}
												>
													<div style={{ marginRight: '.2em' }}>
														<Image
															src={clubCrestUrl(teamA.details.teamId)} // lastOpponent.details.customKit.isCustomTeam === '0' ? lastOpponent.details.teamId : +lastOpponent.details.customKit.crestAssetId
															alt=''
															width={20}
															height={20}
														/>
													</div>
													<PeopleIcon fontSize='small' style={{ marginRight: '.2em' }} />{Object.keys(match.players[club.infos.clubId.toString()]).length}
												</span>
											}
											{...a11yProps(1)}
										/>
										<Tab
											label={
												<span
													className='flexContainer itemsCenter'
													style={{ color: tabIdx === 2 ? 'royalblue' : 'inherit' }}
												>
													<div style={{ marginRight: '.2em' }}>
														<Image
															src={clubCrestUrl(lastOpponent.details.teamId)} // lastOpponent.details.customKit.isCustomTeam === '0' ? lastOpponent.details.teamId : +lastOpponent.details.customKit.crestAssetId
															alt=''
															width={20}
															height={20}
														/>
													</div>
													<PeopleIcon fontSize='small' style={{ marginRight: '.2em' }} />{Object.keys(match.players[oppId]).length}
												</span>
											}
											{...a11yProps(2)}
										/>
									</Tabs>
								</AppBar>

								{/* match overview */}
								<TabPanel value={tabIdx} index={0}>
									<MatchForm match={match} clubId={club.infos.clubId.toString()} oppId={`${oppId}`} />
								</TabPanel>

								{/* players match overview */}
								<TabPanel value={tabIdx} index={1}>
									<PlayerFormInMatch clubId={club.infos.clubId.toString()} match={match} playerArr={Object.keys(match.players[club.infos.clubId.toString()])} />
								</TabPanel>

								{/* opponent players match overview */}
								<TabPanel value={tabIdx} index={2}>
									<PlayerFormInMatch clubId={oppId.toString()} match={match} playerArr={Object.keys(match.players[oppId])} />
								</TabPanel>
							</div>
						</div>
					</Paper>
				)
			})}
		</>
	)
}

export default MatchOverview