import { useRouter } from 'next/router'
import Link from 'next/link'
import { pkg } from '../helpers'
import Image from 'next/image'
import appLogoLight from '../../public/imgs/appLogoLight.png'
import appNameLight from '../../public/imgs/appNameLight.png'
import { useTransContext } from '../context/transContext'

const twitterBadgeUrl = 'https://img.shields.io/twitter/follow/VirtualProSpace.svg?style=flatl&label=Follow&logo=twitter&logoColor=white&color=1da1f2'
const discordBadgeUrl = 'https://img.shields.io/discord/835310846480482314?label=Discord&logo=discord&logoColor=white'

const Footer = () => {

	const router = useRouter()

	const { translated } = useTransContext()

	return (
		<div style={{ height: '320px', marginTop: 'auto' }} className='relative ovHidden width100'>
			<div className='columnContainer itemsCenter footer' style={{ margin: '1.5em 0 0 0' }}>
				{/* App logo */}
				<div style={{ margin: '1em 0' }}>
					<Image
						src={appLogoLight.src}
						alt='logo'
						width={25}
						height={35}
						className='pointer'
						onClick={() => router.push('/')}
					/>
				</div>
				{/* footer content */}
				<div className='flexContainer itemsCenter'>
					<Link href='https://twitter.com/virtualprospace'>
						<a target='_blank' className='mr-05'>
							<Image
								src={twitterBadgeUrl}
								alt='Twitter'
								width={85}
								height={20}
							/>
						</a>
					</Link>

					<Link href='https://discord.gg/Y94sAMkCra'>
						<a target='_blank'>
							<Image
								src={discordBadgeUrl}
								alt='Discord'
								width={129}
								height={20}
							/>
						</a>
					</Link>

				</div>
				<div style={{ margin: '.5em 0px 1em 0px' }}>
					<Image
						src={appNameLight.src}
						alt='appName'
						width={100}
						height={18}
					/>
				</div>

				<p
					style={{ fontSize: '13px', margin: '1em 0', textAlign: 'center' }}
				>
					<Link href='https://gitlab.com/FirstTerraner/vps'><a target='_blank' className='href whiteTxt'>VPS</a></Link> {translated.footer.txt1}
					<br />
					{translated.footer.txt2}
					<Link href='https://choosealicense.com/licenses/gpl-3.0/'><a target='_blank' className='href whiteTxt'>{translated.singleWords.learnMore}.</a></Link>
					<br />
					info@virtualpro.space
				</p>

				<p>
					<Link href='/release'>
						<a className='href whiteTxt' style={{ fontSize: '13px', textAlign: 'center' }}>v{pkg.version}</a>
					</Link>
				</p>

			</div>
		</div>
	)
}

export default Footer