import { Paper } from '@mui/material'
import { useTransContext } from '../context/transContext'
import SmallChip from './chip'
import ErrorIcon from '@mui/icons-material/Error'

const Notice = ({ header, txt, svgSrc }: { header: string, txt: string, svgSrc: string }) => {

	const { translated } = useTransContext()

	return (
		<Paper className='playerClubPaper paperBlueGreen relative ovHidden'>
			<div className='overviewPanelHead'>
				<p className='panelHeaderTxt'>{header}</p>
				{header.includes('Attr') &&
					<SmallChip icon={<ErrorIcon />} label={translated.attributes.notAccurate} />
				}
			</div>
			<div className='panelBody'>
				<div className='columnContainer width100 txtCenter'>
					<p className='z2'>{txt}</p>
				</div>
			</div>
			<img className='matchesBall' src={svgSrc} alt='' />
		</Paper>
	)
}

export default Notice