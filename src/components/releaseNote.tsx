import { Paper } from '@mui/material'
import { ReactElement } from 'react'

/**
 * @param v (version) = String for header (version & date)
 * @param c (changes) = UL Element | undefined for new features
 * @param f (features) = UL Element | undefined for new features
 * @param b (bugs) = UL Element | undefined for bug fixes
 * @param n (notes) = String | undefined for extra note
 * @returns JSX Release Notes Element
 */
const ReleaseNote = ({ v, c, f, b, n }: { v: string, c?: ReactElement, f?: ReactElement, b?: ReactElement, n?: string }) => (
	<Paper className='releasePaper paperBlueGreen mb-1'>
		<p className='typography txtCenter width100'>
			{v}
		</p>
		<div style={{ textAlign: 'left' }}>
			{c && c}

			{f &&
				<>
					<p>New features:</p>
					{f}
				</>
			}

			{b &&
				<>
					<p>Bug fixes:</p>
					{b}
				</>
			}

			{n &&
				<p>
					<strong>Note: </strong><span>{n}</span>
				</p>
			}
		</div>
	</Paper>
)

export default ReleaseNote