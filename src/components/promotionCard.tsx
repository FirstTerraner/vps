import { Paper } from '@mui/material'
import { ReactElement } from 'react'

const PromotionCard = (
	{ icon, title, description }: { description: ReactElement<any, any>, icon: ReactElement<any, any>, title: string }
) => (
	<Paper className='promoPaper paperBlueGreen'>
		<span className='mt-1'>{icon}</span>
		<p className='promotionTitle'>{title}</p>
		{description}
	</Paper>
)

export default PromotionCard