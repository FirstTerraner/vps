import Rating from '@mui/material/Rating'
import { isStr } from '../models/typeGuards'

export const BasicRating = ({ value }: { value?: string }) => isStr(value) ? <Rating name='read-only' value={+value / 2} readOnly /> : <Rating name='no-value' value={null} />