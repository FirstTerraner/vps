import Chip from '@mui/material/Chip'
import { useRouter } from 'next/router'

const SmallChip = ({ label, icon }: { icon?: React.ReactElement<unknown, string | React.JSXElementConstructor<unknown>> | undefined, label: string, }) => {

	const router = useRouter()

	/* const handleClick = () => {
		console.info('You clicked the Chip.')
	} */

	const handleLeaderColors = () => {
		if (!router.asPath.includes('leaderboards')) { return }
		if (label === '1') { return 'bgYellow whiteTxt txtShadow' }
		if (label === '2') { return 'bgSilver whiteTxt txtShadow' }
		if (label === '3') { return 'bgOrange whiteTxt txtShadow' }
		return ''
	}

	return (
		<div style={{ display: 'flex', justifyContent: 'center', flexWrap: 'wrap', margin: '0' }}>
			<Chip
				variant='outlined'
				size='small'
				clickable={false}
				/* avatar={<Avatar>M</Avatar>} */
				label={label}
				className={handleLeaderColors()}
				color='primary'
				/* onClick={handleClick} */
				icon={icon}
				style={{ border: +label > 3 ? '1px solid royalblue' : '1px solid #999' }}
			/>
		</div>
	)
}
export default SmallChip