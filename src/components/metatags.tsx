import Head from 'next/head'
import React from 'react'
import { IMetaTagsProps } from '../models/interfaces'

/**
 * @param pageTitle the title of the page
 * @param metaTitle the title that appears on a sharing card
 * @param des the description that appears on a sharing card
 * @param metaImgSrc the image src that appears if twitterCard is set to "summary_large_image"
 * @param pageUrl the redirecting url on image click
 * @param appName the name of the website/app
 * @param metaAppLogo the logo src of the website/app
 * @param twitterCard the twitter card options 'summary' | 'summary_large_image' see https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/abouts-cards
 * @returns JSX Element for metatags
 */
const MetaTags = ({ pageTitle, metaTitle, des, metaImgSrc, pageUrl, appName, metaAppLogo, twitterCard }: IMetaTagsProps) => (
	<Head>
		<title>{pageTitle}</title>
		<meta property='og:type' content='website' />
		<meta property='og:title' content={metaTitle} />
		<meta property='og:description' content={des} />
		<meta property='og:image' content={metaImgSrc || metaAppLogo} />
		<meta property='og:url' content={pageUrl} />
		<meta property='og:locale' content='de_DE' />
		<meta property='og:locale:alternate' content='fr_FR' />
		<meta property='og:locale:alternate' content='en_US' />
		<meta property='og:locale:alternate' content='nl_NL' />
		<meta property='og:site_name' content={appName} />
		<meta name='twitter:card' content={twitterCard} />
		<meta name='twitter:site' content='@VirtualProSpace' />
		<meta name='twitter:title' content={metaTitle} />
		<meta name='twitter:description' content={des} />
		<meta name='twitter:image:alt' content={appName} />
		<meta name='twitter:image' content={metaImgSrc || metaAppLogo} />
		<meta name='twitter:image:url' content={pageUrl} />
		<meta name='apple-mobile-web-app-title' content={appName} />
		<link rel='shortcut icon' href={metaAppLogo} />
		<link rel='apple-touch-icon' href={metaAppLogo} />
		<link rel='canonical' href={pageUrl} />
		<link rel='alternate' hrefLang='de' href='https://virtualpro.space/de-DE' />
		<link rel='alternate' hrefLang='fr' href='https://virtualpro.space/fr-FR' />
		<link rel='alternate' hrefLang='nl' href='https://virtualpro.space/nl-NL' />
		{/* <link rel='stylesheet' href='https://fonts.googleapis.com/css2?family=Inter&display=optional' /> */}
	</Head>
)

export default MetaTags