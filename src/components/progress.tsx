import LinearProgress from '@mui/material/LinearProgress'
import React from 'react'

const LinearDeterminate = ({ progress, width }: { progress: number, width: number }) => (
	<LinearProgress variant='determinate' value={progress} style={{ minWidth: `${width}%` }} />
)
export default LinearDeterminate