import Link from 'next/link'
import { fifaVersion } from '../../proclubsApis'
import { useRouter } from 'next/router'
// custom components
import ClubResult from '../clubResult'
import Footer from '../footer'
import SearchClubInput from '../searchClubInput'
import AppHeader from '../appHeader'
import Loading from '../loading'
import PromotionCard from '../promotionCard'
import SoonCard from '../soonCard'
// context
import { useClubSearchContext } from '../../context/clubSearchContext'
// icons
import DateRangeIcon from '@mui/icons-material/DateRange'
import EqualizerIcon from '@mui/icons-material/Equalizer'
import ImportContactsIcon from '@mui/icons-material/ImportContacts'
import PolicyOutlinedIcon from '@mui/icons-material/PolicyOutlined'
import Nav from '../nav'
import { useBackdropContext } from '../../context/backdropContext'
import NavDrop from '../navDrop'
import { appName, appUrl, metaAppLogo, metaImgSrc } from '../../helpers'
import MetaTags from '../metatags'
import { useTransContext } from '../../context/transContext'

const SearchPage = () => {

	const router = useRouter()

	const { translated } = useTransContext()

	const { withNav } = useBackdropContext()

	const { clubs } = useClubSearchContext()

	return (
		<div
			className='gonimate fadeIn'
		>
			<MetaTags
				pageTitle={`VPS | ${translated.singleWords.search} ${translated.singleWords.club}`}
				metaTitle={`${translated.singleWords.search} FIFA ${fifaVersion} Pro CLubs | PS4, PS5, XBOXONE, XBSXS, & PC`}
				des={translated.searchPage.metaDes}
				pageUrl={appUrl}
				appName={appName}
				metaImgSrc={metaImgSrc}
				metaAppLogo={metaAppLogo}
				twitterCard='summary_large_image'
			/>

			<Loading />

			<Nav />

			<AppHeader
				pageName={`SEARCH ${router.query.name && router.asPath.includes('/club/search/') ? `"${router.query.name.toString()}"` : ''}`}
			/>

			<div className='pageWrapper columnContainer justifyCenter itemsCenter'>

				<SearchClubInput />

				{/* {clubs && clubs.length > 1 && <p>Found {clubs.length} result{clubs.length > 1 ? 's' : ''}.</p>} */}

				{/* search clubs response */}
				<div className='rankingClubWrap'>
					{clubs && clubs.map((club, i) => (
						<div className='rankingResultsWrap' key={i}>
							<ClubResult club={club} i={i} />
						</div>
					))}
				</div>

				{/* promotion cards */}
				<div className='promoPaperWrap'>
					<div className='promoPaperCol'>
						<PromotionCard
							icon={<PolicyOutlinedIcon fontSize='large' style={{ color: 'royalblue' }} />}
							title={translated.homepage.searchAndFav}
							description={
								<p className='promoDescription'>
									{translated.homepage.searchAndFavDes}
								</p>
							}
						/>
						<PromotionCard
							icon={<EqualizerIcon fontSize='large' style={{ color: 'royalblue' }} />}
							title={translated.homepage.clubAndSquad}
							description={
								<p className='promoDescription'>
									{translated.homepage.clubAndSquadDes}
								</p>
							}
						/>
					</div>

					<div className='promoPaperCol'>
						<PromotionCard
							icon={<DateRangeIcon fontSize='large' style={{ color: 'royalblue' }} />}
							title={translated.homepage.resultAndForm}
							description={
								<p className='promoDescription'>
									{translated.homepage.resultAndFormDes}
								</p>
							}
						/>
						<PromotionCard
							icon={<ImportContactsIcon fontSize='large' style={{ color: 'royalblue' }} />}
							title={translated.homepage.openSrc}
							description={
								<p className='promoDescription'>
									{translated.homepage.openSrcDes1} <Link href='/contribute'><a className='href' target='blank'>{translated.singleWords.contribute}</a></Link> {translated.homepage.openSrcDes2} <Link href='https://discord.gg/Y94sAMkCra'><a target='blank' className='href'>discord {translated.singleWords.server}</a></Link> {translated.homepage.openSrcDes3}.
								</p>
							}
						/>
					</div>
				</div>

				{/* coming soon component */}
				<SoonCard svgSrc='../../../../svgs/Falcao.svg' />

				{/* <div className='svgPaperWrap'>
					<LeaderboardCard svgSrc='../../../../svgs/Lozano.svg' />
				</div> */}

			</div>

			<Footer />

			{withNav && <NavDrop />}

		</div>
	)
}

export default SearchPage