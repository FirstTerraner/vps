import Link from 'next/link'
// custom components
import Footer from '../footer'
import Loading from '../loading'
import PromotionCard from '../promotionCard'
import AppHeader from '../appHeader'
import StayTunedCard from '../stayTunedCard'
// icons
// import EmojiEventsIcon from '@mui/icons-material/EmojiEvents'
// import GradeOutlinedIcon from '@mui/icons-material/GradeOutlined'
// import AnnouncementIcon from '@mui/icons-material/Announcement'
// import LanguageOutlinedIcon from '@mui/icons-material/LanguageOutlined'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import Nav from '../nav'
import { useBackdropContext } from '../../context/backdropContext'
import NavDrop from '../navDrop'
import React from 'react'
import { appUrl, appName, metaImgSrc, metaAppLogo } from '../../helpers'
import MetaTags from '../metatags'
import { useTransContext } from '../../context/transContext'

// page component
const UpcomingPage = () => {

	const { translated } = useTransContext()

	const { withNav } = useBackdropContext()

	return (

		<div className='gonimate fadeIn'>

			<MetaTags
				pageTitle={`VPS | ${translated.singleWords.soon}`}
				metaTitle={translated.upcomingPage.metaTitle}
				des={translated.upcomingPage.metaDes}
				pageUrl={`${appUrl}/upcoming`}
				appName={appName}
				metaImgSrc={metaImgSrc}
				metaAppLogo={metaAppLogo}
				twitterCard='summary_large_image'
			/>

			<Loading />

			<Nav />

			<AppHeader pageName={translated.upcomingPage.pageTitle.toUpperCase()} />

			{/* page content */}
			<div className='pageWrapper columnContainer justifyCenter itemsCenter'>

				<div className='promoPaperCol mb-1'>
					<p className='typography txtCenter width100'>
						{translated.upcomingPage.issueInfo} GitLab <Link href='https://gitlab.com/FirstTerraner/vps/-/issues'><a className='href' target='_blank'>{translated.upcomingPage.issueTracker}</a></Link>.
					</p>
				</div>

				{/* promotion cards */}
				<PromotionCard
					icon={<PersonOutlineIcon fontSize='large' style={{ color: 'royalblue' }} />}
					title={translated.upcomingPage.userAcc}
					description={
						<p style={{ padding: '0 .5em' }}>
							{translated.upcomingPage.userPromo}
						</p>
					}
				/>

				{/* <div className='promoPaperCol'>
						<PromotionCard
							icon={<EmojiEventsIcon fontSize='large' style={{ color: 'royalblue' }} />}
							title='Tournaments'
							description={
								<p style={{ padding: '0 .5em' }}>
									Soon we will be hosting lowman and 11s tournaments.
									Join our <Link href='https://discord.gg/Y94sAMkCra'><a className='href' target='_blank' >discord server</a></Link> for more information.
								</p>
							}
						/>

						<div className='promoPaperCol'>
							<PromotionCard
								icon={<EmojiEventsIcon fontSize='large' style={{ color: 'royalblue' }} />}
								title='Tournaments'
								description={
									<p style={{ padding: '0 .5em' }}>
										Soon we will be hosting lowman and 11s tournaments.
										Join our <Link href='https://discord.gg/Y94sAMkCra'><a className='href' target='_blank' >discord server</a></Link> for more information.
									</p>
								}
							/>
							<PromotionCard
								icon={<AnnouncementIcon fontSize='large' style={{ color: 'royalblue' }} />}
								title='Discord notifications'
								description={
									<p style={{ padding: '0 .5em' }}>
										A bot that posts web-app or user related messages in our discord server to keep the community synced.
									</p>
								}
							/>
							<PromotionCard
								icon={<LanguageOutlinedIcon fontSize='large' style={{ color: 'royalblue' }} />}
								title='Translation'
								description={
									<p style={{ padding: '0 .5em' }}>
										Supporting widely used languages has a high priority in this project.
										You can help adding your language <Link href='/contribute'><a className='href'>here</a></Link>.
									</p>
								}
							/>
						</div>
					</div> */}
				<StayTunedCard
					svg1Src='../../svgs/scissorBall.svg'
					svg2Src='../../svgs/scissor.svg'
				/>

			</div>

			{/* Footer */}
			<Footer />

			{withNav && <NavDrop />}

		</div>
	)
}

export default UpcomingPage