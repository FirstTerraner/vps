// libs
import Link from 'next/link'
// icons
import DateRangeIcon from '@mui/icons-material/DateRange'
import EqualizerIcon from '@mui/icons-material/Equalizer'
import ImportContactsIcon from '@mui/icons-material/ImportContacts'
import PolicyOutlinedIcon from '@mui/icons-material/PolicyOutlined'
// helpers
import { fifaVersion } from '../../proclubsApis'
// custom components
import FavClubs from '../favClubs'
import Footer from '../footer'
import Loading from '../loading'
import PromotionCard from '../promotionCard'
import SearchClubInput from '../searchClubInput'
import AppHeader from '../appHeader'
import LeaderboardCard from '../leaderCard'
import SoonCard from '../soonCard'
import StayTunedCard from '../stayTunedCard'
import React from 'react'
import { getPlatformLocalStorage, setPlatformLocalStorage } from '../../localStorage/platform'
// import NoMatches from '../noMatchesPaper'
import AddRecordCard from '../recordCard'
import SimpleBackdrop from '../backdrop'
import Nav from '../nav'
import { useBackdropContext } from '../../context/backdropContext'
import NavDrop from '../navDrop'
import { appName, appUrl, metaAppLogo, metaImgSrc } from '../../helpers'
import MetaTags from '../metatags'
import { useTransContext } from '../../context/transContext'

const Homepage = () => {

	const { translated } = useTransContext()

	const { withNav } = useBackdropContext()

	// TODO try serversideprops for local storage
	React.useEffect(() => {

		if (!getPlatformLocalStorage()) {
			// set platform in local storage to ps4 as default
			setPlatformLocalStorage('ps4')
		}

	}, [])

	return (

		<div className='gonimate fadeIn one'>

			<MetaTags
				pageTitle={appName}
				metaTitle={`FIFA ${fifaVersion} Pro Clubs ${translated.homepage.metaTitle}`}
				des={`${translated.homepage.metaDes} https://discord.gg/Y94sAMkCra`}
				pageUrl={appUrl}
				metaImgSrc={metaImgSrc}
				appName={appName}
				metaAppLogo={metaAppLogo}
				twitterCard='summary_large_image'
			/>

			<Loading />

			<AppHeader pageName={`FIFA ${fifaVersion} PRO CLUBS`} />

			<Nav />

			{/* page content */}
			<div className='pageWrapper columnContainer justifyCenter itemsCenter'>

				{/* FIFA22 update */}
				{/* <NoMatches
					header='FIFA22 Update Note'
					txt='This website is taking a break due to frequent changes made by EA Sports. The functionality will be available again once FIFA22 launches'
					svgSrc='../../../svgs/scissorBall.svg'
				/> */}

				{/* search club by name component */}
				<SearchClubInput />

				{/* favorites component */}
				<FavClubs />

				<AddRecordCard />

				{/* leaderboard & coming soon */}
				<div className='svgPaperWrap'>
					{/* leaderboards component */}
					<LeaderboardCard svgSrc='../../svgs/Lozano.svg' />

					{/* coming soon component */}
					<SoonCard svgSrc='../../svgs/Falcao.svg' />
				</div>

				{/* promotion cards */}
				<div className='promoPaperWrap'>
					<div className='promoPaperCol'>
						<PromotionCard
							icon={<PolicyOutlinedIcon fontSize='large' style={{ color: 'royalblue' }} />}
							title={translated.homepage.searchAndFav}
							description={
								<p className='promoDescription'>
									{translated.homepage.searchAndFavDes}
								</p>
							}
						/>
						<PromotionCard
							icon={<EqualizerIcon fontSize='large' style={{ color: 'royalblue' }} />}
							title={translated.homepage.clubAndSquad}
							description={
								<p className='promoDescription'>
									{translated.homepage.clubAndSquadDes}
								</p>
							}
						/>
					</div>

					<div className='promoPaperCol'>
						<PromotionCard
							icon={<DateRangeIcon fontSize='large' style={{ color: 'royalblue' }} />}
							title={translated.homepage.resultAndForm}
							description={
								<p className='promoDescription'>
									{translated.homepage.resultAndFormDes}
								</p>
							}
						/>
						<PromotionCard
							icon={<ImportContactsIcon fontSize='large' style={{ color: 'royalblue' }} />}
							title={translated.homepage.openSrc}
							description={
								<p className='promoDescription'>
									{translated.homepage.openSrcDes1} <Link href='/contribute'><a className='href' target='blank'>{translated.singleWords.contribute}</a></Link> {translated.homepage.openSrcDes2} <Link href='https://discord.gg/Y94sAMkCra'><a target='blank' className='href'>discord {translated.singleWords.server}</a></Link> {translated.homepage.openSrcDes3}.
								</p>
							}
						/>
					</div>
				</div>

				{/* stay tuned component */}
				<StayTunedCard
					svg1Src='../../svgs/scissorBall.svg'
					svg2Src='../../svgs/scissor.svg'
				/>

			</div>

			{/* Footer */}
			<Footer />

			{/* backdrop */}
			<SimpleBackdrop />

			{withNav && <NavDrop />}

		</div>
	)
}

export default Homepage
