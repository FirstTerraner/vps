import { useBackdropContext } from '../../context/backdropContext'
import { useTransContext } from '../../context/transContext'
import { appUrl, appName, metaImgSrc, metaAppLogo } from '../../helpers'
import AppHeader from '../appHeader'
import Footer from '../footer'
import Loading from '../loading'
import MetaTags from '../metatags'
import Nav from '../nav'
import NavDrop from '../navDrop'
import SettingsCard from '../settingsCard'
// import StayTunedCard from '../stayTunedCard'

const SettingsPage = () => {

	const { translated } = useTransContext()

	const { withNav } = useBackdropContext()

	return (
		<div className='gonimate fadeIn'>

			<MetaTags
				pageTitle={`VPS | ${translated.singleWords.settings}`}
				metaTitle={translated.settingsPage.metaTitle}
				des={translated.settingsPage.metaDes}
				pageUrl={`${appUrl}/upcoming`}
				appName={appName}
				metaImgSrc={metaImgSrc}
				metaAppLogo={metaAppLogo}
				twitterCard='summary_large_image'
			/>

			<Loading />

			<Nav />

			<AppHeader pageName={translated.singleWords.settings.toUpperCase()} />

			{/* page content */}
			<div className='pageWrapper columnContainer justifyCenter itemsCenter'>

				<SettingsCard />

				{/* <StayTunedCard
					svg1Src='../../svgs/scissorBall.svg'
					svg2Src='../../svgs/scissor.svg'
				/> */}

			</div>

			{/* Footer */}
			<Footer />

			{withNav && <NavDrop />}

		</div>
	)
}

export default SettingsPage