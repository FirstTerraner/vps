import Footer from '../footer'
import Loading from '../loading'
import AppHeader from '../appHeader'
import StayTunedCard from '../stayTunedCard'
import Link from 'next/link'
import Nav from '../nav'
import { useBackdropContext } from '../../context/backdropContext'
import NavDrop from '../navDrop'
import { appName, appUrl, metaAppLogo, metaImgSrc } from '../../helpers'
import MetaTags from '../metatags'

const ContributePage = () => {

	const { withNav } = useBackdropContext()

	return (

		<div className='gonimate fadeIn'>

			<MetaTags
				pageTitle='Contribute to VPS'
				metaTitle={`Contribute to ${appName}`}
				des='This project is open source and anybody can contribute. Follow the guide step by step.'
				pageUrl={`${appUrl}/contribute`}
				metaImgSrc={metaImgSrc}
				appName={appName}
				metaAppLogo={metaAppLogo}
				twitterCard='summary_large_image'
			/>

			<AppHeader pageName='CONTRIBUTE' />

			<Loading />

			<Nav />

			{/* page content */}
			<div className='pageWrapper columnContainer itemsCenter'>
				<div className='ContributeCol'>
					<h1 className='txtCenter'>Getting started</h1>
					<p className='txtCenter'>(English only)</p>
					<p className='typography'>
						If you are familiar with Git, React and Next JS, you can start at point 1.
					</p>
					<p className='typography'>To learn more, take a look at the following resources:</p>
					<ul>
						<li>
							<Link href='https://git-scm.com/docs/gittutorial'><a target='_blank' className='href'>Git documentation</a></Link>
						</li>
						<li>
							<Link href='https://github.com/git-guides/install-git'><a target='_blank' className='href'>Install git</a></Link>
						</li>
						<li>
							<Link href='https://reactjs.org/tutorial/tutorial.html'><a target='_blank' className='href'>React introduction</a></Link>
						</li>
						<li>
							<Link href='https://nextjs.org/docs'><a target='_blank' className='href'>Next.js documentation</a></Link>
						</li>
						<li>
							<Link href='https://nextjs.org/learn'><a target='_blank' className='href'>Next.js tutorial</a></Link>
						</li>
					</ul>
					<p className='typography'>
						<Link href='https://gitlab.com/firstTerraner/vps'><a target='_blank' className='href'>View source code</a></Link>
					</p>
					<p className='typography'>
						1. Open your terminal & clone the project from your wished directory.
					</p>
					<code className='codeWindow mb-1'>
						<div className='codeContent'>
							<p style={{ wordBreak: 'break-word' }}>git clone https://gitlab.com/FirstTerraner/vps.git</p>
							<p style={{ color: '#666' }}># navigate to the project</p>
							cd vps
						</div>
					</code>
					<p className='typography'>
						2. Create a new branch
						from <Link href='https://gitlab.com/FirstTerraner/vps/-/branches'><a target='_blank' className='href'>recent branches</a></Link>.
					</p>
					<code className='codeWindow mb-1'>
						<div className='codeContent'>
							git branch my-awesome-branch
							<p style={{ color: '#666' }}># switch to the new branch</p>
							git checkout my-awesome-branch
						</div>
					</code>
					<p className='typography'>
						3. Create a .env file.
					</p>
					<code className='codeWindow' >
						<div className='codeContent'>
							echo -e &apos;REDIS_URL=your-redis-url\nPRIVATE_TOKEN=your-gitlab-token&apos; {'>'}{'>'} .env
						</div>
					</code>
					<p>
						*Get your <Link href='https://upstash.com/'><a target='_blank' className='href'>REDIS URL</a></Link>.
					</p>
					<p className='mb-1'>
						*Get your <Link href='https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html'><a target='_blank' className='href'>GITLAB TOKEN</a></Link>.
					</p>
					<p className='typography'>
						4. Install <Link href='https://gitlab.com/FirstTerraner/vps/-/blob/main/package.json'><a target='_blank' className='href'>dependencies</a></Link>.
					</p>
					<code className='codeWindow mb-1'>
						<div className='codeContent'>
							npm i
							<p style={{ color: '#666' }}># Now you are good to go, start the development server:</p>
							npm run dev
						</div>
					</code>
					<p className='typography'>
						5. Open <Link href='http://localhost:3000'><a target='_blank' className='href'>http://localhost:3000</a></Link> to see the result.
					</p>
					<p className='typography'>
						You can start editing the files now. &#129304; For bigger changes, open
						an <Link href='https://gitlab.com/FirstTerraner/vps/-/issues'><a target='_blank' className='href'>issue</a></Link> first to discuss the topic.
					</p>
					<p className='typography'>
						After your changes are done, create a merge request.
						It will get merged after a review & all related issues/discussions have been closed.
						<br />
						You will need an account
						at <Link href='https://gitlab.com'><a target='_blank' className='href'>GitLab</a></Link> to
						create merge requests.
					</p>
					<p className='typography mt-1'>
						If you have any questions, send us a message in the support section of
						our <Link href='https://discord.gg/Y94sAMkCra'><a target='_blank' className='href'>discord server</a></Link> or via e-mail: info@virtualpro.space.
					</p>
					<p className='typography mb-1'>
						No time to contribute code? You can also speed up the development process in
						the <Link href='/donations'><a className='href'>donation section</a></Link>. &#128640;
					</p>
					<div className='columnContainer itemsCenter JustifyCenter'>
						<StayTunedCard
							svg1Src='../../svgs/scissorBall.svg'
							svg2Src='../../svgs/scissor.svg'
						/>
					</div>
				</div>

			</div>

			<Footer />

			{withNav && <NavDrop />}

		</div>
	)
}

export default ContributePage