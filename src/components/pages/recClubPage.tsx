import RoomIcon from '@mui/icons-material/Room'
import { Paper } from '@mui/material'
import { useRouter } from 'next/router'
import { isPlatformType } from 'proclubs-api/dist'
import React from 'react'
// helpers
import { fifaVersion } from '../../proclubsApis'
import { useBackdropContext } from '../../context/backdropContext'
import {
	appName,
	clubCrestUrl,
	deciToHex, getRegionFromId, upperCasePlatform
} from '../../helpers'
import { IRecClubOverviewState } from '../../models/interfaces'
import AppHeader from '../appHeader'
import SmallChip from '../chip'
import ClubOverall from '../clubOverview/overall'
// custom components
import Footer from '../footer'
import Loading from '../loading'
import Nav from '../nav'
import NavDrop from '../navDrop'
import Notice from '../notice'
import { BasicRating } from '../rating'
import MetaTags from '../metatags'
import stadiumIcon from '../../../public/imgs/stadium-icon.png'
import Image from 'next/image'
import { useTransContext } from '../../context/transContext'
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight'

const RecClubPage = ({ club }: { club: IRecClubOverviewState }) => {

	const router = useRouter()

	const { translated } = useTransContext()

	const { withNav } = useBackdropContext()

	// console.log('club in rec clubPage: ', club)
	// console.log('squad: ', squad)
	// if(!club) { return null }

	const metaDescription = club.stats ? `Pro Clubs ${translated.singleWords.record} | ${club.stats.wins} - ${club.stats.ties} - ${club.stats.losses} | ${club.stats.totalGames || ''} ${translated.singleWords.matches} | ${club.stats.alltimeGoals} ${translated.singleWords.goals} | ${club.stats.alltimeGoalsAgainst} ${translated.singleWords.goalsAgainst}`
		: translated.clubPage.noStats

	return (
		<>
			<MetaTags
				pageTitle={`VPS | ${club.infos?.name || ''}`}
				metaTitle={club.infos ? club.infos.name + ` | ${translated.singleWords.record} | FIFA ${fifaVersion} | ${isPlatformType(router.query.platform) ? upperCasePlatform(router.query.platform) || '' : '' + ' | '} | ${getRegionFromId(club.infos?.regionId)}` : appName}
				des={metaDescription}
				pageUrl={`https://virtualpro.space/${router.query.platform?.toString() || ''}/club/${club.infos?.clubId || ''}`}
				appName={appName}
				metaAppLogo={clubCrestUrl(club.infos?.teamId)}
				twitterCard='summary'
			/>

			<Loading />

			<Nav />

			{/* page content header */}
			<AppHeader clubInfo={club.infos} showFav />

			{/* page content */}
			<div className='pageWrapper'>

				<div className='columnContainer itemsCenter justifyCenter'>

					{/* History */}
					{club.stats && +club.stats.totalGames > 0 ?
						<Paper className='overviewPaper paperBlueGreen'>
							<div className='overviewPanelHead'>
								<p className='panelHeaderTxt'>{translated.singleWords.record} {translated.singleWords.history}</p>
								<span>{club.stats.wins}&nbsp;&#8226;&nbsp;{club.stats.ties}&nbsp;&#8226;&nbsp;{club.stats.losses}</span>
							</div>
						</Paper>
						:
						<Notice
							header='Club Overview'
							txt='No Matches yet.'
							svgSrc='/svgs/scissorBall.svg'
						/>
					}

					{/* Rating */}
					<Paper className='overviewPaper paperBlueGreen'>
						<div className='overviewPanelHead'>
							<p className='panelHeaderTxt'>{translated.singleWords.rating}</p>
							<BasicRating value={club.stats?.starLevel} />
						</div>
					</Paper>

					{/* Overview */}
					<ClubOverall club={club} />

					{/* All club info if available */}
					{club.stats && !club.stats.hasLost &&
						<Paper className='overviewPaper paperBlueGreen pointer'>
							<div
								className='overviewPanelHead'
								onClick={() => {
									if (!club.stats) { return }
									void router.push(`/${club.stats.plat}/club/${club.stats.clubId}`)
								}}
							>
								<p className='panelHeaderTxt'>{translated.singleWords.moreInfo}</p>
								<KeyboardArrowRightIcon />
							</div>
						</Paper>
					}

					<Paper className='overviewPaper paperBlueGreen'>
						<div className='overviewPanelHead'>
							<p className='panelHeaderTxt'>{translated.singleWords.record} {translated.singleWords.squad}</p>
							<SmallChip label='Coming soon' />
						</div>
					</Paper>

					<Paper className='overviewPaper paperBlueGreen'>
						<div className='overviewPanelHead'>
							<p className='panelHeaderTxt'>{translated.singleWords.matches}</p>
							<SmallChip label='Coming soon' />
						</div>
					</Paper>

					{/* Stadium */}
					<Paper className='overviewPaper paperBlueGreen'>
						<div className='overviewPanelHead'>
							<p className='panelHeaderTxt'>{translated.singleWords.stadium}</p>
						</div>
						<div className='panelBody'>
							<div className='flexContainer itemsCenter'>
								<Image
									src={stadiumIcon.src}
									alt='stadium-icon'
									width={35}
									height={35}
								/>
								<div className='stadiumInfos'>
									<span>{club.infos?.customKit.stadName}</span>
									<span>Region: {getRegionFromId(club.infos?.regionId)}</span>
								</div>
							</div>
							<RoomIcon style={{ color: 'royalblue' }} fontSize='large' />
						</div>
					</Paper>

					{/* Crests */}
					{club.infos && club.infos.customKit.isCustomTeam === '1' &&
						<Paper className='overviewPaper paperBlueGreen'>
							<div className='overviewPanelHead justifyBetween'>
								<p className='panelHeaderTxt'>{translated.clubPage.kits}</p>
								<div className='flexContainer justifyCenter itemsCenter mr-1'>
									<span className='kitColor' style={{ backgroundColor: `#${deciToHex(+club.infos.customKit.kitAColor1)}` }} ></span>
									<span className='kitColor' style={{ backgroundColor: `#${deciToHex(+club.infos.customKit.kitAColor2)}` }} ></span>
									<span className='kitColor' style={{ backgroundColor: `#${deciToHex(+club.infos.customKit.kitAColor3)}` }} ></span>
								</div>
							</div>
						</Paper>
					}

				</div>

			</div>

			<Footer />

			{withNav && <NavDrop />}
		</>
	)
}

export default RecClubPage