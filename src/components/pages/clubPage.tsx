// icons
import RoomIcon from '@mui/icons-material/Room'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import { AppBar, Paper, Tab, Tabs } from '@mui/material'
import { useRouter } from 'next/router'
import { isPlatformType } from 'proclubs-api/dist'
// libs
import { IClubMemberCareer } from 'proclubs-api/dist/model/club'
import React from 'react'
// helpers
import { fifaVersion } from '../../proclubsApis'
import { useBackdropContext } from '../../context/backdropContext'
import {
	appName,
	appUrl,
	deciToHex,
	getAllPlayersFromLastMatches,
	getDivOverview,
	getRegionFromId,
	metaAppLogo,
	upperCasePlatform
} from '../../helpers'
import { IClubOverviewState, ISquadOverviewState, TMatchesState } from '../../models/interfaces'
import { isNum } from '../../models/typeGuards'
import AppHeader from '../appHeader'
// custom components
import SmallChip from '../chip'
import DivisionForm from '../clubOverview/divisionForm'
import LastMatches from '../clubOverview/lastMatches'
import ClubOverall from '../clubOverview/overall'
import RecentResults from '../clubOverview/recentResults'
import SquadOverview from '../clubOverview/squad'
import SquadList from '../clubOverview/squadList'
import Footer from '../footer'
import Loading from '../loading'
import Nav from '../nav'
import NavDrop from '../navDrop'
import Notice from '../notice'
import { BasicRating } from '../rating'
import DivisionSlider from '../slider'
import TabPanel, { a11yProps } from '../tabPanel'
import MetaTags from '../metatags'
import stadiumIcon from '../../../public/imgs/stadium-icon.png'
import Image from 'next/image'
import { useTransContext } from '../../context/transContext'

const ClubPage = (
	{ club, matches, cupMatches, squad }: { club: IClubOverviewState, matches?: TMatchesState, cupMatches?: TMatchesState, squad?: ISquadOverviewState }
) => {

	const router = useRouter()

	const { translated } = useTransContext()

	const { withNav } = useBackdropContext()

	const handleTabIdxRoute = () => {
		if (router.asPath.includes('/squad')) { return 1 }
		if (router.asPath.includes('/div-matches')) { return 2 }
		if (router.asPath.includes('/division')) { return 3 }
		if (router.asPath.includes('/cup-matches')) { return 4 }
		return 0
	}
	// club overview tab idx
	const [tabIdx, setTabIdx] = React.useState(handleTabIdxRoute())
	const handleTabChange = (_event: React.SyntheticEvent, newIdx: unknown) => {
		setTabIdx(isNum(newIdx) ? newIdx : 0)
		const plat = router.query.platform?.toString()
		const cId = router.query.id?.toString()
		if (!plat || !cId) { return }

		if (newIdx === 0) {
			void router.replace(`/${plat}/club/${cId}`)
			return
		}

		if (newIdx === 1) {
			void router.replace(`/${plat}/club/${cId}/squad`)
			return
		}
		if (newIdx === 2) {
			void router.replace(`/${plat}/club/${cId}/div-matches`)
			return
		}
		if (newIdx === 3) {
			void router.replace(`/${plat}/club/${cId}/division`)
			return
		}
		if (newIdx === 4) {
			void router.replace(`/${plat}/club/${cId}/cup-matches`)
		}
	}

	// recent players
	const [recentPlayers, setRecentPlayers] = React.useState<IClubMemberCareer[]>([])
	React.useEffect(() => {
		if (!club.infos || !matches || !cupMatches) { return }
		setRecentPlayers(getAllPlayersFromLastMatches([...matches, ...cupMatches], club.infos.clubId))
	}, [])

	const metaDescription = club.stats ? `Pro Clubs ${translated.singleWords.division} ${club.stats.currentDivision} | ${club.stats.wins} - ${club.stats.ties} - ${club.stats.losses} | ${club.stats.totalGames || ''} ${translated.singleWords.matches} | ${club.stats.alltimeGoals} ${translated.singleWords.goals} | ${club.stats.alltimeGoalsAgainst} ${translated.singleWords.goalsAgainst}`
		: `FIFA ${fifaVersion} Pro Clubs ${translated.clubPage.noStats}`
	// console.log('club.stats.starLevel: ', club.stats?.starLevel)
	// console.log('squad: ', squad)
	// if(!club) { return null }

	return (
		<>
			<MetaTags
				pageTitle={`VPS | ${club.infos?.name || ''}`}
				metaTitle={club.infos ? club.infos.name + ` | FIFA ${fifaVersion} | ${isPlatformType(router.query.platform) ? upperCasePlatform(router.query.platform) || '' : '' + ' | '} | ${getRegionFromId(club.infos?.regionId)}` : appName}
				des={metaDescription}
				pageUrl={`${appUrl}/${router.query.platform?.toString() || ''}/club/${club.infos?.clubId || ''}`}
				appName={appName}
				metaAppLogo={metaAppLogo}
				twitterCard='summary'
			/>

			<Loading />

			<Nav />

			{withNav && <NavDrop />}

			{/* page content header */}
			<AppHeader clubInfo={club.infos} showFav />

			{!club.infos && !club.stats ?
				<>
					{/* club not available */}
					<div className='pageWrapper gonimate fadeIn'>
						<div style={{ marginTop: '5em' }} className='columnContainer itemsCenter'>
							<DeleteForeverIcon fontSize='large' color='primary' />
							<h3 style={{ textAlign: 'center' }}>{translated.clubPage.clubDel}</h3>
						</div>
						<p style={{ textAlign: 'center', padding: '0 1em' }}>
							{translated.clubPage.findSquad} {translated.clubPage.join}
						</p>
					</div>
				</>
				:
				<>
					{/* tabs */}
					<AppBar position='static' color='transparent' elevation={0}>
						<Tabs
							className='clubTabs'
							value={tabIdx}
							onChange={handleTabChange}
							indicatorColor='primary'
							textColor='inherit'
							variant='scrollable'
						>
							<Tab label={<span style={{ color: tabIdx === 0 ? 'royalblue' : 'inherit' }}>{translated.singleWords.overview}</span>} {...a11yProps(0)} />
							<Tab label={<span style={{ color: tabIdx === 1 ? 'royalblue' : 'inherit' }}>{translated.singleWords.squad}</span>} {...a11yProps(1)} />
							<Tab label={<span style={{ color: tabIdx === 2 ? 'royalblue' : 'inherit' }}>{translated.singleWords.matches}</span>} {...a11yProps(2)} />
							<Tab label={<span style={{ color: tabIdx === 3 ? 'royalblue' : 'inherit' }}>{translated.singleWords.division + ' '} {club.stats?.currentDivision}</span>} {...a11yProps(3)} />
							<Tab label={<span style={{ color: tabIdx === 4 ? 'royalblue' : 'inherit' }}>{translated.singleWords.cup}</span>} {...a11yProps(4)} />

						</Tabs>
					</AppBar>

					{/* page content */}
					<div className='pageWrapper gonimate fadeIn'>

						{/* Club Overview Panel */}
						<TabPanel value={tabIdx} index={0}>

							{/* History */}
							{club.stats && +club.stats.totalGames > 0 ?
								<Paper className='overviewPaper paperBlueGreen'>
									<div className='overviewPanelHead'>
										<p className='panelHeaderTxt'>{translated.singleWords.history}</p>
										<span>{club.stats.wins}&nbsp;&#8226;&nbsp;{club.stats.ties}&nbsp;&#8226;&nbsp;{club.stats.losses}</span>
									</div>
								</Paper>
								:
								<Notice
									header={`Club ${translated.singleWords.overview}`}
									txt={translated.clubPage.noMatches}
									svgSrc='/svgs/scissorBall.svg'
								/>
							}

							{/* Last DIVISION Matches */}
							<LastMatches club={club} setTabIdxCB={handleTabChange} headerTxt={translated.singleWords.form} />

							{/* Rating */}
							<Paper className='overviewPaper paperBlueGreen'>
								<div className='overviewPanelHead'>
									<p className='panelHeaderTxt'>{translated.singleWords.rating}</p>
									<BasicRating value={club.stats?.starLevel} />
								</div>
							</Paper>

							{/* Overview */}
							<ClubOverall club={club} />

							{/* Stadium */}
							<Paper className='overviewPaper paperBlueGreen'>
								<div className='overviewPanelHead'>
									<p className='panelHeaderTxt'>{translated.singleWords.stadium}</p>
								</div>
								<div className='panelBody'>
									<div className='flexContainer itemsCenter'>
										<Image
											src={stadiumIcon.src}
											alt='stadium-icon'
											width={35}
											height={35}
										/>
										<div className='stadiumInfos'>
											<span>{club.infos?.customKit.stadName}</span>
											<span>{translated.singleWords.region}: {getRegionFromId(club.infos?.regionId)}</span>
										</div>
									</div>
									<RoomIcon style={{ color: 'royalblue' }} fontSize='large' />
								</div>
							</Paper>

							{/* Crests */}
							{club.infos && club.infos.customKit.isCustomTeam === '1' &&
								<Paper className='overviewPaper paperBlueGreen'>
									<div className='overviewPanelHead justifyBetween'>
										<p className='panelHeaderTxt'>{translated.clubPage.kits}</p>
										<div className='flexContainer justifyCenter itemsCenter mr-1'>
											<span className='kitColor' style={{ backgroundColor: `#${deciToHex(+club.infos.customKit.kitAColor1)}` }} ></span>
											<span className='kitColor' style={{ backgroundColor: `#${deciToHex(+club.infos.customKit.kitAColor2)}` }} ></span>
											<span className='kitColor' style={{ backgroundColor: `#${deciToHex(+club.infos.customKit.kitAColor3)}` }} ></span>
										</div>
									</div>
								</Paper>
							}
						</TabPanel>

						{/* Squad Panel */}
						<TabPanel value={tabIdx} index={1}>
							{club && squad ?
								<SquadOverview club={club} squad={squad} />
								:
								<Notice
									header={translated.singleWords.squad}
									txt={translated.clubPage.noSquad}
									svgSrc='/svgs/scissorBall.svg'
								/>
							}
						</TabPanel>

						{/* Division Form Panel */}
						<TabPanel value={tabIdx} index={2}>
							<RecentResults
								club={club}
								matches={matches}
								pageTitle={translated.singleWords.form}
							/>
							{/* squadlist in last matches */}
							<SquadList memberArr={recentPlayers} title={translated.clubPage.pInLastMatches} />
						</TabPanel>

						{/* Division Panel */}
						<TabPanel value={tabIdx} index={3}>
							{club.stats ?
								<Paper className='overviewPaper relative ovHidden paperBlueGreen'>
									<div className='overviewPanelHead'>
										<p className='panelHeaderTxt'>{translated.singleWords.points}</p>
										<div>
											<SmallChip label={`Matches left: ${10 - +club.stats.gamesPlayed}`} />
										</div>
									</div>
									<div className='panelBody' style={{ minHeight: '120px' }}>
										<div className='width100'>
											<DivisionSlider def={+club.stats.points} marks={getDivOverview(club.stats.currentDivision) || []} />
										</div>
									</div>
									<img className='matchesBall' src='../../../svgs/scissorBall.svg' alt='' />
								</Paper>
								:
								<Notice
									header={`Club "${club.infos?.name || ''}"`}
									txt={translated.clubPage.noMatches}
									svgSrc='/svgs/scissorBall.svg'
								/>
							}
							{<DivisionForm club={club} />}

						</TabPanel>

						{/* Cup Form Panel */}
						<TabPanel value={tabIdx} index={4}>
							{cupMatches && cupMatches.length > 0 ?
								<>
									<RecentResults
										club={club}
										matches={cupMatches}
										pageTitle={translated.singleWords.form}
									/>
									{/* squadlist in last matches */}
									<SquadList memberArr={recentPlayers} title={translated.clubPage.pInLastMatches} />
								</>
								:
								<Notice
									header={translated.singleWords.form}
									txt={translated.clubPage.noMatches}
									svgSrc='/svgs/scissorBall.svg'
								/>
							}
						</TabPanel>

					</div>
				</>
			}

			<Footer />
		</>
	)
}

export default ClubPage