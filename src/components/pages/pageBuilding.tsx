import Footer from '../footer'
import Loading from '../loading'
import BuildOutlinedIcon from '@mui/icons-material/BuildOutlined'
import AppHeader from '../appHeader'
import Nav from '../nav'
import { useBackdropContext } from '../../context/backdropContext'
import NavDrop from '../navDrop'
import { useTransContext } from '../../context/transContext'

const PageBuilding = ({ headerTxt }: { headerTxt: string }) => {

	const { withNav } = useBackdropContext()

	const { translated } = useTransContext()

	// const [addr, setAddr] = React.useState('')

	// const [isReady, cancel, reset] = useTimeout(3000)

	/* React.useEffect(() => {
		if (addr.length > 0) {
			reset()
		}
	}, [addr]) */

	/* React.useEffect(() => {
		if (isReady()) {
			cancel()
			// setIsCopied(false)
			setAddr('')
		}
	}) */

	// const [isCopied, setIsCopied] = React.useState(false)

	return (
		<div className='gonimate fadeIn'>

			<Loading />

			<AppHeader pageName={headerTxt.toUpperCase()} />

			<Nav />

			{/* page content */}
			<div className='pageWrapper columnContainer justifyCenter itemsCenter'>

				<div className='buildMsgWrap'>
					<BuildOutlinedIcon fontSize='large' color='primary' />
					<p className='typography centerTxt width100'>
						{translated.construction.underCons}
					</p>
				</div>

				{/* <p className='typography centerTxt width100'>
					Help improving the site with a donation. Start sending crypto!
					<br />
					A payment provider & transaction history will be added soon.
				</p>
				<p className='typography centerTxt width100'>
					Old payment methods (EUR/USD) will be added aswell.
				</p> */}

				{/* <div className='promoPaperCol mt-1'>
					<div className='flexContainer itemsCenter justifyBetween width100'>
						<Button
							variant='outlined'
							className='btcBtn'
							endIcon={<img src='../../imgs/Bitcoin.svg.png' alt='BTC' width='40px' />}
							onClick={async () => {
								const address = 'bc1qscswyp9k2l8qnrl69qlz8680ggsyzf3pzyufx4'
								await navigator.clipboard.writeText(address)
								setIsCopied(true)
								setAddr(address)
							}}
						>
							Bitcoin (BTC)
						</Button>

						<Button
							variant='outlined'
							className='xmrBtn'
							endIcon={<img src='../../imgs/Monero.svg.png' alt='XMR' width='40px' />}
							onClick={async () => {
								const address = '85qgEiKUCgn7rdpF5pEL9LNTsXyUgsoQ1UmutKnDwoCD8PBmySePDrcHGdT76PxSGAYnS8R2QU64zheMrd3i9XmrLuKLJwj'
								await navigator.clipboard.writeText(address)
								setIsCopied(true)
								setAddr(address)
							}}
						>
							Monero (XMR)
						</Button>

					</div>
				</div> */}

				{/* <div className='promoPaperCol mt-1'>
					<Link
						href='https://en.wikipedia.org/wiki/Bitcoin'
					>
						<a target='_blank' className='href'>What is Bitcoin?</a>
					</Link>
					<Link
						href='https://en.wikipedia.org/wiki/Monero'
					>
						<a target='_blank' className='href'>What is Monero?</a>
					</Link>
				</div> */}

				{/* {
					isCopied && !isReady() &&
					<p
						style={{ wordBreak: 'break-word', wordWrap: 'break-word' }}
						className='txtCenter'
					>
						Address copied! {addr}
					</p>
				} */}

				{/* placeholder */}
				{/* <div style={{ margin: '2.5em 0 0 0' }}></div> */}

				{/* <StayTunedCard
					svg1Src='../../svgs/scissorBall.svg'
					svg2Src='../../svgs/scissor.svg'
				/> */}

			</div>

			{/* Footer */}
			<Footer />

			{withNav && <NavDrop />}

		</div>
	)
}

export default PageBuilding