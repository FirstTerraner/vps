// libs
import { AppBar, Tab, Tabs } from '@mui/material'
import { useRouter } from 'next/router'
import { isPlatformType } from 'proclubs-api/dist'
import { IClubRankLeaderboard } from 'proclubs-api/dist/model/leaderboard'
import React from 'react'
import { fifaVersion } from '../../proclubsApis'
import { useBackdropContext } from '../../context/backdropContext'
import { appName, appUrl, metaAppLogo, metaImgSrc, mlsToMS, upperCasePlatform } from '../../helpers'
import { IHighestClub, IRankingClubsProps } from '../../models/interfaces'
// helpers
import { isNum } from '../../models/typeGuards'
import AppHeader from '../appHeader'
import SimpleBackdrop from '../backdrop'
// custom components
import ClubResult from '../clubResult'
import Footer from '../footer'
import HighestRecordCard from '../highestRec'
import Loading from '../loading'
import Nav from '../nav'
import NavDrop from '../navDrop'
import Notice from '../notice'
import AddRecordCard from '../recordCard'
import TabPanel, { a11yProps } from '../tabPanel'
import MetaTags from '../metatags'
import { useTransContext } from '../../context/transContext'

const RankingPage = ({ rankingClubs, highestRecords, recInfo }: IRankingClubsProps) => {

	console.log('rec info: ', mlsToMS(recInfo?.milSecToWaitLeft))
	if (recInfo) {
		console.log('last update: ', new Date(recInfo.lastUpdateTime).toLocaleTimeString())
	}


	const router = useRouter()

	const { translated } = useTransContext()

	const { withNav } = useBackdropContext()

	const handleTabIdxRoute = () => {
		if (router.asPath.includes('/record')) { return 0 }
		if (router.asPath.includes('/season')) { return 1 }
		return 2
	}

	const [tabIdx, setTabIdx] = React.useState(handleTabIdxRoute())

	// leaderboards tab idx
	const handleTabChange = (_event: React.SyntheticEvent, newIdx: unknown) => {
		setTabIdx(isNum(newIdx) ? newIdx : 0)
		void router.push(`/${router.query.platform?.toString() || ''}/${newIdx === 0 ? 'record' : newIdx === 1 ? 'season' : 'clubs'}/leaderboards`)
	}

	React.useEffect(() => {
		// console.log('handleTabIdxRoute: ', handleTabIdxRoute())
		setTabIdx(handleTabIdxRoute())
	}, [])

	// console.log('ranking clubs: ', rankingClubs)
	const metaDescription = `1. ${rankingClubs?.[0]?.clubInfo.name || ''} (${rankingClubs?.[0]?.rankingPoints || ''} Pts), 2. ${rankingClubs?.[1]?.clubInfo.name || ''} (${rankingClubs?.[1]?.rankingPoints || ''} Pts), 3. ${rankingClubs?.[2]?.clubInfo.name || ''} (${rankingClubs?.[2]?.rankingPoints || ''} Pts)... Visit the site to see more.`
	const metaRecordDescription = `1. ${rankingClubs?.[0]?.clubInfo.name || ''} (${rankingClubs?.[0]?.wins || ''} - 0), 2. ${rankingClubs?.[1]?.clubInfo.name || ''} (${rankingClubs?.[1]?.wins || ''} - 0), 3. ${rankingClubs?.[2]?.clubInfo.name || ''} (${rankingClubs?.[2]?.wins || ''} - 0)... Visit the site to see more.`

	return (
		<div className='gonimate fadeIn'>
			{rankingClubs && rankingClubs.length > 0 &&

				<MetaTags
					pageTitle={`VPS | ${router.query.leader?.toString() || ''} ${translated.singleWords.leaderboards}`}
					metaTitle={`FIFA ${fifaVersion} Pro Clubs ${router.query.leader?.toString().toUpperCase() || ''} ${translated.singleWords.leaderboards} ${isPlatformType(router.query.platform) ? upperCasePlatform(router.query.platform) || '' : ''}`}
					des={router.query.leader && router.query.leader === 'record' ? metaRecordDescription : metaDescription}
					pageUrl={`${appUrl}/${router.query.platform?.toString() || ''}/${router.query.leader?.toString() || ''}/leaderboards}`}
					metaImgSrc={metaImgSrc}
					appName={appName}
					metaAppLogo={metaAppLogo}
					twitterCard='summary_large_image'
				/>
			}

			<Loading />

			<Nav />

			{/* overview img & page info */}
			<AppHeader
				pageName={`${router.query.leader ? (router.query.leader.toString().charAt(0) + router.query.leader.toString().slice(1)).toUpperCase() : ''} ${translated.singleWords.leaderboards.toUpperCase()}`}
				showPlat
			/>

			{/* page content */}
			<AppBar position='static' color='transparent' elevation={0}>
				<Tabs
					value={tabIdx}
					onChange={handleTabChange}
					indicatorColor='primary'
					textColor='inherit'
					centered
				>
					<Tab label={<span style={{ color: tabIdx === 0 ? 'royalblue' : 'inherit' }}>{translated.singleWords.record}</span>} {...a11yProps(0)} />
					<Tab label={<span style={{ color: tabIdx === 1 ? 'royalblue' : 'inherit' }}>{translated.singleWords.division}</span>} {...a11yProps(1)} />
					<Tab label={<span style={{ color: tabIdx === 2 ? 'royalblue' : 'inherit' }}>{translated.singleWords.club}</span>} {...a11yProps(2)} />
				</Tabs>
			</AppBar>
			<div className='pageWrapper'>

				{/* VPS Record Clubs */}
				<TabPanel value={tabIdx} index={0}>

					{/* add record club */}
					<AddRecordCard />

					{/* highest record cross platform */}
					{highestRecords && highestRecords.clubId !== rankingClubs?.[0]?.clubId &&
						<HighestRecordCard highestRecords={highestRecords} />
					}

					{/* platform records */}
					{rankingClubs && rankingClubs.length > 0 ?
						rankingClubs.map((club, i) => (
							<div className='rankingResultsWrap' key={i}>
								<ClubResult club={club} i={i} isRec highestRec={highestRecords} />
							</div>
						))
						:
						<div className='rankingResultsWrap'>
							{isPlatformType(router.query.platform) &&
								<Notice
									header={`${translated.clubPage.noRecClub} ${upperCasePlatform(router.query.platform) || ''}.`}
									txt=''
									svgSrc='../../../svgs/scissorBall.svg'
								/>
							}
						</div>
					}

				</TabPanel>

				{/* FIFA Global Season */}
				<TabPanel value={tabIdx} index={1}>

					{rankingClubs && rankingClubs.length > 0 ?
						rankingClubs.map((club, i) => (
							<div className='rankingResultsWrap' key={i}>
								<ClubResult club={club} i={i} />
							</div>
						))
						:
						<Notice
							header={translated.clubPage.notAvailable}
							txt=''
							svgSrc='../../../svgs/scissorBall.svg'
						/>
					}

				</TabPanel>

				{/* FIFA Global Clubs */}
				<TabPanel value={tabIdx} index={2}>

					{rankingClubs && rankingClubs?.length > 0 ?
						rankingClubs.map((club, i) => (
							<div className='rankingResultsWrap' key={i}>
								<ClubResult club={club} i={i} />
							</div>
						))
						:
						<Notice
							header={translated.clubPage.notAvailable}
							txt=''
							svgSrc='../../../svgs/scissorBall.svg'
						/>
					}

				</TabPanel>

			</div>
			{/* page content end */}

			<Footer />

			{/* backdrop */}
			<SimpleBackdrop />

			{withNav && <NavDrop />}

		</div>
	)
}

export default RankingPage