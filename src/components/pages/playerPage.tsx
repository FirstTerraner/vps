// libs
import { AppBar, Paper, Tab, Tabs } from '@mui/material'
import { useRouter } from 'next/router'
import { isPlatformType } from 'proclubs-api/dist'
import { IClubMemberCareer } from 'proclubs-api/dist/model/club'
import React from 'react'
import { fifaVersion } from '../../proclubsApis'
import { useBackdropContext } from '../../context/backdropContext'
// helpers
import {
	appName,
	appUrl,
	attrStrToObj,
	getAllPlayersFromLastMatches,
	getPlayerAttr,
	getPlayerSpecials,
	getPos,
	metaAppLogo,
	metaImgSrc,
	upperCasePlatform,
	wasPlayerPresentLastMatches
} from '../../helpers'
import { IPlayerPageProps, IVProAttr } from '../../models/interfaces'
import { isNum } from '../../models/typeGuards'
import AppHeader from '../appHeader'
// custom components
import SmallChip from '../chip'
import PlayerClubStats from '../clubOverview/playerClubStats'
import SquadList from '../clubOverview/squadList'
import Footer from '../footer'
import Loading from '../loading'
import Nav from '../nav'
import NavDrop from '../navDrop'
import Notice from '../notice'
import PlayerLeft from '../playerLeft'
import AllClubsStats from '../playerOverview/allClubs'
import Defending from '../playerOverview/attributes/defending'
import Dribbling from '../playerOverview/attributes/dribbling'
import GoalKeeping from '../playerOverview/attributes/goalKeeping'
import Pace from '../playerOverview/attributes/pace'
import Passing from '../playerOverview/attributes/passing'
import Physical from '../playerOverview/attributes/physical'
import Shooting from '../playerOverview/attributes/shooting'
import PlayerOverall from '../playerOverview/overall'
import RecentFormTable from '../playerOverview/table'
import PositionNote from '../positionNote'
import TabPanel, { a11yProps } from '../tabPanel'
import MetaTags from '../metatags'
import { useTransContext } from '../../context/transContext'

// PLAYER OVERVIEW component
const PlayerOverview = ({ club, clubMember, squad, matches, cupMatches }: IPlayerPageProps) => {

	const router = useRouter()

	const { translated } = useTransContext()

	const { withNav } = useBackdropContext()

	const handleTabIdxRoute = () => {
		if (router.asPath.includes('/attributes')) { return 2 }
		if (router.asPath.includes('/form')) { return 1 }
		return 0
	}
	// TODO handle this is serversideprops
	// players present in last matches
	const [presentPlayersArr, setPresentPlayersArr] = React.useState<IClubMemberCareer[]>([])
	// set players array
	React.useEffect(() => {
		setPresentPlayersArr(
			getAllPlayersFromLastMatches(
				[...matches || [], ...cupMatches || []], club.infos?.clubId || 0
			)
		)
	}, [])

	// player overview tab idx
	const [tabIdx, setTabIdx] = React.useState(handleTabIdxRoute)
	const handleTabChange = (_event: React.SyntheticEvent, newIdx: unknown) => {
		setTabIdx(isNum(newIdx) ? newIdx : 0)
		const plat = router.query.platform?.toString()
		const cId = router.query.id?.toString()
		const name = router.query.name?.toString()
		if (!plat || !cId || !name) { return }
		if (newIdx === 2) {
			void router.replace(`/${plat}/club/${cId}/squad/${name}/attributes`)
			return
		}
		if (newIdx === 1) {
			void router.replace(`/${plat}/club/${cId}/squad/${name}/form`)
			return
		}
		void router.replace(`/${plat}/club/${cId}/squad/${name}`)
	}

	// player stats of the single player
	const [playerAttr, setPlayerAttr] = React.useState<IVProAttr | null>()
	const [specials, setSpecials] = React.useState<string[]>([])
	// console.log('clubMember: ', clubMember)
	// console.log('specials: ', specials)
	// console.log('playerAttr: ', playerAttr)
	// console.log('clubMember?.career: ', clubMember?.career)
	React.useEffect(() => {
		// console.log('useEffect specials: ', specials)
		// console.log('player attributes: ', playerAttr)
		if (playerAttr && clubMember?.career) {
			setSpecials(getPlayerSpecials(translated.playerSpecial, playerAttr, +clubMember.career.proHeight))
		}
		if (!clubMember?.stats) { return }
		if (wasPlayerPresentLastMatches(
			[...cupMatches || [], ...matches || []],
			clubMember.stats.name || '',
			club.infos?.clubId.toString() || '')
		) {
			// console.log('player was in last matches')
			const attrs = getPlayerAttr(
				[...cupMatches || [], ...matches || []],
				clubMember.stats.name || '',
				club.infos?.clubId.toString() || ''
			)
			if (attrs.length > 0) {
				setPlayerAttr(attrStrToObj(attrs))
			}
		}

	}, [playerAttr, matches, cupMatches])

	// console.log('recent playersArr: ', playersArr)
	// console.log('matches: ', matches)
	// console.log('cupMatches: ', cupMatches)
	// console.log('playerAttr shows if player was present: ', playerAttr)
	const metaDescription = clubMember ?
		`${getPos(translated.playerPos, clubMember.stats?.proPos)} ${clubMember.career ? +clubMember.career?.proOverall - 1 : 'N/A'} | ${clubMember.career?.proHeight || 'N/A'}cm | ${translated.singleWords.rating}: ${clubMember.stats?.ratingAve || 'N/A'} | ${translated.singleWords.matches}: ${clubMember.career?.gamesPlayed || '0'} | ${translated.singleWords.passAccuracy}: ${clubMember.career?.passSuccessRate || '0'}% | ${translated.singleWords.goals}: ${clubMember.career?.goals || '0'} | ${translated.singleWords.assists}: ${clubMember.career?.assists || 'N/A'} | ${translated.clubPage.sotm}: ${clubMember.career?.manOfTheMatch || 'N/A'} | ${translated.singleWords.assists} ${translated.singleWords.perMatch}: ${clubMember.career ? (+clubMember.career.assists / +clubMember.career.gamesPlayed).toFixed(1) : 'N/A'} ${translated.singleWords.tackleAccuracy}: ${clubMember.career ? (+clubMember.career.tacklesMade / +clubMember.career.gamesPlayed).toFixed(1) : 'N/A'}`
		: translated.clubPage.noSquad

	// component
	return (
		<div>

			<MetaTags
				pageTitle={`VPS | ${clubMember?.stats?.name || ''}`}
				metaTitle={`${clubMember?.stats?.name || translated.clubPage.noSquad} - FIFA${fifaVersion} Pro Clubs ${translated.singleWords.player} ${translated.singleWords.form} ${isPlatformType(router.query.platform) ? upperCasePlatform(router.query.platform) || '"Platform N/A"' : ''}`}
				des={metaDescription}
				pageUrl={`${appUrl}/${router.query.platform?.toString() || ''}/club/${club.stats?.clubId || ''}/squad/${clubMember?.stats?.name || ''}`}
				metaImgSrc={metaImgSrc}
				appName={appName}
				metaAppLogo={metaAppLogo}
				twitterCard='summary_large_image'
			/>

			<Loading />

			<Nav />

			{/* page header */}
			<AppHeader showFav clubMember={clubMember?.career} clubInfo={club.infos} />

			{/* page body */}
			{clubMember?.stats || clubMember?.career ?
				<>
					{/* tab header */}
					<AppBar position='static' color='transparent' elevation={0}>
						<Tabs
							value={tabIdx}
							onChange={handleTabChange}
							indicatorColor='primary'
							textColor='inherit'
							centered
							scrollButtons='auto'
						>
							<Tab label={<span style={{ color: tabIdx === 0 ? 'royalblue' : 'inherit' }}>{translated.singleWords.overview}</span>} {...a11yProps(0)} />
							<Tab label={<span style={{ color: tabIdx === 1 ? 'royalblue' : 'inherit' }}>{translated.singleWords.form}</span>} {...a11yProps(1)} />
							<Tab label={<span style={{ color: tabIdx === 2 ? 'royalblue' : 'inherit' }}>{translated.singleWords.attributes}</span>} {...a11yProps(2)} />
						</Tabs>
					</AppBar>
					{/* tab panels */}
					<div className='pageWrapper'>

						{/* Player Overview Panel */}
						<TabPanel value={tabIdx} index={0}>
							{/* career & progress overall */}
							{clubMember.stats && clubMember.career &&
								<PlayerOverall clubMember={clubMember} />
							}

							{/* all clubs player stats */}
							{clubMember.stats &&
								<AllClubsStats memberStats={clubMember.stats} />
							}

							{/* current club player stats */}
							{clubMember.career && +clubMember.career.gamesPlayed > 0 ?
								<PlayerClubStats
									member={clubMember.career}
									clubName={club.infos?.name || ''}
									selectedMember={clubMember.career?.proName || ''}
								/>
								:
								<Notice
									header={`Club "${club.infos?.name || ''}"`}
									txt={translated.clubPage.noMatches}
									svgSrc='../../../../../svgs/scissorBall.svg'
								/>
							}
							{/* squad list */}
							<SquadList memberArr={squad.memberStats} title={`${club.infos?.name || ''} ${translated.singleWords.squad}`} />

						</TabPanel>

						{/* Player division/cup Form Panel */}
						<TabPanel value={tabIdx} index={1}>

							{/* division */}
							{matches && matches.length > 0 ?
								<Paper className='paperBlueGreen tablePaper'>
									<div className='overviewPanelHead'>
										<p className='panelHeaderTxt'>{translated.singleWords.division} {translated.singleWords.form}</p>
										<PositionNote pos={translated.singleWords.any} marginBottom={false} />
									</div>
									<RecentFormTable
										matches={matches}
										clubId={club.infos?.clubId.toString() || ''}
										playerName={clubMember.stats?.name || ''}
									/>
								</Paper>
								:
								<Notice
									header={`${translated.singleWords.division} ${translated.singleWords.form}`}
									txt={translated.clubPage.playerMissing}
									svgSrc='../../../../../svgs/scissorBall.svg'
								/>
							}
							{/* cup */}

							{cupMatches && cupMatches.length > 0 ?
								<Paper className='paperBlueGreen tablePaper'>
									<div className='overviewPanelHead'>
										<p className='panelHeaderTxt'>{translated.singleWords.cup} {translated.singleWords.form}</p>
										<PositionNote pos={translated.singleWords.any} marginBottom={false} />
									</div>
									<RecentFormTable
										matches={cupMatches}
										clubId={club.infos?.clubId.toString() || ''}
										playerName={clubMember.stats?.name || ''}
									/>
								</Paper>
								:
								<Notice
									header={`${translated.singleWords.cup} ${translated.singleWords.form}`}
									txt={translated.clubPage.playerMissing}
									svgSrc='../../../../../svgs/scissorBall.svg'
								/>
							}

							{/* squadlist in last matches */}
							<SquadList memberArr={presentPlayersArr} title={translated.clubPage.pInLastMatches} />
						</TabPanel>

						{/* Player Attributes Panel */}
						<TabPanel value={tabIdx} index={2}>
							{playerAttr ?
								<>
									{specials.length > 0 &&
										<div className='specialsWrap'>
											{specials.map(special => (
												<div
													key={`${clubMember.stats?.name || ''}-${special}`}
													className='specialChip'
												>
													<SmallChip label={special} />
												</div>
											))}
										</div>
									}
									{/* Overall */}
									{clubMember.stats && clubMember.career &&
										<PlayerOverall clubMember={clubMember} />
									}

									{/* Goalkeeping */}
									{getPos(translated.playerPos, clubMember.stats?.proPos) === 'GK' &&
										<GoalKeeping attr={playerAttr} />
									}

									{/* Shooting */}
									<Shooting attr={playerAttr} />

									{/* Passing */}
									<Passing attr={playerAttr} />

									{/* Dribbling */}
									<Dribbling attr={playerAttr} />

									{/* Defending */}
									<Defending attr={playerAttr} />

									{/* Physical */}
									<Physical attr={playerAttr} />

									{/* Pace */}
									<Pace attr={playerAttr} />

								</>
								:
								<Notice
									header={translated.singleWords.attributes}
									txt={translated.clubPage.notAvailable}
									svgSrc='../../../../../svgs/scissorBall.svg'
								/>
							}
							{/* squadlist in last matches */}
							<SquadList memberArr={presentPlayersArr} title={translated.clubPage.pInLastMatches} />

						</TabPanel>

					</div>
				</>
				:
				<PlayerLeft clubName={club.infos?.name || ''} />
			}
			{/* page body end */}

			<Footer />

			{withNav && <NavDrop />}
		</div >
	)
}

export default PlayerOverview
