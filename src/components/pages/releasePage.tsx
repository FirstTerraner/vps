import Link from 'next/link'
// custom components
import Footer from '../footer'
import Loading from '../loading'
import AppHeader from '../appHeader'
import { appName, appUrl, getDaysAgoString, metaAppLogo, metaImgSrc, pkg } from '../../helpers'
import StayTunedCard from '../stayTunedCard'
import ReleaseNote from '../releaseNote'
import Nav from '../nav'
import { useBackdropContext } from '../../context/backdropContext'
import NavDrop from '../navDrop'
import MetaTags from '../metatags'

// page component
const ReleasePage = () => {

	const { withNav } = useBackdropContext()

	return (

		<div className='gonimate fadeIn'>

			<MetaTags
				pageTitle='VPS | Releases'
				metaTitle={`VPS Version ${pkg.version} Release Notes`}
				des='We are building pro clubs features on top of the data powered by EA Sports. Check out our newest release notes.'
				pageUrl={`${appUrl}/release`}
				appName={appName}
				metaImgSrc={metaImgSrc}
				metaAppLogo={metaAppLogo}
				twitterCard='summary_large_image'
			/>

			<Loading />

			<Nav />

			<AppHeader pageName='RELEASE HISTORY' />

			{/* page content */}
			<div className='pageWrapper columnContainer justifyCenter itemsCenter txtCenter'>

				<div className='mb-1'>
					<p className='typography txtCenter width100'>
						(English only)
					</p>
					<p className='typography txtCenter width100'>
						For suggestions or bug reports, please use the GitLab <Link href='https://gitlab.com/FirstTerraner/vps/-/issues'><a className='href' target='_blank'>issue tracker</a></Link>.
					</p>
				</div>

				<ReleaseNote
					v={`Version 0.2.1 published ${getDaysAgoString(new Date(), new Date(2021, 9, 13))}`}
					c={
						<ul>
							<li>Style adjustments.</li>
							<li>Add warning note to player attributes.</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.2.0 published ${getDaysAgoString(new Date(), new Date(2021, 9, 11))}`}
					f={
						<ul>
							<li>Added settings page</li>
							<li>Translation in german, french & dutch now available in the settings.</li>
							<li>Optimized pages loading time</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.1.3 published ${getDaysAgoString(new Date(), new Date(2021, 9, 3))}`}
					c={
						<ul>
							<li>Optimized error handling</li>
							<li>Optimized image loading with nextjs Image tag</li>
							<li>Optimized pages loading time</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.1.2 published ${getDaysAgoString(new Date(), new Date(2021, 9, 2))}`}
					b={
						<ul>
							<li>Platform dropdown onchange was not changing in search page.</li>
						</ul>
					}
					c={
						<ul>
							<li>Connect gitlab api and show project members in {'"About us"'} page</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.1.1 published ${getDaysAgoString(new Date(), new Date(2021, 8, 29))}`}
					b={
						<ul>
							<li>Some Navigation buttons were redirecting to wrong pages.</li>
							<li>PC record leaderboard page was redirecting to wrong page.</li>
							<li>Some Record clubs were not updated due to a fixed sync bug.</li>
							<li>Added more info in the {'"How it works"'} record section.</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.1.0 published ${getDaysAgoString(new Date(), new Date(2021, 8, 26))}`}
					f={
						<ul>
							<li>
								Record leaderboard (BETA)
							</li>
							<li>
								Add a record club (BETA)
							</li>
							<li>
								Added Record club page for deleted clubs
							</li>
							<li>
								Added Navigation button
							</li>
							<li>
								Added Star rating in club overview
							</li>
							<li>
								Choose leaderboard section before navigating to platform
							</li>
						</ul>
					}
					c={
						<ul>
							<li>
								Updated leaderboard entries layout
							</li>
							<li>
								Updated colors
							</li>
						</ul>
					}
					b={
						<ul>
							<li>Fixed platform dropdown wrong value after changing leaderboard tab</li>
							<li>Small bug fixes & style issues</li>
						</ul>
					}
					n='Some club crests might be wrong due to new image sources from EA Sports that we did not covered yet.
					Also the player attributes might currently not be available.'
				/>

				<ReleaseNote
					v={`Version 0.0.10 published ${getDaysAgoString(new Date(), new Date(2021, 8, 23))}`}
					c={
						<ul>
							<li>
								Updated leaderboard entries layout
							</li>
							<li>
								Update API endpoint handling
							</li>
						</ul>
					}
					b={
						<ul>
							<li>Fixed several responses that changed due to FIFA22 early access release</li>
						</ul>
					}
					n='Some club crests might be wrong due to new image sources from EA Sports that we did not covered yet.
					Also the player attributes might currently not be available.'
				/>

				<ReleaseNote
					v={`Version 0.0.9 published ${getDaysAgoString(new Date(), new Date(2021, 8, 15))}`}
					f={
						<ul>
							<li>
								Added Bitcoin & Monero addresses for donations to maintainer (demo)
								<br />
								See: <Link href='/donations'><a className='href'>Donations</a></Link>
							</li>
							<li>
								Added this release notes page
							</li>
						</ul>
					}
					b={
						<ul>
							<li>Search club by name with no results showed no response</li>
							<li>Leaderboard showed no clubs on xbox series x clubs</li>
							<li>Fixed several bugs due to EA bad responses (FIFA22 update)</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.0.8 published ${getDaysAgoString(new Date(), new Date(2021, 8, 9))}`}
					f={
						<ul>
							<li>Updated About us page</li>
							<li>Updated Contribute page</li>
							<li>Updated pages layout on desktop</li>
						</ul>
					}
					b={
						<ul>
							<li>Platform dropdown menu showed empty text on first visit</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.0.7 published ${getDaysAgoString(new Date(), new Date(2021, 8, 6))}`}
					f={
						<ul>
							<li>Domain launch at virtualpro.space</li>
							<li>Updated footer layout</li>
						</ul>
					}
					b={
						<ul>
							<li>Fixed several styling issues</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.0.6 published ${getDaysAgoString(new Date(), new Date(2021, 8, 1))}`}
					f={
						<ul>
							<li>Added under construction page</li>
							<li>Updated pages layout</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.0.5 published ${getDaysAgoString(new Date(), new Date(2021, 7, 25))}`}
					f={
						<ul>
							<li>Added FIFA leaderboards</li>
							<li>Added metatags to all pages</li>
						</ul>
					}
					b={
						<ul>
							<li>Fix local storage player nation</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.0.4 published ${getDaysAgoString(new Date(), new Date(2021, 7, 19))}`}
					f={
						<ul>
							<li>Added player overview page</li>
							<li>Added player attributes page</li>
							<li>Added player form page</li>
							<li>Added local storage favorite clubs and players</li>
						</ul>
					}
					b={
						<ul>
							<li>Fix match order in division match history</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.0.3 published ${getDaysAgoString(new Date(), new Date(2021, 7, 18))}`}
					f={
						<ul>
							<li>Added squad overview in club</li>
							<li>Added division & cup matches form</li>
							<li>Added search page</li>
							<li>Redirect to club page if club search has 1 result</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.0.2 published ${getDaysAgoString(new Date(), new Date(2021, 7, 16))}`}
					f={
						<ul>
							<li>Added match history overview</li>
							<li>Added match info overview</li>
							<li>Added player list in matches</li>
						</ul>
					}
				/>

				<ReleaseNote
					v={`Version 0.0.1 published ${getDaysAgoString(new Date(), new Date(2021, 7, 14))}`}
					f={
						<ul>
							<li>Added search club by name</li>
							<li>Added club results page</li>
							<li>Added club overview page</li>
						</ul>
					}
				/>

				<StayTunedCard
					svg1Src='../../svgs/scissorBall.svg'
					svg2Src='../../svgs/scissor.svg'
				/>

			</div>

			<Footer />

			{withNav && <NavDrop />}

		</div>
	)
}

export default ReleasePage