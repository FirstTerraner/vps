// libs
// icons
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import FavoriteIcon from '@mui/icons-material/Favorite'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined'
import SettingsIcon from '@mui/icons-material/Settings'
import { useRouter } from 'next/router'
import { isPlatformType } from 'proclubs-api/dist'
import React from 'react'
import { clubCrestUrl } from '../helpers'
// helpers
import { addFavClubLocalStorage, isInFavClubs, removeFavClubLocalStorage } from '../localStorage/favClubs'
import { addFavPlayerLocalStorage, isInFavPlayers, removeFavPlayerLocalStorage } from '../localStorage/favPlayers'
import { IHeaderNavProps } from '../models/interfaces'
import { fifaVersion } from '../proclubsApis'
import LanguageMenu from './langDropdown'
// custom components
import PlatformDropdown from './platformDropdown'

const HeaderNav = ({ clubInfo, clubMember, showFav, showPlat, pageName }: IHeaderNavProps) => {

	const router = useRouter()

	// fav club
	const [isFav, setIsFav] = React.useState(false)
	// fav player
	const [isFavP, setIsFavP] = React.useState(false)

	// set fav states
	React.useEffect(() => {
		if (showFav) {
			// if there is no club member, check for fav club
			if (!clubMember) {
				setIsFav(isInFavClubs(clubInfo?.clubId.toString() || ''))
				return
			}
			// else check for fav player
			setIsFavP(isInFavPlayers(clubMember.name))
		}
	}, [clubInfo])

	return (
		<div className='clubOverviewOpts z2'>

			{!pageName?.includes(`FIFA ${fifaVersion}`) ?
				<>
					{/* back icon btn */}
					<div className='backToWelcome' onClick={() => {
						if (history && history.length > 2) {
							router.back()
							return
						}
						void router.push('/')
					}}>
						<ArrowBackIcon />
					</div>

					{/* home icon btn */}
					<div className='backToHome' onClick={() => router.push('/')}>
						<HomeOutlinedIcon />
					</div>
				</>
				:
				<>
					<div className='settsBtn pointer' onClick={() => router.push('/settings')}>
						<SettingsIcon />
					</div>
					<div className='homeLang'>
						<LanguageMenu />
					</div>
				</>
			}

			{/* fav icon btn */}
			{((showFav && clubMember) || (showFav && clubInfo)) &&
				<div
					className='pointer favClubHeart'
					onClick={() => {
						// nothing to save
						if (!clubInfo || !isPlatformType(router.query.platform)) { return }
						// if there is no club member, set a whole club as fav
						if (!clubMember) {
							if (isInFavClubs(clubInfo.clubId.toString())) {
								removeFavClubLocalStorage(clubInfo.clubId.toString())
								setIsFav(false)
								return
							}
							addFavClubLocalStorage({
								clubId: clubInfo.clubId.toString(),
								clubName: clubInfo.name,
								crestUrl: clubCrestUrl(clubInfo.teamId), // clubInfo.customKit.isCustomTeam === '0' ? clubInfo.teamId : +clubInfo.customKit.crestAssetId
								platform: router.query.platform
							})
							setIsFav(true)
							return
						}
						// if club member is available, save as fav
						if (isFavP) {
							removeFavPlayerLocalStorage(clubMember?.name || '')
							setIsFavP(false)
							return
						}
						addFavPlayerLocalStorage({
							clubId: clubInfo.clubId.toString(),
							playerName: clubMember?.name || '',
							nationUrl: clubMember?.proNationality || '',
							platform: router.query.platform
						})
						setIsFavP(true)
					}}
				>
					{isFav || isFavP ? <FavoriteIcon color='error' /> : <FavoriteBorderIcon />}
				</div>
			}

			{/* platform dropdown */}
			{showPlat &&
				<div className='rankingDropdown' style={{ marginRight: '-1em' }}>
					<PlatformDropdown padding={false} />
				</div>
			}

		</div>
	)
}

export default HeaderNav