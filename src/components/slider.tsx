import Slider from '@mui/material/Slider'

const valuetext = (val: number) => `${val} Pts`

const DivisionSlider = ({ def, marks }: { def: number, marks: { label: string, value: number, }[] }) => (
	<div className='divSliderWrap'>
		<Slider
			disabled
			value={def}
			getAriaValueText={valuetext}
			aria-labelledby='discrete-slider-custom'
			min={0}
			step={1}
			max={30}
			valueLabelDisplay='on'
			marks={marks}
		/>
	</div>
)
export default DivisionSlider