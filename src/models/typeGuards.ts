/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/ban-types */

export function cTo<T>(json?: string | null) {
	if (!json || json?.length < 2) { return null }
	try {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const thing: T = JSON.parse(json)
		return thing
	} catch (e) { console.error(e) }
	return null
}
export function toJsonStr<T>(thing: T) { try { return JSON.stringify(thing) } catch (e) { return null } }

export const isKeyof = <T>(o: T, k: PropertyKey): k is keyof T => k in o
export const toPropnameArr = <T>(propnames: string[], o: T) => {
	o = cTo<T>(toJsonStr(o)) || o
	const result: (keyof T)[] = []
	for (const x of propnames) {
		if (isKeyof(o, x) && !result.includes(x)) { result.push(x) }
	}
	return result
}
/* export const getPropnameArr = <T>(o: T) => {
	const tmp = cTo<T>(toJson(o)) || {}
	const arr = [
		...new Set<string>([tmp, o]
			.flatMap(x => [...Object.keys(x), ...Object.getOwnPropertyNames(x)]))
	]
	// const testArr = [...new Set<string>([
	// 	...Object.keys(o),
	// 	...Object.getOwnPropertyNames(o),
	// 	...Object.keys(tmp),
	// 	...Object.getOwnPropertyNames(tmp)
	// ])]
	// console.log(deepEqual(arr, testArr), arr.every(x => testArr.includes(x)))
	return toPropnameArr(arr, o)
} */

// eslint-disable-next-line @typescript-eslint/ban-types
/* export const checkObj = <T extends {}>(o: any, t: T, level = 0): o is T => {
	const pArr = getPropnameArr(t)
	const pArr2 = getPropnameArr(o)
	console.log({ level }, { pArr }, { pArr2 })

	if (!pArr.every(x => pArr2.includes(x))) { return false }
	if (pArr.length !== pArr2.length) { return false }

	if (!pArr.every(x => typeof o[x] === typeof t[x])) { return false }

	if (!pArr.every(x => isStr(o[x]) === isStr(t[x]))) { return false }
	if (!pArr.every(x => isNum(o[x]) === isNum(t[x]))) { return false }
	if (!pArr.every(x => isArr(o[x]) === isArr(t[x]))) { return false }
	if (!pArr.every(x => isBool(o[x]) === isBool(t[x]))) { return false }
	if (!pArr.every(x => isErr(o[x]) === isErr(t[x]))) { return false }
	if (!pArr.every(x => isFunc(o[x]) === isFunc(t[x]))) { return false }
	if (!pArr.every(x => isNull(o[x]) === isNull(t[x]))) { return false }
	if (!pArr.every(x => isObj(o[x]) === isObj(t[x]))) { return false }
	if (!pArr.every(x => isUndef(o[x]) === isUndef(t[x]))) { return false }

	for (const prop of pArr) {
		if (isObj(o[prop])) { console.log('#########', { prop }, inspect(o[prop], true, 999, true), o[prop].constructor, o[prop].constructor.name) }
		if (isObj(o[prop]) && !checkObj(o[prop], t[prop], level + 1)) { return false }
	}
	return true
} */
interface TypeMap { // for mapping from strings to types
	array: unknown[]
	boolean: boolean
	number: number
	string: string
}

// tslint:disable-next-line: ban-types
export type ObjectType<T> = (new (...args: unknown[]) => T) | Function
type PrimitiveOrConstructor = // 'string' | 'number' | 'boolean' | constructor
	| (new (...args: unknown[]) => unknown)
	| keyof TypeMap

// infer the guarded type from a specific case of PrimitiveOrConstructor
type GuardedType<T extends PrimitiveOrConstructor> = T extends new (...args: unknown[]) => infer U ? U : T extends keyof TypeMap ? TypeMap[T] : never
// finally, guard ALL the types!
export function typeGuard<T extends PrimitiveOrConstructor>(o: unknown, className: T):
	o is GuardedType<T> {
	const localPrimitiveOrConstructor: PrimitiveOrConstructor = className
	if (typeof localPrimitiveOrConstructor === 'string') {
		return typeof o === localPrimitiveOrConstructor
	}
	return o instanceof localPrimitiveOrConstructor
}


export const isEvt = <T>(v: unknown, e: ObjectType<T>) => v instanceof e
export const isArr = (v: unknown): v is unknown[] => !!((v && Array.isArray(v)) || (v && typeof v === 'object' && v.constructor === Array))
export const isBool = (v: unknown): v is boolean => typeof v === 'boolean'
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
export const isErr = (v: any): v is Error => !!(v instanceof Error || (v && v.__proto__ && v.__proto__.name === 'Error'))
// tslint:disable-next-line: ban-types
export const isFunc = (v: unknown): v is Function => typeof v === 'function'
export const isNull = (v: unknown): v is null => v === null
export const isNum = (v: unknown): v is number => typeof v === 'number' && !isNaN(v) && isFinite(v)
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
// eslint-disable-next-line @typescript-eslint/ban-types
export const isObj = (v: unknown): v is object => !!(v && typeof v === 'object' && v.constructor === Object)
export const isStr = (v: unknown): v is string => typeof v === 'string' || v instanceof String
export const isUndef = (v: unknown): v is undefined => typeof v === 'undefined'
export const isObjAndInstance = <T>(v: unknown, type: ObjectType<T>): v is typeof type => !!(v && v instanceof type)
