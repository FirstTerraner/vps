export type TLocale = 'de-DE' | 'fr-FR' | 'nl-NL' | 'en-US' | null

export type TDefaultLocale = 'en-US' | null

export const isTLocale = (lType: unknown) => lType === 'de-DE' || lType === 'fr-FR' || lType === 'nl-NL' || lType === 'en-US' || !lType

export interface IPlayerPos {
	gk: string
	cb: string
	rwb: string
	rb: string
	lwb: string
	lb: string
	cdm: string
	cm: string
	lm: string
	rm: string
	cam: string
	rw: string // right wing
	lw: string // left wing
	cf: string // central front
	rf: string // right front
	lf: string // left front
	st: string // attacker
}

export interface ISpecTrans {
	sniper: string
	finisher: string
	aerialThreat: string
	freeKick: string
	dribbler: string
	playmaker: string
	crosser: string
	tackler: string
	speedster: string
	tactician: string
	acrobat: string
	compForward: string
	compMid: string
	compDef: string
}