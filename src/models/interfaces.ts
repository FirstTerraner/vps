import { TPlatformType } from 'proclubs-api/dist'
import {
	IClubInfo,
	IClubMatches,
	IClubMemberCareer,
	IClubMemberStats,
	IClubSearch,
	IClubStats
} from 'proclubs-api/dist/model/club'
import { IClubRankLeaderboard } from 'proclubs-api/dist/model/leaderboard'

export interface IRespWrap<T> { data: T }
// export interface ISearchRespWrap<T> { clubId: T }
export interface IClubId { clubId: number }
export type TGameType = `gameType${9 | 13}` // 9 for season matches / 13 for cup matches
export interface IPkg {
	name: string
	version: string
}
export interface ITabPanelProps {
	children?: React.ReactNode
	index: number
	value: unknown
}
export interface IClubOverviewState {
	infos: IClubInfo | null
	stats: IClubStats | null
}
export interface IRecClubOverviewState {
	infos: IClubInfo | null
	stats: IRecClub | null
}
export type TClubProps = [IClubInfo | null | undefined, IClubStats | null | undefined]
export type TMatchProps = [IClubMatches[] | null | undefined, IClubMatches[] | null | undefined]
export type TMemberProps = [IClubMemberCareer[] | null | undefined, IClubMemberStats[] | null | undefined]
export interface ISquadOverviewState {
	memberCareer: IClubMemberStats[] | null
	memberStats: IClubMemberCareer[] | null
}
export type TMatchesState = IClubMatches[] | null
export interface IMembersOpenState {
	[key: string]: boolean
}
export interface IMemberPosCount {
	def: number
	front: number
	gk: number
	mid: number
}
export interface IFavClub {
	clubId: string
	clubName: string
	crestUrl: string
	platform?: TPlatformType
}
export interface IFavPlayer {
	clubId: string
	nationUrl: string
	platform: TPlatformType
	playerName: string
}
export interface IRecPlayer {
	name: string
	pos: string // position
	playedMatches?: number // not included yet
}

export interface IRecClub extends IClubSearch {
	squadList: IRecPlayer[]
	addedAt: string
	/* while updating record clubs entries & seeing a loss or club deleted,
	update hasLost value so we know there is no need to fetch the club next time */
	hasLost: boolean
	plat: TPlatformType
}

export const isIRecClub = (val: IRecClub | IClubRankLeaderboard | IClubSearch | IClubOverviewState): val is IRecClub => (val as IRecClub).hasLost !== undefined

export const isIRecClubArr = (val: IRecClub[] | IClubRankLeaderboard[] | IClubSearch[] | IClubOverviewState): val is IRecClub[] =>
	(val as IRecClub[]).length > 0 && (val as IRecClub[])[0].hasLost !== undefined

export interface IHighestClub extends IRecClub {
	plat: TPlatformType
}

export interface IVProAttr {
	acceleration: string
	aggression: string
	agility: string
	attackPosition: string
	balance: string
	ballControl: string
	crossing: string
	curve: string
	dribbling: string
	finishing: string
	freekickAccuracy: string
	gkDiving: string
	gkHandling: string
	gkKicking: string
	gkReflexes: string
	headingAccuracy: string
	interceptions: string
	jumping: string
	longPass: string
	longShots: string
	marking: string
	penalties: string
	reactions: string
	shortPass: string
	shotPower: string
	slideTackle: string
	sprintSpeed: string
	stamina: string
	standTackle: string
	strength: string
	vision: string
	volleys: string
}
export interface IClubMember {
	career: IClubMemberStats | null
	stats: IClubMemberCareer | null
}

export interface IRecentMatchesData {
	assists: string
	gkSaves: string
	goals: string
	oppScore: string
	passes: string
	passRate: string
	position: string
	rating: string
	redCards: string
	score: string
	shotRate: string
	shots: string
	sotm: string
	tackleRate: string
	tackles: string
	timeAgo: string
}

export interface IHeaderNavProps {
	clubInfo?: IClubInfo | null
	clubMember?: IClubMemberStats | null
	pageName?: string
	showFav?: boolean
	showPlat?: boolean
}

export interface IPlayerPageProps {
	squad: ISquadOverviewState
	club: IClubOverviewState,
	clubMember: IClubMember | null
	matches?: IClubMatches[] | null
	cupMatches?: IClubMatches[] | null
}

export interface IPlayerLeftProps {
	clubName: string
}

export interface IMatchesProps {
	club: IClubOverviewState,
	matches: IClubMatches[] | null
	cupMatches: IClubMatches[] | null
	squad: ISquadOverviewState
}

export interface IVisitors {
	[key: string]: IVisitor[]
}

// a: user-agent
// t: time
// TODO l: location
export interface IVisitor {
	ip: string
	a: string
	d: Date
}

export interface IAllVisits {
	'AT': number
}

interface ISAMLID {
	extern_uid: string
	provider: string
	saml_provider_id: number
}
/*
No access (0)
Minimal access (5) (Introduced in GitLab 13.5.)
Guest (10)
Reporter (20)
Developer (30)
Maintainer (40)
*/
type TMemberAccessLvl = 0 | 5 | 10 | 10 | 30 | 40

export interface IProjectMember {
	id: number
	username: string
	name: string
	state: string
	avatar_url: string
	web_url: string
	expires_at: string
	access_level: TMemberAccessLvl
	group_saml_identity: ISAMLID | null
}

export interface IMetaTagsProps {
	pageTitle: string
	metaTitle: string
	des: string
	metaImgSrc?: string
	pageUrl: string
	appName: string
	metaAppLogo: string
	twitterCard: string
}

export interface IRankingClubsProps {
	rankingClubs: IClubRankLeaderboard[] | null
	highestRecords: IHighestClub | null
	recInfo: IRecUpdateInfo | null
}

interface IRecUpdateInfo {
	lastUpdateTime: number
	milSecToWaitLeft: number
}