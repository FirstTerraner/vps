<div align="center">
<p>
	<img src="https://gitlab.com/FirstTerraner/vps/-/raw/main/public/imgs/metaImg.png">
</p>
</div>
<!-- <p>
    <img width='200' src="https://www.virtualpro.space/imgs/appName.png">
</p> -->


<div align="center">
<h1>A browser based open source platform for the FIFA pro clubs community</h1>

[Homepage](https://www.virtualpro.space) | [Pre-release](https://vps-git-dev-firstterraner.vercel.app/) |
[Release notes](https://www.virtualpro.space/release) |
[About us](https://virtualpro.space/about) | [Contribute](https://virtualpro.space/contribute) |
[Donations](https://virtualpro.space/donations)

<!-- [![pipeline status][PipelineStatusBadge]][ProjectUrl]
[![coverage report][CoverageBadge]][ProjectUrl] -->

</div>

<div align="center">

<!-- [![Open Source Love](https://badges.frapsoft.com/os/v2/open-source.png?v=103)](https://github.com/ellerbrock/open-source-badges/) -->
<!-- [![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0) -->
<!-- [![TypeScript](https://badges.frapsoft.com/typescript/code/typescript.svg?v=101)](https://github.com/ellerbrock/typescript-badges/) -->
[![Discord][DiscordBadge]][DiscordUrl]
[![Twitter][TwitterBadge]][TwitterUrl]

</div>

[ProjectUrl]: https://gitlab.com/FirstTerraner/vps
<!-- [ProjectPage]: https://eclubstournaments.gitlab.io/frontend/vps -->

[PipelineStatusBadge]: https://gitlab.com/FirstTerraner/vps/badges/master/pipeline.svg
[CoverageBadge]: https://gitlab.com/FirstTerraner/vps/badges/master/coverage.svg

[DiscordBadge]: https://img.shields.io/discord/835310846480482314?label=Discord&logo=discord&logoColor=white
[TwitterBadge]: https://img.shields.io/twitter/follow/VirtualProSpace.svg?style=flatl&label=Follow&logo=twitter&logoColor=white&color=1da1f2

[DiscordUrl]: https://discord.gg/Y94sAMkCra
[TwitterUrl]: https://twitter.com/VirtualProSpace

# Contribute

<p>Pull requests are always welcome, please make sure to check out the [contribution](https://www.virtualpro.space/contribute) page.</p>

<h3>Thanks a lot!<h3>
